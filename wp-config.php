<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'myletters');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Wqv+y9E]5;6~,|+#xDU80UXuSiz1rR<^%2_Ee5`]4+mMC~ -P8$l9- O9dc#JkbS');
define('SECURE_AUTH_KEY',  '#@XU-RN5G8%dh6fw4[ 1CJ (=C^^WN8TQ9p6@5kT>n3=w%nvlNKU[|#h?a^iUilS');
define('LOGGED_IN_KEY',    'P(#!Px+ZYb`W;fW:}.T{yvSy+z0hd?u?.DjL3WMoy1`c)qgo 0@p >98~3St,[K.');
define('NONCE_KEY',        ':)XgYz91Ry*b=&q7c7k(cnXY~=Iym+xWyOhX:YDB0]-evwzDF3uZdQWGfe/lO]w)');
define('AUTH_SALT',        ',?Z?3z$9MD`H0q*;c5PxFsh!_uB<lNaakVh[Jcc#Vv61vX(+*# H-T-bKU)`a!.7');
define('SECURE_AUTH_SALT', ':(5[0QP.dnncGN|h7kliSBsReYX1S=71|_~{OeE;n4Xw{5o}VdA~8XKfmXIl-J~j');
define('LOGGED_IN_SALT',   'm(?)Y-lVK<$,>,)x69dD*@T|V]MmBvEg#5@$nu)Vc!v%(XR=B`S+WT3,a/|bwQ2&');
define('NONCE_SALT',       'L)c*I4FGIo.#7[qtvpL&?eh[v6j{V#U<B-vM:_mmnCH^VBY LIT?Ytb>*a9*?bN+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
