<?php 

/* Insert custom functions here */

// Hide gravity form lable
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

add_action( 'widgets_init', 'theme_slug_widgets_init' );
function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Footer box 5', 'theme-slug' ),
        'id' => 'footer-box-5',
		'before_widget' => '<div id="%1$s" class="sidebar-widget footer-widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5>',
		'after_title'   => '</h5>',
    ) );
}

function search_filter($query) {
if ( !is_admin() && $query->is_main_query() ) {
    if ($query->is_search) {
      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
      $query->set('tax_query', array( 'taxonomy' => 'manualknowledgebasecat',
						        'field' => 'term_id',
						        'terms' => $_GET['cat'] ) );
      $query->set( 'posts_per_page', '10' );
      //$query->set( 'paged', $paged );
      $query->set('post_type', 'manual_kb');
    }
  }
}
add_action('pre_get_posts','search_filter');
/**
 * Proper way to enqueue scripts.
 */
function theme_enqueue_scripts() {
	global $wp_query;
	// Enqueue jQuery UI and autocomplete
	wp_enqueue_script( 'jquery-ui-core' );
    wp_enqueue_script( 'jquery-ui-autocomplete' );
    wp_enqueue_script( 'childjs', get_stylesheet_directory_uri().'/js/childjs.js' , array(), '0.0.1', true);
 	wp_enqueue_script( 'childjs' );
    wp_localize_script( 'childjs', 'loadmore_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'current_page' => 1,
		'cat' => $_GET['cat'],
		's' => $_GET['s']
	) );  
}

add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );

function theme_autocomplete_js() {
	$cat = $_POST['cat'];
	if($cat){

		$args = array(
				'post_type' => 'manual_kb',
				'post_status' => 'publish',
				'posts_per_page' => -1, 
				'tax_query' => array(
				    array(
				        'taxonomy' => 'manualknowledgebasecat',
				        'field' => 'term_id',
				        'terms' => $cat
				    )
				),
				
			);
		
	}else{
		$args = array(
			'post_type' => 'manual_kb',
			'post_status' => 'publish',
			'posts_per_page'   => -1
		);
   	}

	 $posts = get_posts( $args );
	 
	 if( $posts ) :
		 foreach( $posts as $k => $post ) {
			// $source[$k]['ID'] = $post->ID;
			 $source[$k]['label'] = $post->post_title; // The name of the post
			 $source[$k]['permalink'] = get_permalink( $post->ID );
		 }
	  
	  echo json_encode( array_values( $source ) );
	
	 endif;
	 die(0);
}

add_action('wp_ajax_autoloadposts', 'theme_autocomplete_js'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_autoloadposts', 'theme_autocomplete_js'); // wp_ajax_nopriv_{action}

function theme_autocomplete_withoutcat(){
	
	$args = array(
		'post_type' => 'manual_kb',
		'post_status' => 'publish',
		'posts_per_page'   => -1
	);
   

	 $posts = get_posts( $args );
	 
	 if( $posts ) :
		 foreach( $posts as $k => $post ) {
			 $source[$k]['ID'] = $post->ID;
			 $source[$k]['label'] = $post->post_title; // The name of the post
			 $source[$k]['permalink'] = get_permalink( $post->ID );
		 }
	 ?>
	   <script type="text/javascript">
		 jQuery(document).ready(function($){
		 var posts = <?php echo json_encode( array_values( $source ) ); ?>;
		 
		 jQuery( 'input[name="s"]' ).autocomplete({
		         source: posts,
		         minLength: 2,
		         select: function(event, ui) {
		             var permalink = ui.item.permalink; // Get permalink from the datasource
		 
		             //window.location.replace(permalink);
		         }
		     });
		 });    
	 </script>
	
	 <?php endif;
}

//add_action( 'wp_head', 'theme_autocomplete_withoutcat' );

function my_override_404() {
    // Conditions required for overriding 404
    if (isset($_GET['s']) || isset($_GET['cat'])) {
        global $wp_query;
        $wp_query->is_404 = false;
    }
}
add_action('init', 'my_override_404');


function loadmore_ajax_handler(){

 		$category_id = $_POST['cat'];
  		$textsearch = $_POST['s'];
		$pagedsearch = $_POST['page'] + 1;
  		if($category_id !=''  || !empty($category_id)){
  			if(($category_id !='') && $textsearch !=''){
	  			$args = array(
						'post_type' => 'manual_kb',
						'posts_per_page' => 20,
						'paged' => $pagedsearch, 
						's' => $textsearch,
						'tax_query' => array(
						    array(
						        'taxonomy' => 'manualknowledgebasecat',
						        'field' => 'term_id',
						        'terms' => $category_id
						    )
						),// you may edit this number
					);
	  		}else{
	  			$args = array(
						'post_type' => 'manual_kb',
						'post_status' => 'publish',
						'posts_per_page' => 20, 
						'paged' => $pagedsearch,
						'tax_query' => array(
						    array(
						        'taxonomy' => 'manualknowledgebasecat',
						        'field' => 'term_id',
						        'terms' => $category_id
						    )
						),
						
					);
  			}
		}elseif($textsearch !=''){
			$args = array(
				'post_type' => 'manual_kb',
				'post_status' => 'publish',
				'posts_per_page' => 20,
				'paged' => $pagedsearch,
				's' => $textsearch,
			);
		
		}else{
			$args = array(
				'post_type' => 'manual_kb',
				'post_status' => 'publish',
				'paged' => $pagedsearch,
				'posts_per_page' => 50, // you may edit this number
			);
		}
	$the_query = new WP_Query( $args );
	if ( $the_query->have_posts() ) : 

		while ( $the_query->have_posts() ) : $the_query->the_post();
			get_template_part( 'content', 'search' );
		endwhile;
	 //$GLOBALS['wp_query']->max_num_pages = $the_query->max_num_pages;
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}

add_action('wp_ajax_loadmore', 'loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'loadmore_ajax_handler'); // wp_ajax_nopriv_{action}


if (!function_exists('manual_header_design_control')) {
	function manual_header_design_control($current_post_type = '', $url = '', $parallax_effect = false, $searchbox = false, $replace_title='', $replace_title_act = true, $subtitle = '', $subtitle_act = false, $breadcrumb = true) {
		global $theme_options, $product, $post;
		$plx_data_src = $plx_bk_img = $auto_adjust_padding =  $plx_class = '';
		
			if($parallax_effect == true && $url != '' ) {
				$plx_data_src = 'data-image-src="'.$url.'"  data-parallax="scroll"';
				$plx_bk_img = "background:none;";
				$plx_class = 'parallax-window';
			}
			echo '<div class="jumbotron_new inner-jumbotron jumbotron-inner-fix noise-break header_custom_height '.$plx_class.'" '.$plx_data_src.' style="'.$plx_bk_img.' ">
			<div class="page_opacity header_custom_height_new">
			  <div class="container inner-margin-top">
				<div class="row">
				  <div class="col-md-12 col-sm-12 header_control_text_align"> 
				  	<h1 class="inner-header custom_h1_head">';
					if( $current_post_type == 'manual_faq' ) {
						$faq_catname = get_term_by( 'slug', get_query_var( 'term' ), 'manualfaqcategory' );
						if( !empty($faq_catname) ) {
							echo $faq_catname->name;
						} else {
							$terms = get_the_terms( $post->ID , 'manualfaqcategory' ); 
							if( !empty($terms) ) echo $terms[0]->name;
						}
					} else if( $current_post_type == 'manual_kb' ) { 
						$kb_catname = get_term_by( 'slug', get_query_var( 'term' ), 'manualknowledgebasecat' );
						if( !empty($kb_catname) ) {
							echo $kb_catname->name;
						} else {
							if ( is_tax() ) { 
								$current_term = get_term_by( 'slug', get_query_var( 'term' ), 'manual_kb_tag' );
								echo $current_term->name;
							} else { 
								$terms = get_the_terms( $post->ID , 'manualknowledgebasecat' ); 
								if( !empty($terms) ) echo $terms[0]->name;
							}
						}
					} else if( $current_post_type == 'manual_documentation' ) { 
						$doc_catname = get_term_by( 'slug', get_query_var( 'term' ), 'manualdocumentationcategory' );
						if( !empty($doc_catname) ) {
							echo $doc_catname->name;
						} else {
							$terms = get_the_terms( $post->ID , 'manualdocumentationcategory' ); 
							if( !empty($terms) ) echo $terms[0]->name;
						}
					} else if ( class_exists('bbPress') && is_bbPress() ) { // bbpress
						echo $theme_options['manual-forum-title'];
					} else if( $current_post_type == 'page' || (is_home() && $current_post_type == 'post') || ($current_post_type == 'manual_portfolio') 
								||  function_exists("is_woocommerce") && function_exists("is_product") && is_product() && is_single() /*woo handle - 1*/ 
								||  function_exists("is_woocommerce") && (is_shop())  /*woo handle - 2*/  ) { 
						if( $replace_title_act == true ) echo (($replace_title != '')?$replace_title:get_the_title());
					} else if( function_exists("is_product_category") && is_product_category() /*woo handle - 3*/ ) {
						echo single_term_title();
					} else if( (is_single()) ) {
						$postID = get_option('page_for_posts');
						$pagetitle = get_post_meta( $post->ID, '_manual_page_tagline', true );
						echo ($theme_options['blog_single_title_on_header'] ==  false? (($pagetitle != '')?$pagetitle:'') :get_the_title());
					} else {
						 if ( is_category() ) {
							 echo single_cat_title( '', false ); 
						 } else if ( is_tag() ) {
							echo single_cat_title( '', false ); 
						 } else if( is_archive() ) {
							if ( is_day() ) :
									printf( esc_html__( 'daily archives: %s', 'manual' ), get_the_date() );
								elseif ( is_month() ) :
									printf( esc_html__( 'monthly archives: %s', 'manual' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'manual' ) ) );
								elseif ( is_year() ) :
									printf( esc_html__( 'yearly archives: %s', 'manual' ), get_the_date( _x( 'Y', 'yearly archives date format', 'manual' ) ) );
								else :
									esc_html_e( 'archives', 'manual' );
							endif;
						 } else if( is_404() ) {
							 echo $theme_options['home-404-main-title'];
						 } else if( is_search() ) {
						 	echo $replace_title;
						 } else {
							echo get_the_title();
						 }
					}
					echo '</h1>';
					// subtitle
					if( (is_home() && $current_post_type == 'post') || $current_post_type == 'page' && $subtitle_act == true 
								|| function_exists("is_woocommerce") && (is_shop()) || ($current_post_type == 'manual_portfolio') ) { 
						echo '<p class="inner-header-color">'.$subtitle.'</p>';
					} else if ( class_exists('bbPress') && is_bbPress() ) {
						if( !empty($theme_options['manual-forum-subtitle']) && bbp_is_forum_archive() ) { 
							echo '<p class="inner-header-color">'.$theme_options['manual-forum-subtitle'].'</p>';
						}
					} else if( is_search() ) {
						/*if( $subtitle == '' ) $space_quot = '';
						else $space_quot = '&quot;';
						 echo '<p class="inner-header-color">'.$subtitle.'&nbsp;<b>'.$space_quot.''.get_search_query().''.$space_quot.'</b></p>';*/
					 } 
					// breadcurmb
					if(function_exists("is_product") && is_product() || function_exists("is_product_category") && is_product_category() ){ // woo
						$woo_args = array(
								'delimiter' => '<span class="sep">/</span>',
								'wrap_before' => '<div id="breadcrumbs">',
								'wrap_after' => '</div>',
						);
						echo '<div class="inner-header-color">'.woocommerce_breadcrumb($woo_args) .'</div>'; 
					} else if ( class_exists('bbPress') && is_bbPress() ) { //bbpress
						if( $theme_options['bbpress_breadcrumb_status'] == true ) {
							if( bbp_is_single_user() != true && !bbp_is_forum_archive() ) {
								$manual_breadcrumbs_args = array(
									'before'          => '',
									'after'           => '',
									'sep'             => esc_html__( '/', 'manual' ),
									'home_text'       => esc_html__( 'Home', 'manual' ),
									(($theme_options['bbpress_breadcrumb'] ==  true )?'':'include_root') => ( ($theme_options['bbpress_breadcrumb'] ==  true )? '' : false ),
								);
								echo '<div id="breadcrumbs" class="forum-breadcrumbs">'.bbp_get_breadcrumb($manual_breadcrumbs_args).'</div>'; 
							}
						}
					} else {
						if( is_single() && $theme_options['blog_single_breadcrumb_on_header'] == false ) $breadcrumb = false;
						if( !is_404() && $breadcrumb == true ) { 
							echo '<div class="inner-header-color">'.manual_breadcrumb().'</div>'; 
						}
					}
					
					// search
					if ( class_exists('bbPress') && is_bbPress() ) {
						if( $theme_options['bbpress_search_status'] == true ) {
							echo '<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1 search-margin-top"><div class="global-search">';
							get_template_part( 'search', 'home' );
							echo '</div></div>';
						}
					} else if($searchbox == true) { 
						echo '<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1 search-margin-top"><div class="global-search">';
						get_template_part( 'search', 'home' );
						echo '</div></div>';
					} 
                    
			  echo '</div>
			</div>
		  </div>
		  </div>
		</div>';
		
	}
}
