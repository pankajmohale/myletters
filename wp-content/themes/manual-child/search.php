<?php
/**
 * The template for displaying search results pages.
 */

get_header();
global $wp_query;
global $paged;
/*$wp_query->is_search = false;
//print_r($wp_query);*/
?>
<!-- /start container -->
<div class="container content-wrapper body-content">
<div class="row margin-top-btm-50">
<div class="col-md-8 col-sm-8">
  <?php 
  		$category_id = $_GET['cat'];
  		$textsearch = $_GET['s'];
  		$pagedsearch = ( get_query_var('page') ) ? get_query_var('page') : 1;
  		if($category_id !=''  || !empty($category_id)){
  			if(($category_id !='') && $textsearch !=''){
	  			$args = array(
						'post_type' => 'manual_kb',
						'posts_per_page' => 20,
						'paged' => $pagedsearch, 
						's' => $textsearch,
						'tax_query' => array(
						    array(
						        'taxonomy' => 'manualknowledgebasecat',
						        'field' => 'term_id',
						        'terms' => $category_id
						    )
						),// you may edit this number
					);
	  		}else{
	  			$args = array(
						'post_type' => 'manual_kb',
						'post_status' => 'publish',
						'posts_per_page' => 20, 
						'paged' => $pagedsearch,
						'tax_query' => array(
						    array(
						        'taxonomy' => 'manualknowledgebasecat',
						        'field' => 'term_id',
						        'terms' => $category_id
						    )
						),
						
					);
  			}
		}elseif($textsearch !=''){
			$args = array(
				'post_type' => 'manual_kb',
				'post_status' => 'publish',
				'posts_per_page' => 20,
				'paged' => $pagedsearch,
				's' => $textsearch,
			);
		
		}else{
			$args = array(
				'post_type' => 'manual_kb',
				'post_status' => 'publish',
				'paged' => $pagedsearch,
				'posts_per_page' => 50, // you may edit this number
			);
		}
  		$the_query = new WP_Query( $args );
	if ( $the_query->have_posts() ) : 

		while ( $the_query->have_posts() ) : $the_query->the_post();
			get_template_part( 'content', 'search' );
		endwhile;
	else :
		esc_html_e( 'Sorry!! nothing found related to your search topic, please try search again.', 'manual' );
	endif; ?>
	
	<?php 
    /*$GLOBALS['wp_query'] = $the_query;
	$GLOBALS['wp_query']->max_num_pages = $the_query->max_num_pages;
	
        the_posts_pagination( array(
           'mid_size' => 1,
           'prev_text'          => esc_html__( '&lt;', 'manual' ),
		   'next_text'          => esc_html__( '&gt;', 'manual' ),
    ) ); */
	$GLOBALS['wp_query']->max_num_pages = $the_query->max_num_pages;
	if (  $the_query->max_num_pages > 1){
		echo '<div class="misha_loadmore" data-max-pages="'.$the_query->max_num_pages.'">Load More</div>';
	}
	wp_reset_postdata();
	wp_reset_query();
	?>
  <div class="clearfix"></div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>