<?php global $theme_options; ?>
<input type="hidden" id="oldplacvalue" value="<?php echo $theme_options['global-search-text-paceholder']; ?>">
<!--  -->
 <form role="search" method="get" id="searchformnew" class="searchformnew" action="<?php echo get_site_url(); ?>">
  <div class="form-group">
  	<?php 
	  	$args = array(
			'show_option_none' => __( 'Select category' ),
			'option_none_value'  => '',
			'show_count'       => 0,
			'orderby'          => 'name',
			'echo'             => 0,
			'taxonomy'           => 'manualknowledgebasecat',
		);
		?>

		<?php $select  = wp_dropdown_categories( $args ); 
			 echo $select;
		?>
    <!-- <input type="text"  placeholder="<?php echo $theme_options['global-search-text-paceholder']?>" value="<?php echo get_search_query(); ?>" name="s" id="s" class="form-control" /> -->
    <?php if($_GET['cat'] != ''){
    	$args = array(
		'post_type' => 'manual_kb',
		'tax_query' => array(
		    array(
		    'taxonomy' => 'manualknowledgebasecat',
		    'field' => 'term_id',
		    'terms' => $_GET['cat']
		     )
		  )
		);

		$the_query = new WP_Query( $args );
		if($the_query->have_posts()){
			?>
			<select name="s" id="s">
				<option value=""><?php echo $theme_options['global-search-text-paceholder']?></option>
				<?php 
				while ($the_query->have_posts()) {
					$the_query->the_post(); 
					if($_GET['s'] == get_the_title()){
					 	$selected = 'selected';
					}else{$selected = '';}
					?>
					<option value="<?php the_permalink(); ?>" <?php echo $selected; ?>><?php the_title();?></option>
				<?php } ?>
			</select>
		<?php wp_reset_query(); wp_reset_postdata(); }
    } else{ ?>
	    <select name="s" id="s" disabled="disabled">
	    	<option value=""><?php echo $theme_options['global-search-text-paceholder']?></option>
	    </select>
    <?php } ?>
    <input type="submit" class=" button button-custom" value="<?php echo ($theme_options['global-buttom-search-text-paceholder']?$theme_options['global-buttom-search-text-paceholder']:'Search'); ?>">
  </div>
</form>