function url_query( query ) {
    query = query.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
    var expr = "[\\?&]"+query+"=([^&#]*)";
    var regex = new RegExp( expr );
    var results = regex.exec( window.location.href );
    if ( results !== null ) {
        return results[1];
    } else {
        return false;
    }
}

jQuery(function($){
    $('.misha_loadmore').click(function(){
 
        var button = $(this),
            data = {
            'action': 'loadmore',
            'page' : loadmore_params.current_page,
            'cat' : loadmore_params.cat,
            's' : loadmore_params.s
        };
        var maxPages =  $(this).attr('data-max-pages');
        $.ajax({
            url : loadmore_params.ajaxurl, // AJAX handler
            data : data,
            type : 'POST',
            beforeSend : function ( xhr ) {
                button.text('Loading...'); // change the button text, you can also add a preloader image
            },
            success : function( data ){
                if( data ) { 
                    button.text( 'Load More' ).prev().before(data); // insert new posts
                    loadmore_params.current_page++;
        
                    if ( loadmore_params.current_page == maxPages) 
                        button.remove(); // if last page, remove the button
                    // you can also fire the "post-load" event here if you use a plugin that requires it
                    // $( document.body ).trigger( 'post-load' );
                } else {

                    button.remove(); // if no data, remove the button as well
                }
            }
        });
    });
  /* $('select#cat').change(function(){
        var catvalue = $(this).val();
        data = {
            'action': 'autoloadposts',
            'cat' : catvalue
        };
        $.ajax({
            url : loadmore_params.ajaxurl, // AJAX handler
            data : data,
            type : 'POST',
            success : function(data){
                //console.log(data);
                if(data){
                    var posts = jQuery.parseJSON(data);
                    jQuery( 'input[name="s"]' ).autocomplete({
                         source: posts,
                         minLength: 2,
                         select: function(event, ui) {
                             var permalink = ui.item.permalink; // Get permalink from the datasource
                 
                             //window.location.replace(permalink);
                         }
                     });
                    //});   
                }
            }
        });
    });*/
    $('select#cat').change(function(){
        var catvalue = $(this).val();
        data = {
            'action': 'autoloadposts',
            'cat' : catvalue
        };
        $.ajax({
            url : loadmore_params.ajaxurl, // AJAX handler
            data : data,
            type : 'POST',
            success : function(data){
                //console.log(data);
                if(data){
                    var posts = jQuery.parseJSON(data);
                    var options = $("select#s");
                    options.removeAttr('disabled');
                    options.find('option[value!=""]').remove();
                    var title = url_query('s');
                    $("select#s option[value='"+title+"']").attr('selected', 'selected');
                    $.each( posts, function(key, val) {
                        if(val.label == title){
                            var selected = true;
                        }else{
                            selected = false;
                        }
                        options.append(new Option(val.label, val.permalink,'', selected));
                        
                    });
                    
                }
            }
        });
    });
});

jQuery(document).ready(function(){
    jQuery('input.button.button-custom').on("click", function(event){
        event.preventDefault(); 
        var url = jQuery('#s').val();
        console.log(url);
        window.location = url;
    });
});