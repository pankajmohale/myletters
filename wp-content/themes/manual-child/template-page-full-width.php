<?php
/**
 * Template Name: Categories Page
 */

get_header(); 
?>
<!-- /start container -->
<div class="container content-wrapper body-content">
	<div class="row margin-top-btm-50" style="margin-top:35px!important;">
	 	<?php
	 		$categories = get_terms( 'manualknowledgebasecat', array(
			    'orderby' => 'name',
            	'order' => 'ASC',
			    'hide_empty' => 0,
			) );	
	 	?>
		<div class="col-md-12 col-sm-12">
		 	<ul class="categories-list-wrapper">
			 	<?php
					foreach ( $categories as $term ) {
						// The $term is an object, so we don't need to specify the $taxonomy.
						$term_link = get_term_link( $term );

						// If there was an error, continue to the next term.
						if ( is_wp_error( $term_link ) ) {
							continue;
						}

						// We successfully got a link. Print it out.
						echo '<li class="categories-list"><a href="' . esc_url( $term_link ) . '">' . $term->name . '</a></li>';
					} 
			 	?>
			</ul>
		</div>
<?php get_footer(); ?>
