<?php 
get_header(); 
?>
<!-- /start container -->
<div class="container content-wrapper body-content">
<div class="row margin-top-btm-50">
<div class="col-md-12 col-sm-12 faq">
  <h2 class="singlepg-font">
    <?php the_title(); ?>
  </h2>
  <?php while ( have_posts() ) : the_post(); ?>
   <div class="entry-content clearfix">
    <?php the_content(); ?>
    <?php edit_post_link( esc_html__( 'Edit', 'manual' ), '<p class="edit-link" style="text-align:right">', '</p>', $post->ID ); ?>
  </div>
  <?php endwhile; // end of the loop. ?>
  <div style="clear:both"></div>
  <br>
</div>
<?php get_footer(); ?>
