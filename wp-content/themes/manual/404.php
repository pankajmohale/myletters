<?php get_header(); ?>
<!-- /start container -->
<div class="container content-wrapper body-content">
<div class="row margin-top-btm-50">
<div class="col-md-12 col-sm-12">
  <div class="empty404 margin-top-btm-50">
    <h2> <?php echo ($theme_options['home-404-body-main-title']?$theme_options['home-404-body-main-title']:'Oops! That page can’t be found'); ?></h2>
    <p> <?php echo ($theme_options['home-404-body-main-subtitle-title']?$theme_options['home-404-body-main-subtitle-title']:'It looks like nothing was found at this location. Maybe try a search?'); ?></p>
  </div>
</div>
<?php get_footer(); ?>