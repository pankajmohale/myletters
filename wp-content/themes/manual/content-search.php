<?php global $theme_options; ?>
<div class="search <?php echo get_post_type(); ?>" id="post-<?php the_ID(); ?>">
  <div class="caption">
    <?php the_title( sprintf( '<h4><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h4>' ); ?>
    <p>
      <?php if( $theme_options['searchpg-records-publish-date'] == true ) { ?>
      <i class="fa fa-calendar"></i> <span>
      <?php the_time( get_option('date_format') ); ?>
      </span>
      <?php } ?>
      <?php if( $theme_options['searchpg-records-author-name'] == true ) { ?>
      <i class="fa fa-user"></i> <span>
      <?php $author_id = $post->post_author; echo the_author_meta( $theme_options['searchpg-records-post-user-name'] , $author_id ); ?>
      </span>
      <?php } ?>
      <?php 
		if ( 'post' == get_post_type() ) { 
			edit_post_link( esc_html__( 'Edit', 'manual' ), '<i class="fa fa-edit"></i> <span class="edit-link">', '</span>' );
		} else {
			edit_post_link( esc_html__( 'Edit', 'manual' ), '<i class="fa fa-edit"></i> <span class="edit-link">', '</span>' );
		}
		?>
    </p>
    <?php 
	 if( $theme_options['searchpg-display-post-content'] == true ) { 
		 foreach ( $theme_options['searchpg-display-post-content-on-post-type']  as $post_type ) {
			 if( $post_type == get_post_type( $post ) ) {
			 echo '<p class="search-content">';
			 $content = get_the_content($post->ID); 
			 echo substr(strip_tags($content), 0, ($theme_options['searchpg-display-post-content-character']?$theme_options['searchpg-display-post-content-character']:200) );
			 echo '</p>';
			 }
		}
	 }
	 ?>
  </div>
</div>