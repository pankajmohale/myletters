<?php 
/**
 * woo page
 */
 
get_header();

global $theme_options;

if( $theme_options['woo_display_sidebar_on_product_listing_page'] == true && is_shop() ) {
	$md_sm = 8;
	$sidebar_woo = true;
} else {
	$md_sm = 12;
	$sidebar_woo = false;
}
?>

<!-- /start container -->
<div class="container content-wrapper body-content">
<div class="row margin-top-btm-50">
<div class="col-md-<?php echo $md_sm; ?> col-sm-<?php echo $md_sm; ?>">
  <?php woocommerce_content(); ?>
  <div class="clearfix"></div>
</div>

<?php if( $sidebar_woo == true ) { ?>
<!--sidebar-->
<aside class="col-md-4 col-sm-4" id="sidebar-box">
  <div class="custom-well sidebar-nav blankbg">
    <?php 
    if ( is_active_sidebar( 'manual-woocommerce-widget' ) ) : 
		dynamic_sidebar( 'manual-woocommerce-widget' ); 
    endif; 
	?>
  </div>
</aside>
<!--eof sidebar-->
<?php } ?>

<?php get_footer(); ?>