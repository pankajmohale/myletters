<?php 
function manual_doc_post(){
	
	global $theme_options, $post, $withcomments;
	
	/******
	*** wpstickies Compatible
	******/
	if ( function_exists ( 'wpstickies_js' ) ) {
		wpstickies_js();
	}
	
	if( $theme_options['fix_documentation_busted_msg'] == 0 || $theme_options['fix_documentation_busted_msg'] == '' ) {
		// Check for nonce security
		$nonce = $_POST['nonce'];
		if ( ! wp_verify_nonce( $nonce, 'doc-ajax-nonce' ) )
			die ( 'Busted!');
	}
		
		
    if(isset($_POST['post_id']))
    {
		?>
<script>

<?php 
/******
*** print-o-matic
******/
if (class_exists('WP_Print_O_Matic')) {
?>
jQuery(document).ready(function() {
	jQuery('.printomatic, .printomatictext').click(function() {
		var id = jQuery(this).attr('id');
		var target = jQuery(this).data('print_target');
		if(!target){
			target = jQuery('#target-' + id).val();
		}
		if (target == '%prev%') {
			target = jQuery(this).prev();
		}
		if (target == '%next%') {
			target = jQuery(this).next();
		}

		var w = window.open('', 'printomatic print page', 'status=no, toolbar=no, menubar=no, location=no');

		var print_html = '<!DOCTYPE html><html><head><title>' + document.getElementsByTagName('title')[0].innerHTML + '</title>';
		if ( typeof print_data != 'undefined' && typeof print_data[id] != 'undefined'){

			if ( 'pom_site_css' in print_data[id] && print_data[id]['pom_site_css'] ){
				print_html += '<link rel="stylesheet" type="text/css" href="' + print_data[id]['pom_site_css'] + '" />';
			}

			if ( 'pom_custom_css' in print_data[id] && print_data[id]['pom_custom_css']){
				print_html += '<style>'+ print_data[id]['pom_custom_css'] +'</style>';
			}

			//build the blank page
			w.document.open();
			w.document.write( print_html + '</head><body></body></html>');
			w.document.close();

			if ( 'pom_do_not_print' in print_data[id] && print_data[id]['pom_do_not_print'] ){
				jQuery(print_data[id]['pom_do_not_print']).hide();
			}

			if ( 'pom_html_top' in print_data[id] && print_data[id]['pom_html_top']){
				jQuery(w.document.body).html( print_data[id]['pom_html_top'] );
			}

		}

		var ua = window.navigator.userAgent;
		var ie = true;

		//rot in hell IE
		if ( ua.indexOf("MSIE ") != -1) {
			//alert('MSIE - Craptastic');
			jQuery(w.document.body).append( jQuery( target ).clone( true ).html() );
		}
		else if ( ua.indexOf("Trident/") != -1) {
			//console.log('IE 11 - Trident');
			jQuery(w.document.body).append( jQuery( target ).clone( true ).html() );
		}
		else if ( ua.indexOf("Edge/") != -1 ){
			//console.log('IE 12 - Edge');
			//there is a bug in Edge where no nested elements can be appended.
			jQuery( target ).each(function(){
				var s = jQuery.trim( jQuery( this ).clone( true ).html() );
				jQuery( w.document.body ).append( s );
			});
		}
		else{
			//console.log('good browser');
			jQuery(w.document.body).append( jQuery( target ).clone( true ) );
			ie = false;
		}

		if ( typeof print_data != 'undefined' && typeof print_data[id] != 'undefined'){
            if ( 'pom_do_not_print' in print_data[id] ){
                jQuery( print_data[id]['pom_do_not_print']).show();
            }

            if ( 'pom_html_bottom' in print_data[id] && print_data[id]['pom_html_bottom']){
				jQuery(w.document.body).append( jQuery.trim( print_data[id]['pom_html_bottom'] ) );
			}
		}

		//for IE cycle through and fill in any text input values... rot in hell IE
		if(ie){
			jQuery( target ).find('input[type=text]').each(function() {
				var user_val = jQuery(this).val();
				if(user_val){
					var elem_id = jQuery(this).attr('id');
					if(elem_id){
						w.document.getElementById(elem_id).value = user_val;
					}
					else{
						//we really should have a ID, let's try and grab the element by name attr.
						var elem_name = jQuery(this).attr('name');
						if(elem_name.length){
							named_elements = w.document.getElementsByName(elem_name);
							named_elements[0].value = user_val;
						}
					}
				}
			});
		}

		/* hardcodeed iframe and if so, force a pause... pro version offers more options */

		iframe = jQuery(w.document).find('iframe');
		if (iframe.length && typeof print_data != 'undefined' && typeof print_data[id] != 'undefined') {
            if('pom_pause_time' in print_data[id] && print_data[id]['pom_pause_time'] < 3000){
                print_data[id]['pom_pause_time'] = 3000;
            }
            else if(print_data[id]['pom_pause_time'] === 'undefined'){
                print_data[id]['pom_pause_time'] = 3000;
            }
		}

        if(typeof print_data != 'undefined' && typeof print_data[id] != 'undefined' && 'pom_pause_time' in print_data[id] && print_data[id]['pom_pause_time'] > 0){
            pause_time = setTimeout(printIt, print_data[id]['pom_pause_time']);
        }
		else{
			printIt();
		}

		function printIt(){
			w.focus();
		    w.print();

			if('pom_close_after_print' in print_data[id] && print_data[id]['pom_close_after_print'] == '1'){
				//need a bit of a pause to let safari on iOS render the print privew properly
				setTimeout(
					function() {
						w.close()
					}, 1000
				);
			}
		}

	});

});
(function (original) {
  jQuery.fn.clone = function () {
    var result           = original.apply(this, arguments),
        my_textareas     = this.find('textarea').add(this.filter('textarea')),
        result_textareas = result.find('textarea').add(result.filter('textarea')),
        my_selects       = this.find('select').add(this.filter('select')),
        result_selects   = result.find('select').add(result.filter('select'));

    for (var i = 0, l = my_textareas.length; i < l; ++i) jQuery(result_textareas[i]).val(jQuery(my_textareas[i]).val());
    for (var i = 0, l = my_selects.length;   i < l; ++i) {
      for (var j = 0, m = my_selects[i].options.length; j < m; ++j) {
        if (my_selects[i].options[j].selected === true) {
          result_selects[i].options[j].selected = true;
        }
      }
    }
    return result;
  };
}) (jQuery.fn.clone);	
var print_data = {"id<?php echo $_POST['post_id']; ?>":{"pom_site_css":"","pom_custom_css":"","pom_html_top":"","pom_html_bottom":"","pom_do_not_print":"","pom_pause_time":"","pom_close_after_print":"1"}};	
<?php 
}
?>


jQuery(document).ready(function() {
	
    jQuery(".post-like a").click(function(){ 
        heart = jQuery(this);
        // Retrieve post ID from data attribute
        post_id = heart.data("post_id");
        // Ajax call
        jQuery.ajax({
            type: "post",
            url: doc_ajax_var.url,
			data: { action: 'post-like', 
					nonce: doc_ajax_var.nonce,
					post_id: post_id,
					post_like: '',
				  },
			success: function(data, textStatus, XMLHttpRequest){ 
					jQuery( "span.manual_doc_count" ).text(data);
			},
			error: function(MLHttpRequest, textStatus, errorThrown){  
				//alert(textStatus); 
			}
        });
        return false;
    })
	
	
    jQuery(".post-unlike a").click(function(){ 
        heart = jQuery(this);
        // Retrieve post ID from data attribute
        post_id = heart.data("post_id");
        // Ajax call
        jQuery.ajax({
            type: "post",
            url: doc_ajax_var.url,
			data: { action: 'post-unlike', 
					nonce: doc_ajax_var.nonce,
					post_id: post_id,
					post_like: '',
				  },
			success: function(data, textStatus, XMLHttpRequest){ 
					jQuery( "span.manual_doc_unlike_count" ).text(data);
			},
			error: function(MLHttpRequest, textStatus, errorThrown){  
				//alert(textStatus); 
			}
        });
        return false;
    })	
	
	
	// Impression
	var ids = 0;
	jQuery('.manual-views').each(function(){
		imp_postIDs = jQuery(this).attr('id').replace('manual-views-','');
		ids++;
	});
	if(imp_postIDs != '' ) { 
		jQuery.ajax({
				type: "post",
				url: doc_ajax_var.url,
				data: { action: 'manual-doc-impression', 
						nonce: doc_ajax_var.nonce,
						post_id: imp_postIDs,
					  },
				success: function(data, textStatus, XMLHttpRequest){ /*alert(data);*/ },
				error: function(MLHttpRequest, textStatus, errorThrown){ /*alert(textStatus);*/  }
		});
	}
	
})
</script>
<style>
.pull-right.reply {
	display: none;
}
</style>
<?php 
		$post = get_post($_POST['post_id']);
		
		
		// article access
		$access_meta = get_post_meta( $post->ID, 'doc_single_article_access', true );
		$check_post_user_level_access = get_post_meta( $post->ID, 'doc_single_article_user_access', true );
		if( isset($access_meta['login']) && $access_meta['login'] == 1 && !is_user_logged_in() ) {
			?>
<div class="col-md-12 col-sm-12">
  <div class="manual_login_page">
    <div class="custom_login_form">
      <?php if( isset($access_meta['message']) && $access_meta['message'] != '' ) { ?>
      <h3><?php echo stripslashes($access_meta['message']); ?></h3>
      <?php } ?>
      <?php echo ' <form action="' . esc_url( site_url( 'wp-login.php', 'login_post' ) ) . '" method="post"><input type="submit" class="button-primary" value="Log In"></form>'; ?>
      <ul class="custom_register">
        <?php if ( get_option( 'users_can_register' ) ) { wp_register(); } ?>
        <li><a href="<?php echo wp_lostpassword_url(); ?>" title="Lost Password">
          <?php esc_html_e( 'Lost Password', 'manual' ); ?>
          </a></li>
      </ul>
    </div>
  </div>
</div>
<?php 	
		} else {  
			
			if( !empty($check_post_user_level_access) )  $access_status = manual_doc_access(serialize($check_post_user_level_access));
			else  $access_status = true;
			
			if( $access_status == false ) {
				echo '<div class="doc_access_control"><p>';
				echo $theme_options['documentation-single-page-access-control-message'];
				echo '</p></div>';
			} else {	
			// end of article access   
		
			echo '<div id="single-post post-'.$post->ID.'">';
		  	echo '<div class="page-title-header"> 
				   <h2 class="manual-title title singlepg-font">'. $post->post_title .'</h2>';
				   
				   if( $theme_options['documentation-quick-stats-under-title'] == true ) { 
				   	$class_quick_stats_active = 'style="min-height:10px;"';  
				   } else {
				   	$class_quick_stats_active = '';  
				   }
				   
				   echo '<p '.$class_quick_stats_active.'>';
				   if( $theme_options['documentation-quick-stats-under-title'] == false ) {
				   echo '<i class="fa fa-eye"></i> <span>';  
					
					if( get_post_meta( $post->ID, 'manual_post_visitors', true ) != '' ) { 
						echo get_post_meta( $post->ID, 'manual_post_visitors', true );
						echo esc_html_e( ' views ', 'manual' );
					} else { echo '0 views'; } 
					
					echo '</span>';
                   
				   	 if( $theme_options['documentation-singlepg-publish-date-status'] == true ) {
					 echo '<i class="fa fa-calendar"></i> <span>';  echo get_the_date( get_option('date_format'), $post->ID ); echo '</span>';
					 }
					 
					 if( $theme_options['documentation-singlepg-modified-date-status'] == true ) {
					 if (get_post_modified_time(get_option('date_format'),'',$post->ID) != get_the_time(get_option('date_format'),$post->ID) ) { 
						  echo '<i class="fa fa-calendar-plus-o"></i> <span>'.get_post_modified_time(get_option('date_format'),'',$post->ID).'</span>';
					  } 
					 } 
					 
					 if( $theme_options['documentation-disable-doc-author-name'] == true ) {
					 echo '<i class="fa fa-user"></i> <span>';  $author_id = $post->post_author; echo the_author_meta( $theme_options['documentation-single-post-user-name'] , $author_id ); echo '</span>';
					 }
					 
					 echo ' <i class="fa fa-thumbs-o-up"></i> <span>'; if( get_post_meta( $post->ID, 'votes_count_doc_manual', true ) == '' ) { echo 0; } else { echo get_post_meta( $post->ID, 'votes_count_doc_manual', true ); } echo '</span>';
					 
				   }
				   
				   if( $theme_options['documentation-single-pg-print-status'] == true ) {
				   if (class_exists('WP_Print_O_Matic')) { echo do_shortcode('[print-me printstyle="pom-small-grey" tag="span" id="'.$post->ID.'" target=".entry-content"]'); echo '<span></span>'; }
				   }
				    
				   edit_post_link( esc_html__( 'Edit', 'manual' ), '<i class="fa fa-edit"></i> <span class="edit-link">', '</span>', $post->ID );
                   
				  echo '</p>
				 </div><div class="post-cat margin-btm-35"></div>
				 <div class="entry-content clearfix">'.apply_filters('the_content', $post->post_content).'</div>';
				 
				  
					if( get_post_meta( $post->ID, '_manual_attachement_access_status', true ) == true && !is_user_logged_in() ) {
						$message = get_post_meta( $post->ID, '_manual_attachement_access_login_msg', true );
						manual_access_attachment($message, 1);
					} else { 
						manual_kb_attachment_files($post->ID); 
					}
				 
				  
				// manual_kb_attachment_files($post->ID);
				 echo '<div style="clear:both"></div>';
					if( $theme_options['documentation-social-share-status'] == false ) {
					manual_social_share(get_permalink($post->ID));
					}

					if( ($theme_options['documentation-voting-buttons-status'] == false && $theme_options['documentation-voting-login-users'] == false ) ||
						($theme_options['documentation-voting-buttons-status'] == false && $theme_options['documentation-voting-login-users'] == true && is_user_logged_in())
					) {
					echo '<div class="panel-heading" style="padding:0px;">
						<div id="rate-topic-content" class="row-fluid">
							<div class="rate-buttons"> ';
					if(isset($theme_options['yes-no-above-message'])) { echo '<p class="helpfulmsg" >'.$theme_options['yes-no-above-message'].'</p>'; }
                    
			echo ' <span class="post-like"><a data-post_id="'.$post->ID.'" href="#"><span class="btn btn-success rate custom-like-dislike-btm" data-rating="1"><i class="glyphicon glyphicon-thumbs-up"></i> <span class="manual_doc_count">'. $meta_values = get_post_meta( $post->ID, 'votes_count_doc_manual', true ).' '.$theme_options['yes-user-input-text'].'</span></span></a></span>&nbsp;';		
			
			echo '<span class="post-unlike"><a data-post_id="'. $post->ID .'" href="#"><span class="btn btn-danger rate custom-like-dislike-btm" data-rating="0">
                <i class="glyphicon glyphicon-thumbs-down"></i> <span class="manual_doc_unlike_count">'. $meta_values = get_post_meta( $post->ID, 'votes_unlike_doc_manual', true ) .' '.$theme_options['no-user-input-text'].' </span></span></a></span> ';		
							
							
			echo '</div>';
			
			if( is_super_admin() && is_user_logged_in() ) {
				echo '<span class="post-reset"><a data-post_id="'.$post->ID.'" href="#"><span class="btn btn-link" data-rating="0"> <i class="fa fa-refresh"></i> <span class="rating_reset_display"> Reset </span></span></a></span>';
			}
			
			echo '</div>
					<div class="clearfix"></div>
					<span class="manual-views" id="manual-views-'.$post->ID.'"></span>
				</div>';						  
					}
					
		echo '</div>';
		
		if( $theme_options['documentation-related-post-status'] == true ) manual_doc_related_post($post->ID);
		
		// article access
		}
		}
		// eof article access
		
		/*if( $theme_options['documentation-comment-status'] == true ) {
			if ( comments_open() || get_comments_number() ) { 
				$withcomments = true;
				comments_template( '', true ); 
			}
		}*/
		
	}
	 exit;
}
add_action('wp_ajax_nopriv_display-doc-post', 'manual_doc_post');
add_action('wp_ajax_display-doc-post', 'manual_doc_post');
?>