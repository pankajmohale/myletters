<?php 

class manual_Customize {

		public static function register( $wp_customize ) {
		
		} // eof register
		
		/**
		* This will output the custom WordPress settings to the live theme's WP head.
		*/
	    public static function header_output() {
		  $theme_nav_type = '';
		  global $post, $theme_options; 
		  ?>
			<style type="text/css">
				<?php if( isset($theme_options['manual-global-color-link']['rgba']) && $theme_options['manual-global-color-link']['rgba'] != '' ) { ?>
.custom-link, .custom-link-blog, .more-link, .load_more a {color: <?php echo $theme_options['manual-global-color-link']['rgba']; ?> !important;}.custom-link:hover, .custom-link-blog:hover, .more-link:hover, .load_more a:hover { color: <?php echo $theme_options['manual-global-color-link-hover']['rgba']; ?> !important; }
				<?php } ?>
				<?php if( isset($theme_options['manual-global-color-botton']['rgba']) && $theme_options['manual-global-color-botton']['rgba'] != '' ) { ?>
				.button-custom, p.home-message-darkblue-bar, p.portfolio-des-n-link, .portfolio-section .portfolio-button-top, .body-content .wpcf7 input[type="submit"], .container .blog-btn, .sidebar-widget.widget_search input[type="submit"], .navbar-inverse .navbar-toggle, .custom_login_form input[type="submit"], .custom-botton, button#bbp_user_edit_submit, button#bbp_topic_submit, button#bbp_reply_submit, button#bbp_merge_topic_submit, .bbp_widget_login button#user-submit, input[type=submit] {background-color: <?php echo $theme_options['manual-global-color-botton']['rgba']; ?> !important;}
				.navbar-inverse .navbar-toggle, .container .blog-btn,input[type=submit] { border-color: <?php echo $theme_options['manual-global-color-botton']['rgba']; ?>!important;}
				.button-custom:hover, p.home-message-darkblue-bar:hover, .body-content .wpcf7 input[type="submit"]:hover, .container .blog-btn:hover, .sidebar-widget.widget_search input[type="submit"]:hover, .navbar-inverse .navbar-toggle:hover, .custom_login_form input[type="submit"]:hover, .custom-botton:hover, button#bbp_user_edit_submit:hover, button#bbp_topic_submit:hover, button#bbp_reply_submit:hover, button#bbp_merge_topic_submit:hover, .bbp_widget_login button#user-submit:hover, input[type=submit]:hover{  background-color: <?php echo $theme_options['manual-global-color-botton-hover']['rgba']; ?> !important; }
				
				<?php } ?>
				.footer-go-uplink { color:<?php echo (!empty($theme_options['manual-go-up-icon-color']['rgba'])?$theme_options['manual-go-up-icon-color']['rgba']:''); ?>!important; font-size: <?php echo $theme_options['go_up_arrow_font_size']; ?>px!important; }
				<?php if( isset($theme_options['manual-hover-icon-color']['rgba']) && $theme_options['manual-hover-icon-color']['rgba'] != '' ) { ?>
				.browse-help-desk .browse-help-desk-div .i-fa:hover, ul.news-list li.cat-lists:hover:before, .body-content li.cat.inner:hover:before, .kb-box-single:hover:before {color:<?php echo $theme_options['manual-hover-icon-color']['rgba']; ?>; } .social-share-box:hover { background:<?php echo $theme_options['manual-hover-icon-color']['rgba']; ?>; border: 1px solid <?php echo $theme_options['manual-hover-icon-color']['rgba']; ?>; }
				<?php } ?>
				
				<?php 
				$display_on_forum_page_head_call = '';
				if( $theme_options['manual-trending-post-type-search-status-on-forum-pages'] == false ) {
					if( get_post_type() == 'forum' ) $display_on_forum_page_head_call = 1;
				} else {
					 $display_on_forum_page_head_call = 2;
				}
				if ( $theme_options['manual-trending-post-type-search-status'] == true && ($display_on_forum_page_head_call != 1) ){ 
					if ( !is_rtl() ) {?>
				.form-control.header-search.search_loading { background: #fff url("<?php echo trailingslashit( get_template_directory_uri() ); ?>img/loader.svg") no-repeat right 255px center!important; } @media (max-width:767px) { .form-control.header-search.search_loading { background: #fff url("<?php echo trailingslashit( get_template_directory_uri() ); ?>img/loader.svg") no-repeat right 115px center!important; } }
				<?php } else { ?>
				.form-control.header-search.search_loading { background: #fff url("<?php echo trailingslashit( get_template_directory_uri() ); ?>img/loader.svg") no-repeat right 645px center!important; } @media (max-width:767px) { .form-control.header-search.search_loading { background: #fff url("<?php echo trailingslashit( get_template_directory_uri() ); ?>img/loader.svg") no-repeat right 465px center!important; } } @media (min-width:767px) {  .form-control.header-search.search_loading { background: #fff url("<?php echo trailingslashit( get_template_directory_uri() ); ?>img/loader.svg") no-repeat right 645px center!important; }  }
					
				<?php 	}
				} ?>
				
				
				<?php 
				// menu navigation style control  255
				manual_menu_navigation_control();
			    // control header
				manual_header_control_global();
				// remaining global
				manual_customizer_enhance(); 
				?>
				
				<?php 
				$check_current_page = basename( get_page_template() );
				if($check_current_page == 'template-home.php' && $theme_options['home-switch-to-page-header-status'] == true) { ?>
.trending-search span.popular-keyword-title {color: #D6D6D6;}.trending-search a {color: #C5C5C5!important;} .jumbotron .jumbotron-bg-color { background-color: rgba(0, 0, 0, 0.2);} .navbar { background: rgba(53, 55, 62, 0.2)!important; z-index: 999; } @media (max-width: 991px) and (min-width: 768px) { .navbar { background:#FFFFFF!important; } }  @media (max-width: 767px){  .navbar { background:#FFFFFF!important; }  };
				<?php } ?>
				
				
            </style>
          <?php
	    }
		
		public static function generate_css( $selector, $style, $mod_name, $prefix='', $postfix='', $echo=true ) {
		  $return = '';
		  $mod = get_theme_mod($mod_name);
		  if ( ! empty( $mod ) ) {
			 $return = sprintf('%s { %s:%s; }',
				$selector,
				$style,
				$prefix.$mod.$postfix
			 );
			 if ( $echo ) {
				echo $return;
			 }
		  }
		  return $return;
		}
		
		
	   /**
		* This outputs the javascript needed to automate the live settings preview.
		*/
	   public static function live_preview() {
		  wp_enqueue_script( 
			   'manual-themecustomizer', // Give the script a unique ID
			   get_template_directory_uri() . '/js/customize-preview.js', // Define the path to the JS file
			   array(  'jquery', 'customize-preview' ), // Define dependencies
			   '', // Define a version (optional) 
			   true // Specify whether to put in footer (leave this true)
		  );
	   }
		

}
// Setup the Theme Customizer settings and controls...
add_action( 'customize_register' , array( 'manual_Customize' , 'register' ) );
// Output custom CSS to live site
add_action( 'wp_head' , array( 'manual_Customize' , 'header_output' ) );
// Enqueue live preview javascript in Theme Customizer admin screen
add_action( 'customize_preview_init' , array( 'manual_Customize' , 'live_preview' ) );
?>