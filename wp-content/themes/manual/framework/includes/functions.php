<?php 
/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: BBPRESS SEARCH HACK
/*-----------------------------------------------------------------------------------*/

/**
 * Include bbPress 'topic' custom post type in WordPress' search results
 */
if (!function_exists('manual_bbp_topic_search')) { 
	function manual_bbp_topic_search( $topic_search ) {
		$topic_search['exclude_from_search'] = false;
		return $topic_search;
	}
	add_filter( 'bbp_register_topic_post_type', 'manual_bbp_topic_search' );
}

/**
 * Include bbPress 'reply' custom post type in WordPress' search results
 */
if (!function_exists('manual_bbp_reply_search')) {
	function manual_bbp_reply_search( $reply_search ) {
		$reply_search['exclude_from_search'] = false;
		return $reply_search;
	}
	add_filter( 'bbp_register_reply_post_type', 'manual_bbp_reply_search' );
}

/**
 * Include bbPress 'forum' custom post type in WordPress' search results 
 */
if (!function_exists('manual_bbp_forum_search')) {
	function manual_bbp_forum_search( $forum_search ) {
		$forum_search['exclude_from_search'] = false;
		return $forum_search;
	}
	add_filter( 'bbp_register_forum_post_type', 'manual_bbp_forum_search' );
}


/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: TRENDING SEARCH
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_trending_search')) {
	function manual_trending_search() {
		global $theme_options;
		$search_keywords = array();
		if( isset($theme_options['manual-trending-live-search-status']) && $theme_options['manual-trending-live-search-status'] == true ){ 
			echo '<div class="trending-search">';
			if( isset($theme_options['manual-trending-text']) ){ echo '<span class="popular-keyword-title">'.$theme_options['manual-trending-text'].'</span>';  }  
			if( isset($theme_options['manual-three-trending-search-text']) ) {
				foreach( $theme_options['manual-three-trending-search-text'] as $val ) {
					if( empty($val) ) continue;
					$search_keywords[] = '<a href="" class="trending-search-popular-keyword">'.$val.'</a>';
				}
				echo implode('<span class="comma">,</span> ', $search_keywords );
			}
			echo '</div>';
		}
	}
}


/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: LIVE SEARCH - DISPLAY NAVIGATION
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_live_search_navigation')) {
	function manual_live_search_navigation($getID, $post_type) { 
	global $theme_options;
		if( $theme_options['manual-live-search-post-navigation-status'] == true ) {
		 echo '<div class="live_search_navigation">';
					if( $post_type == 'post') {
						$categories = get_the_category( $getID );
						$count_cat_loop = count($categories);
						if ( ! empty( $categories ) ) {
							$i = 0;
							foreach( $categories as $category ) {
								if($i == 1 && $count_cat_loop > $i ) { echo ', ...'; break; }
								echo 'Post / '.$category->name;
								$i++;
							}
						}
					} else if( $post_type == 'manual_documentation' || $post_type == 'manual_faq' || $post_type == 'manual_kb'  ) {
						if( $post_type == 'manual_documentation') { 
							$categories = get_the_terms( $getID, 'manualdocumentationcategory' );
							$breadcrumb_name = $theme_options['doc-breadcrumb-name'];
						} else if( $post_type == 'manual_faq') { 
							$categories = get_the_terms( $getID, 'manualfaqcategory' );
							$breadcrumb_name = 'Faq';
						} else { 
							$categories = get_the_terms( $getID, 'manualknowledgebasecat' );
							$breadcrumb_name = $theme_options['kb-breadcrumb-name'];
						}
						$count_cat_loop = count($categories);
						if ( ! empty( $categories ) ) {
							$i = 0;
							foreach( $categories as $category ) {
								if($i == 1 && $count_cat_loop > $i ) { echo ', ...'; break; }
								echo $breadcrumb_name.' / '.$category->name;
								$i++;
							}
						}
					}
			echo '</div>';
		}
	}
}

/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: LIVE SEARCH URL PROCESS
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_site_root_url_process')) {
	function manual_site_root_url_process() {
		$ajax_live_search_url = home_url('/'); 
		$ajax_live_search_url_query_str = parse_url($ajax_live_search_url, PHP_URL_QUERY);
		$only_site_url = preg_replace('/\\?.*/', '', $ajax_live_search_url);
		if($ajax_live_search_url_query_str != '' ) {
			$live_search_final_url = $only_site_url.'?'.$ajax_live_search_url_query_str.'&ajax=on&s=';
		} else {
			$live_search_final_url = $only_site_url.'?ajax=on&s=';
		}
		return $live_search_final_url;
	}
}


/*-----------------------------------------------------------------------------------*/
/*	MANUAL RESPONSIVE LAYOUT "BAR ICON REPLACEMENT"
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_responsive_layout_bar_icon_replacement')) {
	function manual_responsive_layout_bar_icon_replacement() { 
		global $theme_options;
		if( $theme_options['theme-responsive-bar-icon-replacement'] == false ) { 
			$barname = 'fa fa-bars';
			$replacement_html_text = $css_call = '';
		} else { 
			$barname = ''; 
			$replacement_html_text = $theme_options['theme-responsive-bar-icon-replacement-text'];
			$css_call = 'style="font-style:normal;cursor:pointer;color:#FFFFFF;font-weight:'.$theme_options['first-level-menu-weight'].';font-size:'.$theme_options['first-level-menu-font-size'].'px;text-transform:'.$theme_options['first-level-menu-text-transform'].'"';
		}
		echo '<i class="'.$barname.' navbar-toggle" '.$css_call.'>'.$replacement_html_text.'</i>';
	}
}


/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: ADVANCE SEARCH
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_shFilter')) {
	function manual_shFilter($query) {  
		if( isset($_GET['post_type']) && $_GET['post_type'] != '' ) {
			if ($query->is_search){  
				$query->set('post_type', $_GET['post_type'] ); 
			} 
		}
		return $query;  
	}  
	add_filter('pre_get_posts', 'manual_shFilter'); 
}

if (!function_exists('manual_attachment_sh_clauses')) {
	function manual_attachment_sh_clauses( $pieces ) {  
		global $wp_query, $wpdb, $theme_options;
		$vars = $wp_query->query_vars;
		if ( empty( $vars ) ) {
			$vars = ( isset( $_REQUEST['query'] ) ) ? $_REQUEST['query'] : array();
		}
		
		// Rewrite the where clause
		if ( ( isset($_GET['post_type']) &&  ($_GET['post_type'] == '')   ) ) { 
		
			if( isset( $theme_options['manual-default-search-type-multi-select'] ) ) { 
				$where = $post_typeIN = '';
				$query_where = array();
				$count = 1;
				foreach ( $theme_options['manual-default-search-type-multi-select']  as $post_type ) {
					if( $count == 1 ) $comma = '';
					else $comma = ',';
					
					$post_typeIN .= "".$comma."'".$post_type."'";
					
					if( $post_type == 'attachment' ) $add_attachOR = " OR $wpdb->posts.post_status = 'inherit' ";
					else $add_attachOR = '';
					
					$count++;
				}
				$query_where[] = " AND (($wpdb->posts.post_title LIKE '%".$_GET['s']."%') OR ($wpdb->posts.post_excerpt LIKE '%".$_GET['s']."%') OR ($wpdb->posts.post_content LIKE '%".$_GET['s']."%')) AND $wpdb->posts.post_type IN ( ".$post_typeIN." ) AND ($wpdb->posts.post_status = 'publish' OR $wpdb->posts.post_status = 'closed' ".$add_attachOR.")";
				$pieces['where'] =  $query_where[0];
			}       
			
		} else if ( ( isset($_GET['post_type']) && 'attachment' == $_GET['post_type'] ) ) { 
			$pieces['where'] = " AND $wpdb->posts.post_type = 'attachment' AND $wpdb->posts.post_status = 'inherit' ";
		}
		
		return $pieces;
	}
	add_filter( 'posts_clauses', 'manual_attachment_sh_clauses');
}



/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: DOCUMENTATION ACCESS CONTROL
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_doc_access')) {
	function manual_doc_access($check_user_role) {
		if ( is_user_logged_in() &&  $check_user_role != '' && !is_super_admin() ) {  
			$value = '';
			$check_roles = unserialize($check_user_role);
			$current_user = wp_get_current_user();
			$wp_role = $current_user->roles;
			foreach ($wp_role as $role_value => $role_name) {
				if ( in_array($role_name, $check_roles) ) {
					$value = 1;
				} else {
					continue;	
				}
			}
			if( $value == 1 ) return true;
			else return false;
		}
		return true;
	}
}


/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: FOOTER
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_footer_notification_msg')) {
	function manual_footer_notification_msg($hide){
		global $theme_options;
		$notification_background = '';
		
		if( $theme_options['footer-notification-status'] == true ) {
			if( !empty($theme_options['footer-notification-bar-background-img']['url']) ) { 
				$notification_background = 'background-image:url('.$theme_options['footer-notification-bar-background-img']['url'].');background-size: cover; background-position: center;';
			} else { 
				if( isset($theme_options['footer-notification-bar-bg-color']['rgba']) && $theme_options['footer-notification-bar-bg-color']['rgba'] != '' ) { 
				$notification_background = 'background:'.$theme_options['footer-notification-bar-bg-color']['rgba'].';'; 
				}
			}
			
			if( $hide == 1 && $theme_options['footer-text'] != '' ) {
			echo '<div id="footer-info" >
				  <div class="bg-color" style="'.$notification_background.'">
					<div class="container">
					  <div class="row  text-padding" style="margin:'.$theme_options['footer-notification-bar-text-margin'].'px 0px">
						<div class="col-md-12 col-sm-12 footer-msg-bar">';
						 if( isset($theme_options['footer-text']) && $theme_options['footer-text'] != '') { echo $theme_options['footer-text']; }
				   echo '</div>
					  </div>
					</div>
				  </div>
				</div>';
			}
		}
	}
}

if (!function_exists('manual_footer_controls')) {
	function manual_footer_controls(){
		global $theme_options, $post;
		
		$current_post_type = get_post_type();
		if( is_404() ) {
			if( $theme_options['404-notofication-bar-status'] == false ) manual_footer_notification_msg(2);
			else manual_footer_notification_msg(1);
		} else if(function_exists("is_woocommerce") && (is_shop() || is_checkout() || is_account_page() || is_product() )){ // woo
			if( $theme_options['woo-hide-notification-bar'] == true ) manual_footer_notification_msg(2);
			else manual_footer_notification_msg(1);
			
		} else if($current_post_type == 'manual_documentation') {
			if( $theme_options['documentation-notification-bar-global'] == true ) manual_footer_notification_msg(2);
			else manual_footer_notification_msg(1);
			
		} else if($current_post_type == 'manual_kb') {
			if( $theme_options['kb-hide-notification-bar'] == true ) manual_footer_notification_msg(2);
			else manual_footer_notification_msg(1);
			
		} else if( is_page() || $current_post_type == 'manual_portfolio' ) { 
			if( get_post_meta( $post->ID, '_manual_footer_force_hide_msg_box', true ) == true ) { 
				manual_footer_notification_msg(2);
			} else { 
				manual_footer_notification_msg(1);
			}
		} else {
			manual_footer_notification_msg(1);
		}
		
	}
}


/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: SOCIAL SHARE CONTROL POST
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_social_share')) {
	function manual_social_share($url){
		global $theme_options;
		if( isset($theme_options['theme-social-box']) && $theme_options['theme-social-box'] == true ) {
			if( isset($theme_options['theme-social-box-mailto-subject']) ){
				$mailto = $theme_options['theme-social-box-mailto-subject'];
			} else {
				$mailto = '';
			}
			
		?>
		<div class="social-box">
		<?php 
		if( !empty($theme_options['theme-social-share-displaycrl-status']) ) {
			foreach ( $theme_options['theme-social-share-displaycrl-status'] as $key => $value ) {
				if( $key == 'linkedin' && $value == 1 ) {
					echo '<a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url='.$url.'"><i class="fa fa-linkedin social-share-box"></i></a>';
				} 
				if( $key == 'twitter' && $value == 1 ) {
					echo '<a target="_blank" href="https://twitter.com/home?status='.$url.'"><i class="fa fa-twitter social-share-box"></i></a>';
				}
				if( $key == 'facebook' && $value == 1 ) {
					echo '<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u='.$url.'" title="facebook"><i class="fa fa-facebook social-share-box"></i></a>';
				}
				if( $key == 'pinterest' && $value == 1 ) {
					echo '<a target="_blank" href="https://pinterest.com/pin/create/button/?url='. $url .'&media=&description="><i class="fa fa-pinterest social-share-box"></i></a>';
				}
				if( $key == 'google-plus' && $value == 1 ) {
					echo '<a target="_blank" href="https://plus.google.com/share?url='.$url.'"><i class="fa fa-google-plus social-share-box"></i></a>';
				}
				if( $key == 'email' && $value == 1 ) {
					echo '<a target="_blank" href="mailto:?Subject='.$mailto.'"><i class="fa fa-envelope-o social-share-box"></i></a>';
				}
			} 
		} 
		?>
		</div>
		<?php 
		}
	}
}


if (!function_exists('manual_get_social_share')) {
	function manual_get_social_share($url){
		global $theme_options;
		if( isset($theme_options['theme-social-box']) && $theme_options['theme-social-box'] == true ) {
			if( isset($theme_options['theme-social-box-mailto-subject']) ){
				$mailto = $theme_options['theme-social-box-mailto-subject'];
			} else {
				$mailto = '';
			}
			
		$return = '<div class="social-box">';
		if( !empty($theme_options['theme-social-share-displaycrl-status']) ) {
			foreach ( $theme_options['theme-social-share-displaycrl-status'] as $key => $value ) {
				if( $key == 'linkedin' && $value == 1 ) {
					$return .= '<a target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&amp;url='.$url.'"><i class="fa fa-linkedin social-share-box"></i></a>';
				} 
				if( $key == 'twitter' && $value == 1 ) {
					$return .= '<a target="_blank" href="https://twitter.com/home?status='.$url.'"><i class="fa fa-twitter social-share-box"></i></a>';
				}
				if( $key == 'facebook' && $value == 1 ) {
					$return .= '<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u='.$url.'" title="facebook"><i class="fa fa-facebook social-share-box"></i></a>';
				}
				if( $key == 'pinterest' && $value == 1 ) {
					$return .= '<a target="_blank" href="https://pinterest.com/pin/create/button/?url='. $url .'&media=&description="><i class="fa fa-pinterest social-share-box"></i></a>';
				}
				if( $key == 'google-plus' && $value == 1 ) {
					$return .= '<a target="_blank" href="https://plus.google.com/share?url='.$url.'"><i class="fa fa-google-plus social-share-box"></i></a>';
				}
				if( $key == 'email' && $value == 1 ) {
					$return .= '<a target="_blank" href="mailto:?Subject='.$mailto.'"><i class="fa fa-envelope-o social-share-box"></i></a>';
				}
			} 
		} 
		$return .= '</div>';
		}
	return $return;	
	}
}


/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: HAMBURGER MENU
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_css_hamburger_menu_control')) {
	function manual_css_hamburger_menu_control(){
		$hamburger_class = '';
		global $theme_options,$post;
		$current_post_type = get_post_type();
		if( $current_post_type == 'page' || $current_post_type == 'manual_portfolio' ) {
			$page_hamburger_menu = get_post_meta( $post->ID, '_manual_header_display_hamburger_bar', true );
			if( $page_hamburger_menu == true ) {
					$hamburger_class = 'hidemenu';
			} else {
				$hamburger_class = '';
			}
		} else {
			$activate_post_type = array();
			if( !empty( $theme_options['target-display-search-box-on-menu-bar'] ) ) {
			foreach ( $theme_options['target-display-search-box-on-menu-bar']  as $post_type ) {
						if( $post_type == 'manual_ourteam' ||
							$post_type == 'manual_tmal_block' ||
							$post_type == 'manual_org_block' ||
							$post_type == 'manual_hp_block'  ||
							$post_type == 'reply' ||
							$post_type == 'topic' || $post_type == 'page' ) { continue; }
							$activate_post_type[] = $post_type;
					}
			}
			if( !empty( $activate_post_type ) && in_array($current_post_type, $activate_post_type) ) {
				if( $theme_options['activate-hamburger-menu'] == true ) {
					$hamburger_class = 'hidemenu';
				} else {
					$hamburger_class = '';
				}
			}
		}
		return $hamburger_class;
	}
}


if (!function_exists('manual_hamburger_menu_control')) {
	function manual_hamburger_menu_control(){
		global $theme_options, $post;
		$current_post_type = get_post_type();
		
		if( $current_post_type == 'page' || $current_post_type == 'manual_portfolio' ) {
			$page_hamburger_menu = get_post_meta( $post->ID, '_manual_header_display_hamburger_bar', true );
			$page_search_on_the_menu = get_post_meta( $post->ID, '_manual_header_display_search_box_on_menu_bar', true );
			if( $page_hamburger_menu == true ) {
				echo '<div class="hamburger-menu">
						<span></span> <span></span> <span></span> <span></span>
					 </div>';
				if( $page_search_on_the_menu == true ) {
					 echo '<div class="form-group menu-bar-form col-md-offset-3">';
						$modern_search_design_header = get_post_meta( $post->ID, '_manual_header_display_search_box_modern_on_menu_bar', true );
						if( $modern_search_design_header == true ) {
							manual_nav_bar_search_normal();
						} else {
							get_template_part( 'search', 'home' );
						}
					 echo '</div>';
				}
			}
		
		} else {
			$activate_post_type = array();
			if( !empty( $theme_options['target-display-search-box-on-menu-bar'] ) ) {
			foreach ( $theme_options['target-display-search-box-on-menu-bar']  as $post_type ) {
						if( $post_type == 'manual_ourteam' ||
							$post_type == 'manual_tmal_block' ||
							$post_type == 'manual_org_block' ||
							$post_type == 'manual_hp_block'  ||
							$post_type == 'reply' ||
							$post_type == 'topic' || $post_type == 'page' ) { continue; }
							$activate_post_type[] = $post_type;
					}
			}
			if( !empty( $activate_post_type ) && in_array($current_post_type, $activate_post_type) ) {
				if( $theme_options['activate-hamburger-menu'] == true ) { 
					echo '<div class="hamburger-menu">
						<span></span> <span></span> <span></span> <span></span>
					</div>';
					
					if( $theme_options['activate-search-box-on-menu-bar'] == true ) {
						 echo '<div class="form-group menu-bar-form col-md-offset-3">';
							if( $theme_options['replace-search-design-with-modern-bar'] == true ) { 
								manual_nav_bar_search_normal();
							} else {
								get_template_part( 'search', 'home' );
							}
						 echo '</div>';
					}
				}
			}
		} // eof page
	}
}

if (!function_exists('manual_nav_bar_search_normal')) {
	function manual_nav_bar_search_normal(){
		global $theme_options;
        echo '<input type="hidden" id="oldplacvalue" value="'.$theme_options['global-search-text-paceholder'].'">
        <form role="search" method="get" id="searchform_nav" class="searchform" action="'.esc_url( home_url( '/' ) ).'">
          <div class="form-group">
            <input type="text"  placeholder="'.$theme_options['global-search-text-paceholder'].'" value="'.get_search_query().'" name="s" id="s" class="form-control header-search custom-simple-header-search" />
            <input type="hidden" value="" name="post_type" id="search_post_type">
            <input type="submit" class=" button button-custom custom-simple-search" value="&#xf002;">
          </div>
        </form>';
	}
}


/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: SOCIAL SHARE CONTROL FOOTER
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_footer_social_share')) {
	function manual_footer_social_share(){
	global $theme_options;
	if( isset($theme_options['footer-social-twitter']) && $theme_options['footer-social-twitter'] != ''  ) {  ?>
        <li><a href="<?php echo $theme_options['footer-social-twitter']; ?>" title="Twitter" target="_blank"><i class="fa fa-twitter social-footer-icon"></i></a></li>
        <?php } ?>
        
        <?php if( isset($theme_options['footer-social-facebook']) && $theme_options['footer-social-facebook'] != ''  ) {  ?>
        <li><a href="<?php echo $theme_options['footer-social-facebook']; ?>" title="Facebook" target="_blank"><i class="fa fa-facebook social-footer-icon"></i></a></li>
        <?php } ?>
        
        <?php if( isset($theme_options['footer-social-youtube']) && $theme_options['footer-social-youtube'] != ''  ) {  ?>
        <li><a href="<?php echo $theme_options['footer-social-youtube']; ?>" title="YouTube" target="_blank"><i class="fa fa-youtube social-footer-icon"></i></a> </li>
        <?php } ?>
        
        <?php if( isset($theme_options['footer-social-google']) && $theme_options['footer-social-google'] != ''  ) {  ?>
        <li><a href="<?php echo $theme_options['footer-social-google']; ?>" title="Google+" target="_blank"><i class="fa fa-google-plus social-footer-icon"></i></a></li>
        <?php } ?>
        
        <?php if( isset($theme_options['footer-social-instagram']) && $theme_options['footer-social-instagram'] != ''  ) {  ?>
        <li><a href="<?php echo $theme_options['footer-social-instagram']; ?>" title="Instagram" target="_blank"><i class="fa fa-instagram social-footer-icon"></i></a></li>
        <?php } ?>
        
        <?php if( isset($theme_options['footer-social-linkedin']) && $theme_options['footer-social-linkedin'] != ''  ) {  ?>
        <li><a href="<?php echo $theme_options['footer-social-linkedin']; ?>" title="Linkedin" target="_blank"><i class="fa fa-linkedin social-footer-icon"></i></a> </li>
        <?php } ?>
        
        <?php if( isset($theme_options['footer-social-pinterest']) && $theme_options['footer-social-pinterest'] != ''  ) {  ?>
        <li><a href="<?php echo $theme_options['footer-social-pinterest']; ?>" title="Pinterest" target="_blank"><i class="fa fa-pinterest social-footer-icon"></i></a> </li>
        <?php } ?>
        
        <?php if( isset($theme_options['footer-social-vimo']) && $theme_options['footer-social-vimo'] != ''  ) {  ?>
        <li><a href="<?php echo $theme_options['footer-social-vimo']; ?>" title="Vimo" target="_blank"><i class="fa fa-vimeo-square social-footer-icon"></i></a> </li>
        <?php } ?>
		
		<?php if( isset($theme_options['footer-social-tumblr']) && $theme_options['footer-social-tumblr'] != ''  ) {  ?>
        <li><a href="<?php echo $theme_options['footer-social-tumblr']; ?>" title="Tumblr" target="_blank"><i class="fa fa-tumblr-square social-footer-icon"></i></a> </li>
        <?php }
	}
}

/*-----------------------------------------------------------------------------------*/
/*	WOOCOMMERSE ::  REPLACE HEADER CSS
/*-----------------------------------------------------------------------------------*/
function manual_woo_shop_column_css_handler(){
	global $theme_options;
	if( $theme_options['woo_column_product_listing'] == 4  ) {
		echo '@media (max-width:767px) { .woocommerce ul.products li.product{ width: 99%; } }';
	} else if( $theme_options['woo_column_product_listing'] == 3  ) {
		echo '.woocommerce ul.products li.product{ width: 30.7%; } @media (max-width:767px) { .woocommerce ul.products li.product{ width: 99.5%; } }';
	}
}


/*-----------------------------------------------------------------------------------*/
/*	HEX 2 RGB
/*-----------------------------------------------------------------------------------*/ 
if (!function_exists('hex2rgb')) {
	function hex2rgb($hex) {
	   $hex = str_replace("#", "", $hex);
	
	   if(strlen($hex) == 3) {
		  $r = hexdec(substr($hex,0,1).substr($hex,0,1));
		  $g = hexdec(substr($hex,1,1).substr($hex,1,1));
		  $b = hexdec(substr($hex,2,1).substr($hex,2,1));
	   } else {
		  $r = hexdec(substr($hex,0,2));
		  $g = hexdec(substr($hex,2,2));
		  $b = hexdec(substr($hex,4,2));
	   }
	   $rgb = array($r, $g, $b);
	   return $rgb; // returns an array with the rgb values
	}
}



/*-----------------------------------------------------------------------------------*/
/*	Google Analytics Code
/*-----------------------------------------------------------------------------------*/ 
if(!function_exists("manual_google_analytics_code")){
    function manual_google_analytics_code($location){
    	global $theme_options;
    	$saved_location = (isset($theme_options['manual-tracking-code-position']) && !empty($theme_options['manual-tracking-code-position']) ? $theme_options['manual-tracking-code-position'] : "");
    	
    	if(!empty($theme_options['manual-google-analytics'])){
    		if($location == "head" && $saved_location == 1) {
                echo "<script type='text/javascript'>"; 
    			echo $theme_options['manual-google-analytics'];
                echo "</script>";

    		} elseif($location == "body" && empty($saved_location)){
                echo "<script type='text/javascript'>"; 
    			echo $theme_options['manual-google-analytics'];
                echo "</script>";
    		}	
    		
    	}
    }
}



/*-----------------------------------------------------------------------------------*/
/*	DOCUMENTATION CAT PAGE
/*-----------------------------------------------------------------------------------*/ 
if (!function_exists('manual_documentation_cat_pages')) {
	function manual_documentation_cat_pages( $post, $post_info, $term_id ) {
		global $theme_options;
		
		if( isset( $theme_options['documentation-record-display-order'] ) && $theme_options['documentation-record-display-order'] != '' ) {
		$display_order_doc = $theme_options['documentation-record-display-order'];	
		} else {
			$display_order_doc = 'ASC';	
		}
		if( isset( $theme_options['documentation-record-display-order-by'] ) && $theme_options['documentation-record-display-order-by'] != '' ) {
			$display_order_doc_by = $theme_options['documentation-record-display-order-by'];	
		}
		
		$children = get_posts( array( 'post_parent' => $post->ID, 
									  'post_type' => $post_info, 
									  'orderby' => $display_order_doc_by,
									  'order' => $display_order_doc,
									  'posts_per_page'   => -1,
									  'taxonomy' => 'manualdocumentationcategory'
									  ));
		$child = count($children);
		if( $child > 0 ) {
			echo '<ul class="parent-display-'.$post->ID.'">';
			foreach($children as $child) :
				$count_child = manual_count_child_post($child, $post_info, $term_id); 
				if( is_object_in_term( $child->ID, 'manualdocumentationcategory', $term_id) === true ): ?>
	
	<li manual-topic-id="<?php echo $child->ID; ?>" > <a href="<?php echo get_permalink($child->ID); ?>" rel="<?php echo $child->ID; ?>" class="post-link <?php if( count($count_child) > 0 ) { echo 'has-inner-child';  } ?>" ><?php echo $child->post_title; ?></a>
	  <?php manual_documentation_cat_pages($child, $post_info, $term_id); ?>
	</li>
	<?php 
				endif;
			endforeach; 
			echo '</ul>';
		}
	}
}


if (!function_exists('manual_count_child_post')) {
	function manual_count_child_post($post, $post_info, $term_id){
		$count = array();
		$children = get_posts( array( 'post_parent' => $post->ID, 'post_type' => $post_info, 'taxonomy' => 'manualdocumentationcategory'  ));
		$child = count($children);
		if( $child > 0 ) {
			foreach($children as $child) : 
				if( is_object_in_term( $child->ID, 'manualdocumentationcategory', $term_id) === true ): 
					$count[] = 1;
				endif;
			endforeach; 
			return $count;
		}
	}
}


/*-----------------------------------------------------------------------------------*/
/*	VOTING (GLOBAL)  :: LIKE, UNLIKE, RESET  (SUPPORT FUNCTION)
/*-----------------------------------------------------------------------------------*/
$timebeforerevote = 30; // = 30 mins
function manual_hasAlreadyVoted($post_id)
{
    global $timebeforerevote;
 
    // Retrieve post votes IPs
    $meta_IP = get_post_meta($post_id, "voted_IP");
	if (!empty($meta_IP)) {
		$voted_IP = $meta_IP[0];
	} else {
		$voted_IP = '';
	}
     
    if(!is_array($voted_IP))
        $voted_IP = array();
         
    // Retrieve current user IP
    $ip = $_SERVER['REMOTE_ADDR'];
     
    // If user has already voted
    if(in_array($ip, array_keys($voted_IP)))
    {
        $time = $voted_IP[$ip];
        $now = time();
         
        // Compare between current time and vote time
        if(round(($now - $time) / 60) > $timebeforerevote)
            return false;
             
        return true;
    }
     
    return false;
}


/*-----------------------------------------------------------------------------------*/
/*	DOCUMENTATION RELATED POST
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_doc_related_post')) {
	function manual_doc_related_post($currentID) {  
		
		global $post, $theme_options;
		if( isset($theme_options['documentation-related-post-per-page']) && $theme_options['documentation-related-post-per-page'] != '' ) {
			$posts_per_page = $theme_options['documentation-related-post-per-page'];
		} else {
			$posts_per_page = 6;
		}
		$categories = get_the_terms($currentID, 'manualdocumentationcategory');
		//print_r($categories);
		if ($categories) {
			$category_ids = array();
			foreach($categories as $individual_category) 
				$category_ids[] = $individual_category->term_id;
				//print_r($category_ids);
			$args=array(
			'post_type' => 'manual_documentation',
			'tax_query' => array(
				array(
					'taxonomy' => 'manualdocumentationcategory',
					'field' => 'term_id',
					'terms' => $category_ids
				)
			),
			'post__not_in' => array($currentID),
			'posts_per_page'=> $posts_per_page, // Number of related posts that will be shown.
			'ignore_sticky_posts'=>1 // sticky post hide
		   );
		   $related_articles_query = new wp_query( $args );
		   if( $related_articles_query->have_posts() ) {
		   ?>
			<style>.manual_related_articles h5:before { left: 26px; } .body-content li.cat.inner:before { left: 30px; }</style>
			<section class="manual_related_articles">
				<h5><?php echo $theme_options['documentation-related-post-title']; ?></h5>
				<span class="separator small"></span>
				<ul class="kbse">
				<?php 
				 while( $related_articles_query->have_posts() ) {
						$related_articles_query->the_post();
				?>
					<li class="cat inner"><a href="<?php the_permalink()?>"><?php the_title(); ?></a></li>
				 <?php } ?>
				</ul>
			</section>      
		   <?php 	   
		   }
		}
		wp_reset_postdata();
	}
}



/*-----------------------------------------------------------------------------------*/
/*	KNOWLEDGEBASE RELATED POST
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_kb_related_post')) {
	function manual_kb_related_post() {
		global $post, $theme_options;
		if( isset($theme_options['kb-related-post-per-page']) && $theme_options['kb-related-post-per-page'] != '' ) {
			$posts_per_page = $theme_options['kb-related-post-per-page'];
		} else {
			$posts_per_page = 6;
		}
		$categories = get_the_terms($post->ID, 'manualknowledgebasecat');
		//print_r($categories);
		if ($categories) {
			$category_ids = array();
			foreach($categories as $individual_category) 
				$category_ids[] = $individual_category->term_id;
				//print_r($category_ids);
			$args=array(
			'post_type' => 'manual_kb',
			'tax_query' => array(
				array(
					'taxonomy' => 'manualknowledgebasecat',
					'field' => 'term_id',
					'terms' => $category_ids
				)
			),
			'post__not_in' => array($post->ID),
			'posts_per_page'=> $posts_per_page, // Number of related posts that will be shown.
			'ignore_sticky_posts'=>1 // sticky post hide
		   );
		   $related_articles_query = new wp_query( $args );
		   if( $related_articles_query->have_posts() ) {
		   ?>
			<section class="manual_related_articles">
				<h5><?php echo $theme_options['kb-related-post-title']; ?></h5>
				<span class="separator small"></span>
				<ul class="kbse">
				<?php 
				 while( $related_articles_query->have_posts() ) {
						$related_articles_query->the_post();
				?>
					<li class="cat inner"><a href="<?php the_permalink()?>"><?php the_title(); ?></a></li>
				 <?php } ?>
				</ul>
			</section>      
		   <?php 	   
		   }
		}
		wp_reset_postdata();
	}
}



/*-----------------------------------------------------------------------------------*/
/*	ATTACHMENT FILE
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_kb_attachment_files')) {
	function manual_kb_attachment_files($postID = '') {
		global $theme_options;
		if( isset($theme_options['attached-file-title']) && $theme_options['attached-file-title'] != '' ) {
			$attached_title = $theme_options['attached-file-title'];
		}
		
		if( $postID != '' ) { 
			$entries = get_post_meta( $postID, '_manual_custom_post_attached_files', true );
		} else {  
			$entries = get_post_meta( get_the_ID(), '_manual_custom_post_attached_files', true );
		}
		if( !empty($entries)) { 
		echo '<div class="manual_attached_section">
			  <h5>'.$attached_title.'</h5>
			  <span class="separator small"></span>
			  <div class="wrapbox">
			  <table class="table table-hover"> 
				<thead> 
					<tr> 
						<th>#</th> 
						<th>'.$attached_title = $theme_options['attached-file-type'].'</th> 
						<th>'.$attached_title = $theme_options['attached-file-size'].'</th> 
						<th>'.$attached_title = $theme_options['attached-file-download'].'</th> 
					</tr> 
				</thead>	
				 ';
			$i = 1;	
			foreach ( (array) $entries as $key => $entry ) {
				$file_size = filesize( get_attached_file( $entry['image_id'] ) );
				$attach_file_type = wp_check_filetype($entry['image']);
				$filename = ( get_the_title($entry['image_id'])?get_the_title($entry['image_id']):basename( get_attached_file( $entry['image_id'] ) )); 
				$img = $title = $desc = $caption = '';
				if ( isset( $entry['title'] ) ) $title = esc_html( $entry['title'] );
					if ( isset( $entry['image'] ) ) { 
						echo '<tbody> 
							<tr> 
								<th scope="row">'.$i.'</th> 
								<td>'. '.'.$attach_file_type['ext'].'</td> 
								<td>'. size_format($file_size, 2) .'</td> 
								<td><a href="'. wp_get_attachment_url( $entry['image_id'] ) .'" '.( (isset($entries[$key]['new_window']) && $entries[$key]['new_window'] == true)?'target="_blank"':'' ).'>'. $filename .'</a></td> 
							</tr> 
						</tbody>'; 
					}
			$i++;		
			}
			echo '</table></div></div>';
		}
	}
}



/*-----------------------------------------------------------------------------------*/
/*	ACCESS ATTACHMENT
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_access_attachment')) {
	function manual_access_attachment($message, $ajaxcall_login = '') {
		global $theme_options;
		echo '<div class="manual_attached_section">';
		echo '<h5>'.$theme_options['attached-file-title'].'</h5>';
		echo '<span class="separator small"></span>
		  <div class="wrapbox" style="background:none;">
			   <div class="col-md-12 col-sm-12">
					<div class="manual_login_page">
					  <div class="custom_login_form">';
					   if( $message != '' ) { 
							echo '<h3>'.stripslashes($message).'</h3>'; 
						}
						if( $ajaxcall_login == '' ) {
							$args = array(
								'echo' => false,
							);
							echo wp_login_form($args); 
						} else {
							echo ' <form action="' . esc_url( site_url( 'wp-login.php', 'login_post' ) ) . '" method="post"><input type="submit" class="button-primary" value="Log In"></form>';
						}
						echo '<ul class="custom_register">';
						if ( get_option( 'users_can_register' ) ) { wp_register(); }
						echo '<li><a href="'.wp_lostpassword_url().'" title="Lost Password">Lost Password</a></li>
						</ul>
					  </div>
					</div>
				</div>
		  </div>
		</div>';	
	}
}


/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: HEADER LOGO
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_global_header')) {
	function manual_global_header() {
		global $theme_options;
		 if( $theme_options['hide-header-logo-status'] == false ) { 
		  echo '<a class="navbar-brand" href="'.esc_url( home_url('/') ).'">';

		  if( isset($theme_options['theme-header-logo']['url']) && $theme_options['theme-header-logo']['url'] != '' ) { 
			$logo_url = $theme_options['theme-header-logo']['url']; 
		  } else { 
			$logo_url = get_template_directory_uri().'/img/logo-dark.png'; 
		  }
		  
		  if( isset($theme_options['theme-nav-homepg-logo-when-img-bg']['url']) && $theme_options['theme-nav-homepg-logo-when-img-bg']['url'] != '' ) {
			$white_url = $theme_options['theme-nav-homepg-logo-when-img-bg']['url'];
		  } else {
			$white_url = get_template_directory_uri().'/img/logo-home.png';  
		  }
	  
		  echo '<img src="'.esc_url( $logo_url ).'" class="pull-left custom-nav-logo home-logo-show" alt="">'; 
		  echo '<img src="'.esc_url( $white_url ).'" class="pull-left custom-nav-logo inner-page-white-logo" alt="">'; 
		  echo '</a>';
       } 
		
	}
}

/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: MENU NAVIGATION STYLE CONTROL 
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_menu_navigation_control')) {
	function manual_menu_navigation_control() {
		global $theme_options;
		// check navigation type [manual options]	
		if( $theme_options['theme-nav-type'] == 1 ) $theme_nav_type = 1;
		else $theme_nav_type = 2;
			
		if($theme_nav_type == 2) { 
			echo '.navbar { position: absolute; width: 100%; background: transparent!important; } .jumbotron_new.jumbotron-inner-fix{position: inherit;} .jumbotron_new .inner-margin-top { padding-top: 92px; }';
		} else {
			echo '';
		}	
		
	}
}

/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: HEADER CONTROL GLOBAL
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_header_control_global')) {
	function manual_header_control_global() {
		global $theme_options;
		
		/*MENU CONTROL*/
		echo '.navbar-inverse .navbar-nav>li>a { font-family:'.($theme_options['custom-nav-font']?$theme_options['custom-nav-font']:$theme_options['theme-typography-nav']['font-family']).'!important; text-transform: '.$theme_options['first-level-menu-text-transform'].'; font-weight: '.$theme_options['first-level-menu-weight'].'; font-size: '.$theme_options['first-level-menu-font-size'].'px; letter-spacing: '.$theme_options['first-level-menu-letter-spacing'].'; color:'.$theme_options['first-level-menu-text-color']['regular'].'!important; } .navbar-inverse .navbar-nav>li>a:hover { color:'.$theme_options['first-level-menu-text-color']['hover'].'!important; }
#navbar ul li > ul { background-color:'.$theme_options['menu-inner-level-background-color'].'; border-color:'.$theme_options['menu-inner-level-background-color'].'; box-shadow: 0 5px 11px 0 rgba(0,0,0,.27); padding: 10px 0px;} #navbar ul li > ul li a { font-family:'.($theme_options['custom-nav-font']?$theme_options['custom-nav-font']:$theme_options['theme-typography-nav']['font-family']).'!important; font-weight:'.$theme_options['menu-inner-level-weight'].'; font-size:'.$theme_options['menu-inner-level-font-size'].'px; color:'.$theme_options['menu-inner-level-text-color']['regular'].'!important; } #navbar ul li > ul li a:hover { color:'.$theme_options['menu-inner-level-text-color']['hover'].'!important; } @media (max-width: 991px) {
.mobile-menu-holder li a {  font-family:'.($theme_options['custom-nav-font']?$theme_options['custom-nav-font']:$theme_options['theme-typography-nav']['font-family']).'!important; } }';
		
		  // default
		  if( $theme_options['default-header-sytle-backgorund-image'] == false ) {
			  echo '.noise-break { background: #F7F7F7 url('.get_template_directory_uri().'/img/noise.jpg) repeat; }';
		  } 
		  
		  if( $theme_options['default-header-sytle-backgorund-image'] == true && isset($theme_options['manual-header-custom-image']['url']) && $theme_options['manual-header-custom-image']['url'] != '' ) {
			   echo '.noise-break { background: '.$theme_options['default-header-sytle-background-color'].' url('.$theme_options['manual-header-custom-image']['url'].') repeat; background-size: cover; background-position:'.$theme_options['header-background-position'].' }';
		  } else if( $theme_options['default-header-sytle-backgorund-image'] == true && $theme_options['manual-header-custom-image']['url'] == '' ) {
			  echo '.noise-break { background: '.$theme_options['default-header-sytle-background-color'].'; }';
		  }
		  
		  // check the logo switch status
		  if( $theme_options['theme-nav-type'] == 2 && $theme_options['manual-header-custom-image']['url'] == '' && 
		  	  $theme_options['default-header-sytle-backgorund-image'] == false) {
			  echo 'img.inner-page-white-logo { display: none; } img.home-logo-show { display: block; }';
		  } else if( $theme_options['theme-nav-type'] == 2 && $theme_options['manual-header-custom-image']['url'] != '' && 
		  		     $theme_options['default-header-sytle-backgorund-image'] == true) {
			  echo 'img.inner-page-white-logo { display: block; } img.home-logo-show { display: none; } .navbar-inverse .navbar-nav>li>a { color:'.$theme_options['first-level-menu-text-color-for-img-bg']['regular'].'!important; } .navbar-inverse .navbar-nav>li>a:hover { color:'.$theme_options['first-level-menu-text-color-for-img-bg']['hover'].'!important; } .hamburger-menu span { background: #ffffff;}';
		  } else if( $theme_options['header-force-white-logo-and-text'] == 1 && $theme_options['manual-header-custom-image']['url'] == '' && $theme_options['default-header-sytle-backgorund-image'] == true ) { 
		  	 echo 'img.inner-page-white-logo { display: block; } img.home-logo-show { display: none; } .navbar-inverse .navbar-nav>li>a { color:'.$theme_options['first-level-menu-text-color-for-img-bg']['regular'].'!important; } .navbar-inverse .navbar-nav>li>a:hover { color:'.$theme_options['first-level-menu-text-color-for-img-bg']['hover'].'!important; } .hamburger-menu span { background: #ffffff;}';
		  } else {
			  echo 'img.inner-page-white-logo { display: none; } img.home-logo-show { display: block; }';
		  }
		  echo 'nav.navbar.after-scroll-wrap img.inner-page-white-logo{ display: none; } nav.navbar.after-scroll-wrap img.home-logo-show { display: block; }';
		 
		 if( $theme_options['header-opacity-uploadimage-global'] == true && !empty($theme_options['manual-header-custom-image']['url']) && $theme_options['default-header-sytle-backgorund-image'] == true ) {  echo '.page_opacity.header_custom_height_new{background: rgba(0,0,0,0.3);}'; }
		  
		// header height
		echo '.page_opacity.header_custom_height_new{ padding: '.$theme_options['default-header-sytle-height'].'px 0px!important; }';
		  
		// nav background and border bottom
		 echo '.navbar {  z-index: 99; border: none;';   
		 if( $theme_options['apply-nav-box-shadow'] ) echo 'box-shadow: 0px 0px 30px 0px rgba(0, 0, 0, 0.05);'; 
		 if( $theme_options['apply-nav-border'] ==  1) echo 'border-bottom:1px solid '.$theme_options['apply-nav-border-color']['rgba'].'; ';
		 else echo 'border-bottom:none;';
		 if( $theme_options['apply-nav-background'] ==  1) echo ' background: '.$theme_options['apply-nav-background-color']['rgba'].'!important;';
		 echo '}';
		 
		 // controls header text align
		 echo ' .header_control_text_align { text-align:'.$theme_options['default-header-text-align'].'; } ';
		 // header title control
		 echo 'h1.custom_h1_head { color: '.$theme_options['default-top-header-title-color'].'!important; font-size: '.$theme_options['default-header-title-font-size'].'px!important; font-weight: '.$theme_options['default-header-title-font-weight'].'!important; text-transform:'.$theme_options['default-header-title-text-transform'].'!important; letter-spacing: '.$theme_options['default-header-title-font-letter-spacing'].'px!important; }';
		 // subtitle
		 echo 'p.inner-header-color { color:'.$theme_options['default-top-header-subtitle-color'].'; font-size: '.$theme_options['default-header-subtitle-font-size'].'px!important; letter-spacing: '.$theme_options['default-header-subtitle-font-letter-spacing'].'px!important; font-weight:'.$theme_options['default-header-subtitle-font-weight'].'!important; text-transform:'.$theme_options['default-header-subtitle-text-transform'].';  }';
		 // breadcrumbs
		 echo '#breadcrumbs {color:'.$theme_options['default-top-header-breadcrumb-color'].'; text-transform:'.$theme_options['default-header-breadcrumb-text-transform'].'; letter-spacing: '.$theme_options['default-header-breadcrumb-letter-spacing'].'px; font-size: '.$theme_options['default-header-breadcrumb-font-size'].'px; font-weight: '.$theme_options['default-header-breadcrumb-font-weight'].';  padding-top: '.$theme_options['default-header-breadcrumb-padding'].'px;} #breadcrumbs span{ color:'.$theme_options['default-top-header-breadcrumb-color'].'; } #breadcrumbs a{ color:'.$theme_options['default-top-header-breadcrumb-link-color']['regular'].'; } #breadcrumbs a:hover{ color:'.$theme_options['default-top-header-breadcrumb-link-color']['hover'].'; } ';
		 // trending search
		 echo '.trending-search span.popular-keyword-title { color:'.$theme_options['theme_header_treanding_search_color'].'; } .trending-search a { color:'.$theme_options['theme_header_treanding_search_link_color']['regular'].'!important; } .trending-search a:hover { color:'.$theme_options['theme_header_treanding_search_link_color']['hover'].'!important; }'; 
		 
		  //ipad + mobie fix
			echo '@media (min-width:768px) and (max-width:991px) { .navbar { position:relative!important; background: #FFFFFF!important; } .jumbotron_new.jumbotron-inner-fix .inner-margin-top{ padding-top: 0px!important; } .navbar-inverse .navbar-nav > li > a { color: #181818!important; } .padding-jumbotron{  padding:0px 0px 0px; } body.home .navbar-inverse .navbar-nav>li>a { color: #000000!important; } body.home .navbar-inverse .navbar-nav > li > a:hover{ color: #7C7C7C!important; } img.inner-page-white-logo { display: none; } img.home-logo-show { display: block; } ul.nav.navbar-nav.hidemenu { display: block; }} 
			     @media (max-width:767px) { .navbar { position:relative!important; background: #FFFFFF!important; } .padding-jumbotron{ padding:0px 10px;  } .navbar-inverse .navbar-nav > li > a { color: #181818!important; padding-top: 10px!important; } .jumbotron_new.jumbotron-inner-fix .inner-margin-top { padding-top: 0px!important;  } .navbar-inverse .navbar-nav > li > a { border-top: none!important; } body.home .navbar-inverse .navbar-nav>li>a { color: #000000!important; } body.home .navbar-inverse .navbar-nav > li > a:hover{ color: #7C7C7C!important; } img.inner-page-white-logo { display: none; } img.home-logo-show { display: block; } }';
		
	}
}


/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: CUSTOMIZER ENHANCE 
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_customizer_enhance')) {
	function manual_customizer_enhance() {
		global $theme_options,$post;
		
		/*LOGO ADJUSTMENT*/
		$readjust_logo_height = $readjust_logo_margintop = '';
		if( !empty($theme_options['theme-logo-readjust-height']['units']) && !empty($theme_options['theme-logo-readjust-height']['height']) ) { 
			$readjust_logo_height = $theme_options['theme-logo-readjust-height']['height'];
			echo '.custom-nav-logo { height: '.$readjust_logo_height.'!important;}'; 
		}
		if( !empty($theme_options['theme-logo-readjust-margin-top']['units']) && !empty($theme_options['theme-logo-readjust-margin-top']['height']) ) { 
			$readjust_logo_margintop = $theme_options['theme-logo-readjust-margin-top']['height'];
			echo '.custom-nav-logo { margin-top: '.$readjust_logo_margintop.';}'; 
		}
		if( !empty($theme_options['theme-logo-readjust-sticky-height']['units']) && !empty($theme_options['theme-logo-readjust-sticky-height']['height']) ) { 
			$readjust_sticky_menu_logo_height = $theme_options['theme-logo-readjust-sticky-height']['height'];
			echo 'nav.navbar.after-scroll-wrap .custom-nav-logo { height: '.$readjust_sticky_menu_logo_height.'!important;}'; 
		}
		if( !empty($theme_options['theme-logo-readjust-sticky-margin-top']['units']) && !empty($theme_options['theme-logo-readjust-sticky-margin-top']['height']) ) { 
			$readjust_sticky_menu_logo_margintop = $theme_options['theme-logo-readjust-sticky-margin-top']['height'];
			echo 'nav.navbar.after-scroll-wrap .custom-nav-logo { margin-top: '.$readjust_sticky_menu_logo_margintop.';}'; 
		}
		
		/*REDEFINE FOOTER DESIGN*/
		echo '.footer-bg { background: '.$theme_options['theme_footer_widget_bg_color'].'; } .footer-widget h5 { color: '.$theme_options['theme_footer_widget_title_color'].'!important; } .footer-widget .textwidget { color: '.$theme_options['theme_footer_widget_text_color'].'!important; } .footer-widget a {
color: '.$theme_options['theme_footer_widget_text_link_color']['regular'].'!important; } .footer-widget a:hover { color:'.$theme_options['theme_footer_widget_text_link_color']['hover'].'!important; } span.post-date { color: '.$theme_options['theme_footer_widget_text_color'].'; }'; 

		echo '.footer_social_copyright, .footer-bg.footer-type-one{ background-color: '.$theme_options['theme_footer_social_bg_color'].'; } .footer-btm-box p, .footer-bg.footer-type-one p { color: '.$theme_options['theme_footer_social_text_color'].'; } .footer-btm-box a, .footer-bg.footer-type-one .footer-btm-box-one a{ color: '.$theme_options['theme_footer_social_link_color']['regular'].'!important;  } .footer-btm-box a:hover, .footer-bg.footer-type-one .footer-btm-box-one a:hover { color: '.$theme_options['theme_footer_social_link_color']['hover'].'!important; } .footer-btm-box .social-footer-icon, .footer-bg.footer-type-one .social-footer-icon { color: '.$theme_options['theme_footer_social_icon_link_color']['regular'].'; } .footer-btm-box .social-footer-icon:hover, .footer-bg.footer-type-one .social-footer-icon:hover { color:'.$theme_options['theme_footer_social_icon_link_color']['hover'].'; }';
		
		/*SEARCH ICON BOUNCEIN ANIMATION*/
		if( $theme_options['manual-live-search-icon-bouncein'] == true ) {
			echo 'form.searchform i.livesearch{ animation: bounceIn 750ms linear infinite alternate; -moz-animation: bounceIn 750ms linear infinite alternate;   -webkit-animation: bounceIn 750ms linear infinite alternate; -o-animation: bounceIn 750ms linear infinite alternate; } @-webkit-keyframes bounceIn{0%,20%,40%,60%,80%,100%{-webkit-transition-timing-function:cubic-bezier(0.215,0.610,0.355,1.000);transition-timing-function:cubic-bezier(0.215,0.610,0.355,1.000);}0%{opacity:0;-webkit-transform:scale3d(.3,.3,.3);transform:scale3d(.3,.3,.3);}20%{-webkit-transform:scale3d(1.1,1.1,1.1);transform:scale3d(1.1,1.1,1.1);}40%{-webkit-transform:scale3d(.9,.9,.9);transform:scale3d(.9,.9,.9);}60%{opacity:1;-webkit-transform:scale3d(1.03,1.03,1.03);transform:scale3d(1.03,1.03,1.03);}80%{-webkit-transform:scale3d(.97,.97,.97);transform:scale3d(.97,.97,.97);}100%{opacity:1;-webkit-transform:scale3d(1,1,1);transform:scale3d(1,1,1);}}
	keyframes bounceIn{0%,20%,40%,60%,80%,100%{-webkit-transition-timing-function:cubic-bezier(0.215,0.610,0.355,1.000);transition-timing-function:cubic-bezier(0.215,0.610,0.355,1.000);}0%{opacity:0;-webkit-transform:scale3d(.3,.3,.3);-ms-transform:scale3d(.3,.3,.3);transform:scale3d(.3,.3,.3);}20%{-webkit-transform:scale3d(1.1,1.1,1.1);-ms-transform:scale3d(1.1,1.1,1.1);transform:scale3d(1.1,1.1,1.1);}40%{-webkit-transform:scale3d(.9,.9,.9);-ms-transform:scale3d(.9,.9,.9);transform:scale3d(.9,.9,.9);}60%{opacity:1;-webkit-transform:scale3d(1.03,1.03,1.03);-ms-transform:scale3d(1.03,1.03,1.03);transform:scale3d(1.03,1.03,1.03);}80%{-webkit-transform:scale3d(.97,.97,.97);-ms-transform:scale3d(.97,.97,.97);transform:scale3d(.97,.97,.97);}100%{opacity:1;-webkit-transform:scale3d(1,1,1);-ms-transform:scale3d(1,1,1);transform:scale3d(1,1,1);}}
	.bounceIn{-webkit-animation-name:bounceIn;animation-name:bounceIn;-webkit-animation-duration:.75s;animation-duration:.75s;}';
		}
		
		/*CUSTOM CODE*/
		if( isset($theme_options['manual-editor-css']) && $theme_options['manual-editor-css'] != '' ) { echo $theme_options['manual-editor-css']; }
		
		/*CONTROL CSS BEFORE REDUX ACTIVE*/  
		if(!is_plugin_active('redux-framework/redux-framework.php')){
			echo 'img.home-logo-show { display: block!important; } h1.inner-header { text-align: center; }body { color: #424242; font-family:Open Sans!important; font-size: 14px; line-height: 1.4; letter-spacing: 0.3px; }h1 {  font-family: Raleway; font-weight:800; font-size:36px; line-height: 40px; text-transform:none; letter-spacing: 0.2px; color: #363d40; }h2 {  font-family: Raleway; font-weight:800; font-size:31px; line-height: 35px; text-transform:none; letter-spacing: 0.2px; color: #626363; }h3 {  font-family: Raleway; font-weight:700; font-size:26px; line-height: 34px; text-transform:none; letter-spacing: 0.2px; color: #585858; }h4 {  font-family: Raleway; font-weight:700; font-size:21px; line-height: 24px; text-transform:none; letter-spacing: 0.2px; color: #585858; }h5 {  font-family: Raleway; font-weight:700; font-size:16px; line-height: 20px; text-transform:none; letter-spacing: 0.5px; color: #585858; }h6 {  font-family: Raleway; font-weight:700; font-size:14px; line-height: 20px; text-transform:none; letter-spacing: 0.2px; color: #585858; }.page_opacity.header_custom_height_new { padding: 90px 0px!important; }.header_control_text_align {     text-align: center!important; }#breadcrumbs {color: #919191; text-transform: capitalize; letter-spacing: 0px; font-size: 14px; font-weight: 400; padding-top: 0px; }';
		}
		
		// Fonts
		if( $theme_options['theme-typography-body']['font-family'] != '' ) {
			echo 'body { color: '.esc_attr($theme_options['theme-typography-body']['color']).'; font-family:'.esc_attr(($theme_options['custom-body-font']?$theme_options['custom-body-font']:$theme_options['theme-typography-body']['font-family'])).'!important; font-size: '.esc_attr($theme_options['theme-typography-body']['font-size']).'; line-height: '. preg_replace( '/px/', '', esc_attr($theme_options['theme-typography-body']['line-height']) ).'; letter-spacing: '.esc_attr($theme_options['theme-typography-body']['letter-spacing']).'; }';
		}
	
		// h1
		if( $theme_options['theme-h1-typography']['font-family'] != '' ) { 
			if( isset($theme_options['theme-h1-typography']['font-weight']) && 
				$theme_options['theme-h1-typography']['font-weight'] != ''  ) {
				$h1_weight = esc_attr($theme_options['theme-h1-typography']['font-weight']);	
			} else {
				$h1_weight = esc_attr($theme_options['theme-h1-typography']['font-style']);
			}
			echo 'h1 {  font-family: '.esc_attr(($theme_options['custom-h1-font']?$theme_options['custom-h1-font']:$theme_options['theme-h1-typography']['font-family'])).'; font-weight:'.$h1_weight.'; font-size:'.esc_attr($theme_options['theme-h1-typography']['font-size']).( ($theme_options['theme-h1-typography']['font-size'] == '36')?'px':'' ).'; line-height: '.esc_attr($theme_options['theme-h1-typography']['line-height']).( ($theme_options['theme-h1-typography']['line-height'] == '20' || $theme_options['theme-h1-typography']['line-height'] == '40')?'px':'' ).'; text-transform:'.esc_attr($theme_options['theme-h1-typography']['text-transform']).'; letter-spacing: '.esc_attr($theme_options['theme-h1-typography']['letter-spacing']).( ($theme_options['theme-h1-typography']['letter-spacing'] == '0.2')?'px':'' ).'; color: '.esc_attr($theme_options['theme-h1-typography']['color']).'; }';				
		}
		// h2
		if( $theme_options['theme-h2-typography']['font-family'] != '' ) {
			if( isset($theme_options['theme-h2-typography']['font-weight']) && 
				$theme_options['theme-h2-typography']['font-weight'] != ''  ) {
				$h2_weight = esc_attr($theme_options['theme-h2-typography']['font-weight']);	
			} else {
				$h2_weight = esc_attr($theme_options['theme-h2-typography']['font-style']);
			}
			echo 'h2 {  font-family: '.esc_attr(($theme_options['custom-h2-font']?$theme_options['custom-h2-font']:$theme_options['theme-h2-typography']['font-family'])).'; font-weight:'.$h2_weight.'; font-size:'.esc_attr($theme_options['theme-h2-typography']['font-size']).( ($theme_options['theme-h2-typography']['font-size'] == '31')?'px':'' ).'; line-height: '.esc_attr($theme_options['theme-h2-typography']['line-height']).(($theme_options['theme-h2-typography']['line-height'] == '30' || $theme_options['theme-h2-typography']['line-height'] == '35')?'px':'' ).'; text-transform:'.esc_attr($theme_options['theme-h2-typography']['text-transform']).'; letter-spacing: '.esc_attr($theme_options['theme-h2-typography']['letter-spacing']).( ($theme_options['theme-h2-typography']['letter-spacing'] == '0.2')?'px':'' ).'; color: '.esc_attr($theme_options['theme-h2-typography']['color']).'; }';				
		}
		// h3
		if( $theme_options['theme-h3-typography']['font-family'] != '' ) {
			if( isset($theme_options['theme-h3-typography']['font-weight']) && 
				$theme_options['theme-h3-typography']['font-weight'] != ''  ) {
				$h3_weight = esc_attr($theme_options['theme-h3-typography']['font-weight']);	
			} else {
				$h3_weight = esc_attr($theme_options['theme-h3-typography']['font-style']);
			}
			echo 'h3 {  font-family: '.esc_attr(($theme_options['custom-h3-font']?$theme_options['custom-h3-font']:$theme_options['theme-h3-typography']['font-family'])).'; font-weight:'.$h3_weight.'; font-size:'.esc_attr($theme_options['theme-h3-typography']['font-size']).( ($theme_options['theme-h3-typography']['font-size'] == '26')?'px':'' ).'; line-height: '.esc_attr($theme_options['theme-h3-typography']['line-height']).(($theme_options['theme-h3-typography']['line-height'] == '20' || $theme_options['theme-h3-typography']['line-height'] == '34')?'px':'' ).'; text-transform:'.esc_attr($theme_options['theme-h3-typography']['text-transform']).'; letter-spacing: '.esc_attr($theme_options['theme-h3-typography']['letter-spacing']).( ($theme_options['theme-h3-typography']['letter-spacing'] == '0.2')?'px':'' ).'; color: '.esc_attr($theme_options['theme-h3-typography']['color']).'; }';
		}
		// h4
		if( $theme_options['theme-h4-typography']['font-family'] != '' ) {
			if( isset($theme_options['theme-h4-typography']['font-weight']) && 
				$theme_options['theme-h4-typography']['font-weight'] != ''  ) {
				$h4_weight = esc_attr($theme_options['theme-h4-typography']['font-weight']);	
			} else {
				$h4_weight = esc_attr($theme_options['theme-h4-typography']['font-style']);
			}
			echo 'h4 {  font-family: '.esc_attr(($theme_options['custom-h4-font']?$theme_options['custom-h4-font']:$theme_options['theme-h4-typography']['font-family'])).'; font-weight:'.$h4_weight.'; font-size:'.esc_attr($theme_options['theme-h4-typography']['font-size']).( ($theme_options['theme-h4-typography']['font-size'] == '21')?'px':'' ).'; line-height: '.esc_attr($theme_options['theme-h4-typography']['line-height']).(($theme_options['theme-h4-typography']['line-height'] == '20' || $theme_options['theme-h4-typography']['line-height'] == '24')?'px':'' ).'; text-transform:'.esc_attr($theme_options['theme-h4-typography']['text-transform']).'; letter-spacing: '.esc_attr($theme_options['theme-h4-typography']['letter-spacing']).( ($theme_options['theme-h4-typography']['letter-spacing'] == '0.2')?'px':'' ).'; color: '.esc_attr($theme_options['theme-h4-typography']['color']).'; }';
		}
		// h5
		if( $theme_options['theme-h5-typography']['font-family'] != '' ) {
			if( isset($theme_options['theme-h5-typography']['font-weight']) && 
				$theme_options['theme-h5-typography']['font-weight'] != ''  ) {
				$h5_weight = esc_attr($theme_options['theme-h5-typography']['font-weight']);	
			} else {
				$h5_weight = esc_attr($theme_options['theme-h5-typography']['font-style']);
			}
			echo 'h5 {  font-family: '.esc_attr(($theme_options['custom-h5-font']?$theme_options['custom-h5-font']:$theme_options['theme-h5-typography']['font-family'])).'; font-weight:'.$h5_weight.'; font-size:'.esc_attr($theme_options['theme-h5-typography']['font-size']).( ($theme_options['theme-h5-typography']['font-size'] == '16')?'px':'' ).'; line-height: '.esc_attr($theme_options['theme-h5-typography']['line-height']).(($theme_options['theme-h5-typography']['line-height'] == '20')?'px':'' ).'; text-transform:'.esc_attr($theme_options['theme-h5-typography']['text-transform']).'; letter-spacing: '.esc_attr($theme_options['theme-h5-typography']['letter-spacing']).( ($theme_options['theme-h5-typography']['letter-spacing'] == '0.5')?'px':'' ).'; color: '.esc_attr($theme_options['theme-h5-typography']['color']).'; }';
		}
		// h6
		if( $theme_options['theme-h6-typography']['font-family'] != '' ) {
			if( isset($theme_options['theme-h6-typography']['font-weight']) && 
				$theme_options['theme-h6-typography']['font-weight'] != ''  ) {
				$h6_weight = esc_attr($theme_options['theme-h6-typography']['font-weight']);	
			} else {
				$h6_weight = esc_attr($theme_options['theme-h6-typography']['font-style']);
			}
			echo 'h6 {  font-family: '.esc_attr(($theme_options['custom-h6-font']?$theme_options['custom-h6-font']:$theme_options['theme-h6-typography']['font-family'])).'; font-weight:'.$h6_weight.'; font-size:'.esc_attr($theme_options['theme-h6-typography']['font-size']).( ($theme_options['theme-h6-typography']['font-size'] == '14')?'px':'' ).'; line-height: '.esc_attr($theme_options['theme-h6-typography']['line-height']).(($theme_options['theme-h6-typography']['line-height'] == '20')?'px':'' ).'; text-transform:'.esc_attr($theme_options['theme-h6-typography']['text-transform']).( ($theme_options['theme-h6-typography']['letter-spacing'] == '0.2')?'px':'' ).'; letter-spacing: '.esc_attr($theme_options['theme-h6-typography']['letter-spacing']).'; color: '.esc_attr($theme_options['theme-h6-typography']['color']).'; }';
		}
		// Link Color
		echo '.body-content .knowledgebase-cat-body h4 a, .body-content .knowledgebase-body h5:before, .body-content .knowledgebase-body h5 a, #bbpress-forums .bbp-reply-author .bbp-author-name, #bbpress-forums .bbp-topic-freshness > a, #bbpress-forums li.bbp-body ul.topic li.bbp-topic-title a, #bbpress-forums .last-posted-topic-title a, #bbpress-forums .bbp-forum-link, #bbpress-forums .bbp-forum-header .bbp-forum-title, .body-content .blog .caption h2 a, a.href, .body-content .collapsible-panels p.post-edit-link a, .tagcloud.singlepg a, h4.title-faq-cat a, .portfolio-next-prv-bar .portfolio-prev a, .portfolio-next-prv-bar .portfolio-next a, .search h4 a, .portfolio-filter ul li span{ color:'.$theme_options['manual-global-link-color']['regular'].'!important; }  
		.body-content .knowledgebase-cat-body h4 a:hover, .body-content .knowledgebase-body h5:hover:before, .body-content .knowledgebase-body h5 a:hover, #bbpress-forums .bbp-reply-author .bbp-author-name:hover, #bbpress-forums .bbp-topic-freshness > a:hover, #bbpress-forums li.bbp-body ul.topic li.bbp-topic-title a:hover, #bbpress-forums .last-posted-topic-title a:hover, #bbpress-forums .bbp-forum-link:hover, #bbpress-forums .bbp-forum-header .bbp-forum-title:hover, .body-content .blog .caption h2 a:hover, .body-content .blog .caption span:hover, .body-content .blog .caption p a:hover, .sidebar-nav ul li a:hover, .tagcloud a:hover , a.href:hover, .body-content .collapsible-panels p.post-edit-link a:hover, .tagcloud.singlepg a:hover, .body-content li.cat a:hover, h4.title-faq-cat a:hover, .portfolio-next-prv-bar .portfolio-prev a:hover, .portfolio-next-prv-bar .portfolio-next a:hover, .search h4 a:hover, .portfolio-filter ul li span:hover{ color:'.$theme_options['manual-global-link-color']['hover'].'!important; }';
		//kb control
		echo '.body-content .knowledgebase-body h5 { font-size:'.esc_attr($theme_options['theme-h5-typography']['font-size']+1).( ($theme_options['theme-h5-typography']['font-size'] == '13')?'px':'px' ).'!important; }';
		if( $theme_options['knowledgebase-cat-quick-stats-under-title'] == true ) { echo '.kb-box-single:before { font-size: 28px; margin-top: -3px; } .kb-box-single { padding: 14px 10% 0px 44px; margin-bottom: 0px;; }'; }
		if( $theme_options['knowledgebase-quick-stats-under-title'] == true ) { echo '.body-content .kb-single:before { font-size: 39px; } .body-content .kb-single { padding: 0px 0px 5px 55px; } .body-content .kb-single:before { top: -4px; }'; }
		
		// page css control
		$current_post_type = get_post_type();
		
		if( is_404() ) {
			manual_custom_page_css_control($theme_options['404-page-header-background-img']['url'], $theme_options['404-header-background-position'], $theme_options['404-apply-nav-background'], $theme_options['404-header-opacity-uploadimage-global'], $theme_options['404-header-height']);
		} else if( is_search() ) {
			manual_custom_page_css_control($theme_options['search-page-header-background-img']['url'], $theme_options['search-header-background-position'], $theme_options['search-apply-nav-background'], $theme_options['search-header-opacity-uploadimage-global'], $theme_options['search-header-height']);
			if( $theme_options['searchpg-records-publish-date'] == false && $theme_options['searchpg-records-author-name'] == false && !is_user_logged_in() ) { echo '.body-content .search::before, .body-content .search.manual_documentation:before {font-size: 32px; margin-top: -10px; }'; }
		} else if( $current_post_type == 'manual_portfolio' ) {  
			manual_header_page_css_control($post->ID);
		} else if($current_post_type == 'manual_faq') {
			if (function_exists('category_image_src')) {
					$display_cat_img = false; 
					// Get only image url
					$display_cat_params = array(
					  'term_id' => null,
					  'size' => 'full'
					);
					$check_cat_img_exist = category_image_src( $display_cat_params , $display_cat_img );
					$check_cat_img_exist = esc_url( $check_cat_img_exist );
			}
			if( $check_cat_img_exist != '') {
					manual_custom_page_css_control($check_cat_img_exist, '', $theme_options['faq-apply-nav-background-category-page'], $theme_options['faq-header-opacity-uploadimage-global-category-page'], $theme_options['faq-header-height-category-page']);
			}
			
		} else if($current_post_type == 'manual_kb') {
			
			if( is_single() ) {
				$image = get_post_meta( get_the_ID(), '_manual_header_image', 1 ); // post upload image
				$terms = wp_get_post_terms($post->ID, 'manualknowledgebasecat');
				$current_term = $terms[0];
				$term_id = $current_term->term_id;
				if (function_exists('category_image_src')) {
					$display_cat_img = false; 
					// Get only image url
					$display_cat_params = array(
					  'term_id' => $term_id,
					  'size' => 'full'
					);
					$check_cat_img_exist = category_image_src( $display_cat_params , $display_cat_img );
					$check_cat_img_exist = esc_url( $check_cat_img_exist );
				}
				if( $theme_options['kb-single-pg-apply-category-image-status'] == true && $check_cat_img_exist != '' && $image == '' ) {
					manual_custom_page_css_control($check_cat_img_exist, $theme_options['kbcat-header-background-position'], $theme_options['kbcat-apply-nav-background'], $theme_options['kbcat-header-opacity-uploadimage-global'], $theme_options['kbcat-header-height']);
				} else {
					if( $image != '') { 
						if( get_post_meta( get_the_ID(), '_manual_remove_nav_bg', 1 ) == false ) $nav = true;
						else $nav = false;
						if( get_post_meta( get_the_ID(), '_manual_bg_opacity_uploadimg', 1 ) == false ) $header_opacity = true;
						else $header_opacity = false;
						manual_custom_page_css_control($image, 'center center', $nav, $header_opacity, get_post_meta( get_the_ID(), '_manual_header_height', 1 ) );
					} else {
						$nav = $header_opacity = false;
						manual_custom_page_css_control('', '', $nav, $header_opacity, get_post_meta( get_the_ID(), '_manual_header_height', 1 ) );
					}
				}
			} else {
				if (function_exists('category_image_src')) {
					$display_cat_img = false; 
					// Get only image url
					$display_cat_params = array(
					  'term_id' => null,
					  'size' => 'full'
					);
					$check_cat_img_exist = category_image_src( $display_cat_params , $display_cat_img );
					$check_cat_img_exist = esc_url( $check_cat_img_exist );
				}
				if( $check_cat_img_exist != '') {
					manual_custom_page_css_control($check_cat_img_exist, $theme_options['kbcat-header-background-position'], $theme_options['kbcat-apply-nav-background'], $theme_options['kbcat-header-opacity-uploadimage-global'], $theme_options['kbcat-header-height']);
				} else {
					manual_custom_page_css_control('', '', '', '', $theme_options['kbcat-header-height']);
				}
			}
			
		} else if($current_post_type == 'manual_documentation') {
			
			if( is_single() ) {
				$terms = wp_get_post_terms($post->ID, 'manualdocumentationcategory');
				$current_term = $terms[0];
				$term_id = $current_term->term_id;
				if (function_exists('category_image_src')) {
					$display_cat_img = false; 
					// Get only image url
					$display_cat_params = array(
					  'term_id' => $term_id,
					  'size' => 'full'
					);
					$check_cat_img_exist = category_image_src( $display_cat_params , $display_cat_img );
					$check_cat_img_exist = esc_url( $check_cat_img_exist );
				}
				if( $check_cat_img_exist != '') {
					manual_custom_page_css_control($check_cat_img_exist, $theme_options['doc-header-background-position'], $theme_options['documentation-apply-nav-background-category-page'], $theme_options['documentation-header-opacity-uploadimage-global-category-page'], $theme_options['documentation-header-height-category-page']);
				} else {
					manual_custom_page_css_control('', '', '', '', $theme_options['documentation-header-height-category-page']);
				}
			} else {
				if (function_exists('category_image_src')) {
					$display_cat_img = false; 
					// Get only image url
					$display_cat_params = array(
					  'term_id' => null,
					  'size' => 'full'
					);
					$check_cat_img_exist = category_image_src( $display_cat_params , $display_cat_img );
					$check_cat_img_exist = esc_url( $check_cat_img_exist );
				}
				if( $check_cat_img_exist != '') {
					manual_custom_page_css_control($check_cat_img_exist, $theme_options['doc-header-background-position'], $theme_options['documentation-apply-nav-background-category-page'], $theme_options['documentation-header-opacity-uploadimage-global-category-page'], $theme_options['documentation-header-height-category-page']);
				} else {
					manual_custom_page_css_control('', '', '', '', $theme_options['documentation-header-height-category-page']);
				}
			}
			if( $theme_options['documentation-quick-stats-under-title'] == true && !is_user_logged_in()  ) { echo '.page-title-header:before {font-size: 38px;}.page-title-header { padding: 0px 0px 5px 53px; margin-bottom: -4px; }'; }
			
		// WOO
		} else if(function_exists("is_woocommerce") && (is_shop() || is_checkout() || is_account_page())){
			if(is_shop()){
				$page_id = get_option('woocommerce_shop_page_id');
				manual_woo_shop_column_css_handler();		
			} elseif(is_checkout()) {
				$page_id = get_option('woocommerce_pay_page_id'); 
			} elseif(is_account_page()) {
				$page_id = get_option('woocommerce_myaccount_page_id'); 
			} elseif(is_account_page()) {
				$page_id = get_option('woocommerce_edit_address_page_id'); 
			} elseif(is_account_page()) {
				$page_id = get_option('woocommerce_view_order_page_id'); 
			}
			$woopage  = get_post( $page_id );
			manual_header_page_css_control( $woopage->ID );
		} else if( is_home() && $current_post_type == 'post' ) {
			$postID = get_option('page_for_posts');
			manual_header_page_css_control($postID);
		} else if( $current_post_type == 'page' ) { 
			manual_header_page_css_control($post->ID);
		} else if( $current_post_type == 'post' && $theme_options['blog_single_page_global_header_settings'] == true && !is_single() ) {
			$postID = get_option('page_for_posts');
			manual_header_page_css_control($postID);
			
		// POST SINGLE
		} else if( $current_post_type == 'post' && is_single() ) {
			
			$feature_image_url = get_the_post_thumbnail_url($post->ID,'large');
			if( $feature_image_url != '' && $theme_options['blog_featured_image_on_the_header'] == true ) {
			manual_custom_page_css_control($feature_image_url, 'center center', $theme_options['blog-apply-nav-background'], $theme_options['blog-header-opacity-uploadimage-global'], $theme_options['blog-header-height']);
			echo  'h1.custom_h1_head { color: #ffffff!important; font-weight: 700!important; }';
			}
			if( $theme_options['blog_single_page_icon_format'] == true ) echo '.body-content .blog.post.format-image:before, .body-content .blog.post.format-quote:before, .body-content .blog.post.format-video:before, .body-content .blog.post.format-audio:before, .body-content .blog.post.format-standard:before { content: ""; }';
		
		// BBPRESS	
		} else if ( class_exists('bbPress') && is_bbPress() ) { 
			if( isset($theme_options['bbpress-header-image']['url']) && $theme_options['bbpress-header-image']['url'] != '') {
				manual_custom_page_css_control($theme_options['bbpress-header-image']['url'], $theme_options['bbpress-header-background-position'], $theme_options['bbpress-apply-nav-background'], $theme_options['bbpress-header-opacity-uploadimage-global'], $theme_options['bbpress-header-height']);
			}
		}
		// eof page css control
				
	}
}

/********************************************
*** PAGE CONTROL HEADER SETTINGS / CSS **
*********************************************/

if (!function_exists('manual_custom_page_css_control')) {
	function manual_custom_page_css_control($img='', $imgbgposition = 'center center', $navbarbg = true, $header_opacity = true, $headerheight = '') {
		global $theme_options;
		if( $img != '' ) {
		echo '.navbar {  border: none; box-shadow:none; background:none!important; } .page_opacity.header_custom_height_new { padding: 112px 0px!important; } .noise-break { background: #f1f1f1 url('.$img.') repeat; background-size: cover; background-position:'.$imgbgposition.'; } h1.custom_h1_head { color: #ffffff!important; } #breadcrumbs, #breadcrumbs span  { color: #f5f5f5!important; } #breadcrumbs a, .trending-search a { color: #f5f5f5!important; } .trending-search a:hover, #breadcrumbs a:hover { color: #ffffff!important; } .trending-search span.popular-keyword-title { color: #dedede; } p.inner-header-color { color: #e8e8e8; }';
		if( $header_opacity == true ) { echo '.page_opacity.header_custom_height_new { background: rgba(0,0,0,0.3); }'; }
		if( $navbarbg == true ) { echo '.navbar {  background:rgba(58,58,64,0.2)!important; }'; }
		if($theme_options['theme-nav-type'] == 2) { 
		echo 'img.home-logo-show { display: none; } img.inner-page-white-logo { display: block; } .navbar-inverse .navbar-nav>li>a { color:'.$theme_options['first-level-menu-text-color-for-img-bg']['regular'].'!important; } .navbar-inverse .navbar-nav>li>a:hover { color:'.$theme_options['first-level-menu-text-color-for-img-bg']['hover'].'!important; } @media (max-width: 991px) and (min-width: 768px) { img.inner-page-white-logo { display: none!important; } img.home-logo-show { display: block; } .navbar {  background:#FFFFFF!important; } } @media (max-width: 767px) {  img.inner-page-white-logo { display: none!important; } img.home-logo-show { display: block; } .navbar {  background:#FFFFFF!important; } }'; }
		}
		if( $headerheight != '' ) echo '.page_opacity.header_custom_height_new { padding: '.$headerheight.'px 0px!important; }';
	}
}

if (!function_exists('manual_header_page_css_control')) {
	function manual_header_page_css_control($postID) {
		global $theme_options,$post;
		
			if( get_post_meta( $postID, '_manual_nav_style_type', true ) ==  'standard' ) {  
				echo '.navbar { position: inherit; background:none!important; }img.inner-page-white-logo{ display: none; } img.home-logo-show { display: block; } .jumbotron_new .inner-margin-top { padding-top: 0px; }';
			} else { 
				echo '.navbar { position: absolute; width: 100%!important; background: transparent!important; } .jumbotron_new .inner-margin-top { padding-top: 92px; } @media (max-width: 991px) and (min-width: 768px) { .navbar { position: relative!important; background: #FFFFFF!important; } }';
			}
			
			if( get_post_meta( $postID, '_manual_header_background_color_force_white', true ) ==  true ) { echo '@media (min-width:768px) and (max-width:991px) { img.home-logo-show { display: block!important; } img.inner-page-white-logo{ display: none!important; } } @media (max-width:767px) { img.home-logo-show { display: block!important; } img.inner-page-white-logo{ display: none!important; }  }'; }
			
			if( get_post_meta( $postID, '_manual_nav_style_type', true ) ==  'custom' && (get_post_meta($postID, "_manual_header_background_color", true) != '' || get_post_meta( $postID, '_manual_header_image', true ) != '' || get_post_meta( $postID, '_manual_slider_rev_shortcode', true ) != '' ) ) {
				echo '.navbar{ ';
				if( get_post_meta( $postID, '_manual_remove_nav_header_bg_opacity', true ) == true ) echo 'background:none!important;';
				else echo 'background:rgba(58, 58, 64, 0.2)!important;';
				
				if( get_post_meta( $postID, '_manual_remove_nav_border_line', true ) == false ) { echo 'border-bottom:none!important;';
				} else { 
					$rgb_border = hex2rgb(get_post_meta( $postID, '_manual_nav_border_color', true ));
					$border_rgb_color = 'rgba('.$rgb_border[0].','.$rgb_border[1].','.$rgb_border[2].', '.(get_post_meta( $postID, '_manual_nav_border_opacity', true )?get_post_meta( $postID, '_manual_nav_border_opacity', true ):'1').')';
					echo 'border-bottom: 1px solid '.$border_rgb_color.';'; 
				}

				if( get_post_meta( $postID, '_manual_remove_nav_box_shadow', true ) == false ) echo 'box-shadow: none!important;';
				else echo 'box-shadow: 0px 0px 30px 0px rgba(0, 0, 0, 0.05);';
				echo '}  @media (max-width: 991px) and (min-width: 768px) { .navbar { background: #FFFFFF!important; } }  @media (max-width:767px) { .navbar { background: #FFFFFF!important; }  } ';
			}
			
			if( get_post_meta( $postID, '_manual_header_hide_menu_bar', true ) == true ) {
				echo '.navbar { display:none; } .jumbotron_new.jumbotron-inner-fix .inner-margin-top { padding-top:0px!important; }';
			} 
			
			if( get_post_meta($postID, "_manual_header_re_adjust_padding", true) != '' ) {
				echo '.page_opacity.header_custom_height_new { padding: '.get_post_meta($postID, "_manual_header_re_adjust_padding", true).'px 0px!important; }';
			}
			echo '.header_control_text_align { text-align:'.get_post_meta($postID, "_manual_text_align_title_and_desc", true).'!important; }';
			if( get_post_meta( $postID, '_manual_header_image', true ) != '' || get_post_meta( $postID, '_manual_slider_rev_shortcode', true ) != '' ) {
				echo '.noise-break { background-position: '.(get_post_meta( $postID, '_manual_background_img_display_position', true )!=''?get_post_meta( $postID, '_manual_background_img_display_position', true ):'center center').'!important; background-size:cover!important; background:url("'.get_post_meta( $postID, '_manual_header_image', true ).'") } img.inner-page-white-logo{ display: block; } img.home-logo-show { display: none; } .navbar-inverse .navbar-nav>li>a { color:'.$theme_options['first-level-menu-text-color-for-img-bg']['regular'].'!important; } .navbar-inverse .navbar-nav>li>a:hover { color:'.$theme_options['first-level-menu-text-color-for-img-bg']['hover'].'!important; } @media (max-width: 991px) and (min-width: 768px) { img.inner-page-white-logo { display: none; } img.home-logo-show { display: block; } } @media (max-width: 767px) { img.inner-page-white-logo { display: none; } img.home-logo-show { display: block; } } .hamburger-menu span { background: #ffffff; }';
				
				if( get_post_meta( $postID, '_manual_header_bk_opacity_effect', true ) == true ) { 
					echo '.page_opacity.header_custom_height_new{background: rgba(0,0,0,0.3);}';
				} else {
					echo '.page_opacity.header_custom_height_new{background:none;}';
				}
				
			}
			
			if( get_post_meta( $postID, '_manual_nav_style_type', true ) ==  'standard' && (get_post_meta( $postID, '_manual_header_image', true ) != '' || get_post_meta( $postID, '_manual_slider_rev_shortcode', true ) != '')) {
				echo 'img.inner-page-white-logo{ display: none; } img.home-logo-show { display: block; } .navbar-inverse .navbar-nav>li>a { color:'.$theme_options['first-level-menu-text-color']['regular'].'!important; } .navbar-inverse .navbar-nav>li>a:hover { color:'.$theme_options['first-level-menu-text-color']['hover'].'!important; }.jumbotron_new .inner-margin-top { padding-top: 0px; }';
			}
			
			if( get_post_meta($postID, "_manual_header_background_color", true) != '' && get_post_meta( $postID, '_manual_header_image', true ) == '' ) {
				echo '.noise-break{ background:'.get_post_meta($postID, "_manual_header_background_color", true).'; }';
				if( get_post_meta($postID, "_manual_header_background_color_force_white", true) == true && get_post_meta( $postID, '_manual_nav_style_type', true ) ==  'custom' ) {
				echo 'img.inner-page-white-logo{ display: block; } img.home-logo-show { display: none; } .navbar-inverse .navbar-nav>li>a { color: #f7f7f7!important; } .navbar-inverse .navbar-nav>li>a:hover { color: #c3c3c3!important; }';	
				}
			}
			
			// title text control
			$title_text_padding = $title_text_line_height = '';
			$extra_title_text = get_post_meta( $postID, '_manual_extra_title_text_settings', true );
			foreach ( (array) $extra_title_text as $key => $entry ) {
				if ( isset( $entry['title_text_padding'] ) ) {
					$title_text_padding = esc_html( $entry['title_text_padding'] );
				}
				if ( isset( $entry['title_text_line_height'] ) ) {
					$title_text_line_height = esc_html( $entry['title_text_line_height'] );
				}
			}
			if( get_post_meta( $postID, '_manual_page_tagline_color', true ) ==  '' && get_post_meta( $postID, '_manual_header_image', true ) != '' ) { 
				$h1_customh1_header_color = '#FFFFFF';
			} else { 
				$h1_customh1_header_color = get_post_meta( $postID, '_manual_page_tagline_color', true );
			}
			echo 'h1.custom_h1_head { color:'.$h1_customh1_header_color.'!important; font-size:'.get_post_meta( $postID, '_manual_page_tagline_size', true ).'px!important; font-weight: '.get_post_meta( $postID, '_manual_page_tagline_weight', true ).'!important; text-transform: '.get_post_meta( $postID, '_manual_header_title_text_transform', true ).'!important; line-height:'.$title_text_line_height.'!important; font-family: '.get_post_meta( $postID, '_manual_page_tagline_font_family', true ).'!important; padding:'.$title_text_padding.'; }';
			
			// sub title text control
			$subtitle_text_color = $subtitle_text_size = $subtitle_text_weight = $subtitle_font_family = $subtitle_text_transform = $sub_title_text_padding = $sub_title_text_line_height = '';
			$sub_title_text = get_post_meta( $postID, '_manual_subtitle_text_settings', true );
			foreach ( (array) $sub_title_text as $key => $entry ) {
				if ( isset( $entry['title_text_color'] ) ) {
					$subtitle_text_color = esc_html( $entry['title_text_color'] );
				}
				if ( isset( $entry['title_text_size'] ) ) {
					$subtitle_text_size = esc_html( $entry['title_text_size'] );
				}
				if ( isset( $entry['title_text_weight'] ) ) {
					$subtitle_text_weight = esc_html( $entry['title_text_weight'] );
				}
				if ( isset( $entry['subtitle_font_family'] ) ) {
					$subtitle_font_family = esc_html( $entry['subtitle_font_family'] );
				}
				if ( isset( $entry['subtitle_text_transform'] ) ) {
					$subtitle_text_transform = esc_html( $entry['subtitle_text_transform'] );
				}
				if ( isset( $entry['sub_title_text_padding'] ) ) {
					$sub_title_text_padding = esc_html( $entry['sub_title_text_padding'] );
				}
				if ( isset( $entry['sub_title_text_line_height'] ) ) {
					$sub_title_text_line_height = esc_html( $entry['sub_title_text_line_height'] );
				}
			}
			if( $subtitle_text_color ==  '' && get_post_meta( $postID, '_manual_header_image', true ) != '' ) { 
				$p_inner_header_color = '#FFFFFF';
			} else { 
				$p_inner_header_color = $subtitle_text_color;
			}
			
			echo 'p.inner-header-color { color:'.$p_inner_header_color.'!important; font-size:'.$subtitle_text_size.'!important; font-weight: '.$subtitle_text_weight.'!important; font-family: '.$subtitle_font_family.'!important; text-transform:'.$subtitle_text_transform.'; padding: '.$sub_title_text_padding.'; line-height:'.$sub_title_text_line_height.'; }';
			
			// Breadcrumb Control
			$breadcrumb_text_color = $breadcrumb_link_text_color = $breadcrumb_link_text_hover_color = '';
			$breadcrumb_text = get_post_meta( $postID, '_manual_breadcrumb_settings', true );
			foreach ( (array) $breadcrumb_text as $key => $entry ) {
				if ( isset( $entry['text_color'] ) ) {
					$breadcrumb_text_color = esc_html( $entry['text_color'] );
				}
				if ( isset( $entry['link_text_color'] ) ) {
					$breadcrumb_link_text_color = esc_html( $entry['link_text_color'] );
				}
				if ( isset( $entry['link_text_hover_color'] ) ) {
					$breadcrumb_link_text_hover_color = esc_html( $entry['link_text_hover_color'] );
				}
			}
			
			if( $breadcrumb_text_color == '' && get_post_meta( $postID, '_manual_header_image', true ) != '' ) { 
				$breadcrumb_text_color = '#ffffff';
			} else { 
				$breadcrumb_text_color = $breadcrumb_text_color;
			}
			
			if( $breadcrumb_link_text_color == '' && get_post_meta( $postID, '_manual_header_image', true ) != '' ) { 
				$breadcrumb_link_text_color = '#eff1f5';
			} else { 
				$breadcrumb_link_text_color = $breadcrumb_link_text_color;
			}
			
			echo '#breadcrumbs span, #breadcrumbs { color:'.$breadcrumb_text_color.'; } #breadcrumbs a { color:'.$breadcrumb_link_text_color.'; } #breadcrumbs a:hover { color:'.$breadcrumb_link_text_hover_color.'; }' ;
			
			// Trending Search
			$trending_text_color = $trending_link_text_color = $trending_link_text_hover_color = '';
			$trending_search_text = get_post_meta( $postID, '_manual_trending_search_settings', true );
			foreach ( (array) $trending_search_text as $key => $entry ) {
				if ( isset( $entry['text_color'] ) ) {
					$trending_text_color = esc_html( $entry['text_color'] );
				}
				if ( isset( $entry['link_text_color'] ) ) {
					$trending_link_text_color = esc_html( $entry['link_text_color'] );
				}
				if ( isset( $entry['link_text_hover_color'] ) ) {
					$trending_link_text_hover_color = esc_html( $entry['link_text_hover_color'] );
				}
			}
			
			if( $trending_text_color == '' && get_post_meta( $postID, '_manual_header_image', true ) != '' ) { 
				$trending_text_color = '#ffffff';
			} else { 
				$trending_text_color = $trending_text_color;
			}
			
			if( $trending_link_text_color == '' && get_post_meta( $postID, '_manual_header_image', true ) != '' ) { 
				$trending_link_text_color = '#eff1f5';
			} else { 
				$trending_link_text_color = $trending_link_text_color;
			}
			
			echo '.trending-search span.popular-keyword-title { color:'.$trending_text_color.'; } .trending-search a { color:'.$trending_link_text_color.'!important; } .trending-search a:hover { color:'.$trending_link_text_hover_color.'!important; }';
			
		// eof page control
	}
}

if (!function_exists('manual_header_display_control_break_page')) {
	function manual_header_display_control_break_page($postID) {
		global $theme_options,$post;
		$url = '';
		$searchbox = $parallax_effect = false;
		$revslider = get_post_meta($postID, "_manual_slider_rev_shortcode", true);
		if (!empty($revslider)){ 
			echo '<div class="q_slider"><div class="q_slider_inner">'.do_shortcode($revslider).'</div></div>';
		} else {
			if( !get_post_meta( $postID, '_manual_header_hide_header_bar', true ) == 'on' ) { 
				$pagetitle = get_post_meta( $postID, '_manual_page_tagline', true );
				if( get_post_meta( $postID, '_manual_header_searh_box', true ) == true ) { $searchbox = true; }
				if( get_post_meta( $postID, '_manual_header_parallax_effect', true ) == true && get_post_meta( $postID, '_manual_header_image', true ) != '' ) {
					$url = get_post_meta( $postID, '_manual_header_image', true );
					$parallax_effect = true;
				}
				if( get_post_meta( $postID, '_manual_header_no_title', true ) == true ) $replace_title_act = false;
				else $replace_title_act =  true;
				
				if( get_post_meta( $postID, '_manual_page_desc', true ) != '' ) { 
					$subtitle = get_post_meta( $postID, '_manual_page_desc', true );
					$subtitle_act = true;
				} else {
					$subtitle = '';
					$subtitle_act = false;
				}
				
				if( get_post_meta( $postID, '_manual_header_breadcrumb_status', true ) == true ) $breadcrumb = true;
				else $breadcrumb = false;
					
				manual_header_design_control( get_post_type(), $url, $parallax_effect, $searchbox, $pagetitle, $replace_title_act, $subtitle, $subtitle_act, $breadcrumb);
			}
		}
	}
}

/********************************************
*** EOF PAGE CONTROL HEADER SETTINGS / CSS **
*********************************************/

if (!function_exists('manual_header_display_control_check')) {
	function manual_header_display_control_check() {
		global $theme_options;
		$check_home_current_page = basename( get_page_template() );
		if( $check_home_current_page == 'template-home.php' && $theme_options['home-switch-to-page-header-status'] == true ) { } else {
			manual_header_display_control_break();
		}
	}
}

if (!function_exists('manual_header_display_control_break')) {
	function manual_header_display_control_break() {
		global $theme_options,$post;
		
		$current_post_type = get_post_type();
		
		if( is_404() ) {
			if( $theme_options['home-404-search-bar-status'] == true ) $searchbar = false;
			else  $searchbar = true;
			manual_header_design_control( '', '', false, $searchbar, '', false, '', false, false);
		} else if( is_search() ) {
			if( $theme_options['search-page-header-search-bar-status'] == true ) $searchbar = true;
			else  $searchbar = false;
			
			if( $theme_options['search-page-header-title'] != '' ) $pagetitle = $theme_options['search-page-header-title'];
			else $pagetitle = '';
			
			if( $theme_options['search-page-header-sub-title'] != '' ) $subtitle = $theme_options['search-page-header-sub-title'];
			else $subtitle = '';
			
			manual_header_design_control( '', '', false, $searchbar, $pagetitle, false, $subtitle, false, false);
		} else if( $current_post_type == 'manual_portfolio' ) {  
			manual_header_display_control_break_page($post->ID);
		} else if($current_post_type == 'manual_faq') {
			if( $theme_options['faq-cat-header-search-status'] == false )  $searchbar = true;
			else  $searchbar = false;
			if( $theme_options['faq-cat-header-breadcrumb-status'] == false )  $breadcrumb = true;
			else  $breadcrumb = false;
			manual_header_design_control($current_post_type, '', false, $searchbar, '', true, '', false, $breadcrumb);
		} else if($current_post_type == 'manual_kb') {
			if( is_single() ) {
				if( $theme_options['kb-single-pg-header-search-status'] == false )  $searchbar = true;
				else  $searchbar = false;
				if( $theme_options['kb-single-pg-header-breadcrumb-status'] == false )  $breadcrumb = true;
				else  $breadcrumb = false;
				manual_header_design_control($current_post_type, '', false, $searchbar, '', true, '', false, $breadcrumb);
			} else {
				if( $theme_options['kb-cat-header-search-status'] == false ) { $searchbar = true;
				} else { $searchbar = false; }
				if( $theme_options['kb-cat-header-breadcrumb-status'] == false ) { $breadcrumb = true;
				} else { $breadcrumb = false; }
				manual_header_design_control($current_post_type, '', false, $searchbar, '', true, '', false, $breadcrumb);
			}
	   } else if($current_post_type == 'manual_documentation') {
			if( is_single() ) {
				if( $theme_options['documentation-disable-search-category-page'] == false ) { $searchbar = true;
				} else { $searchbar = false; }
				if( $theme_options['documentation-disable-breadcrumb-category-page'] == false ) { $breadcrumb = true;
				} else { $breadcrumb = false; }
				manual_header_design_control($current_post_type, '', false, $searchbar, '', true, '', false, $breadcrumb);
			} else {
				if( $theme_options['documentation-disable-search-category-page'] == false ) { $searchbar = true;
				} else { $searchbar = false; }
				if( $theme_options['documentation-disable-breadcrumb-category-page'] == false ) { $breadcrumb = true;
				} else { $breadcrumb = false; }
				manual_header_design_control($current_post_type, '', false, $searchbar, '', true, '', false, $breadcrumb);
			}
	  } else if(function_exists("is_woocommerce") && (is_shop() || is_checkout() || is_account_page())){ // woo
			if(is_shop()){
				$page_id = get_option('woocommerce_shop_page_id');
			} elseif(is_checkout()) {
				$page_id = get_option('woocommerce_pay_page_id'); 
			} elseif(is_account_page()) {
				$page_id = get_option('woocommerce_myaccount_page_id'); 
			} elseif(is_account_page()) {
				$page_id = get_option('woocommerce_edit_address_page_id'); 
			} elseif(is_account_page()) {
				$page_id = get_option('woocommerce_view_order_page_id'); 
			}
			$woopage  = get_post( $page_id );
			manual_header_display_control_break_page($woopage->ID);
			
		} else if ( class_exists('bbPress') && is_bbPress() ) {  // bbpress
			manual_header_design_control();
		} else if( is_home() && $current_post_type == 'post' ) { // post home page
			$postID = get_option('page_for_posts');
			manual_header_display_control_break_page($postID);
		} else if( $current_post_type == 'page' ) {   // page
			manual_header_display_control_break_page($post->ID);
		} else if( $current_post_type == 'post' && $theme_options['blog_single_page_global_header_settings'] == true && !is_single() ) { // post !single page
			$postID = get_option('page_for_posts');
			manual_header_display_control_break_page($postID);
		} else if( $current_post_type == 'post' && is_single() ) { // post single page
			manual_header_design_control();
		} else {  
			$url = '';
			$parallax_effect = $searchbox = false;
			if( $theme_options['default-header-sytle-backgorund-image'] == true && isset($theme_options['manual-header-custom-image']['url']) && $theme_options['manual-header-custom-image']['url'] != '' && $theme_options['header-uploadimage-parallax-effect'] == 1 ) { 
				$parallax_effect = true;
				$url = $theme_options['manual-header-custom-image']['url'];
			}
			if( isset($theme_options['target-search-bar-display-on-the-header']) && $theme_options['target-search-bar-display-on-the-header'] != '') {
				foreach ( $theme_options['target-search-bar-display-on-the-header']  as $post_type ) {
					if( $post_type == 'manual_ourteam' ||
						$post_type == 'manual_tmal_block' ||
						$post_type == 'manual_org_block' ||
						$post_type == 'manual_hp_block'  ||
						$post_type == 'reply' ||
						$post_type == 'topic' || $post_type == 'page' ) { continue; }
						$activate_post_type[] = $post_type;
				}
				if( !empty( $activate_post_type ) && in_array($current_post_type, $activate_post_type) ) {
					if( $theme_options['activate-search-bar-on-the-header'] == true ) $searchbox = true;
				}
			}
			manual_header_design_control('', $url, $parallax_effect, $searchbox);	
		}
	}
}


if (!function_exists('manual_header_design_control')) {
	function manual_header_design_control($current_post_type = '', $url = '', $parallax_effect = false, $searchbox = false, $replace_title='', $replace_title_act = true, $subtitle = '', $subtitle_act = false, $breadcrumb = true) {
		global $theme_options, $product, $post;
		$plx_data_src = $plx_bk_img = $auto_adjust_padding =  $plx_class = '';
		
			if($parallax_effect == true && $url != '' ) {
				$plx_data_src = 'data-image-src="'.$url.'"  data-parallax="scroll"';
				$plx_bk_img = "background:none;";
				$plx_class = 'parallax-window';
			}
			echo '<div class="jumbotron_new inner-jumbotron jumbotron-inner-fix noise-break header_custom_height '.$plx_class.'" '.$plx_data_src.' style="'.$plx_bk_img.' ">
			<div class="page_opacity header_custom_height_new">
			  <div class="container inner-margin-top">
				<div class="row">
				  <div class="col-md-12 col-sm-12 header_control_text_align"> 
				  	<h1 class="inner-header custom_h1_head">';
					if( $current_post_type == 'manual_faq' ) {
						$faq_catname = get_term_by( 'slug', get_query_var( 'term' ), 'manualfaqcategory' );
						if( !empty($faq_catname) ) {
							echo $faq_catname->name;
						} else {
							$terms = get_the_terms( $post->ID , 'manualfaqcategory' ); 
							if( !empty($terms) ) echo $terms[0]->name;
						}
					} else if( $current_post_type == 'manual_kb' ) { 
						$kb_catname = get_term_by( 'slug', get_query_var( 'term' ), 'manualknowledgebasecat' );
						if( !empty($kb_catname) ) {
							echo $kb_catname->name;
						} else {
							if ( is_tax() ) { 
								$current_term = get_term_by( 'slug', get_query_var( 'term' ), 'manual_kb_tag' );
								echo $current_term->name;
							} else { 
								$terms = get_the_terms( $post->ID , 'manualknowledgebasecat' ); 
								if( !empty($terms) ) echo $terms[0]->name;
							}
						}
					} else if( $current_post_type == 'manual_documentation' ) { 
						$doc_catname = get_term_by( 'slug', get_query_var( 'term' ), 'manualdocumentationcategory' );
						if( !empty($doc_catname) ) {
							echo $doc_catname->name;
						} else {
							$terms = get_the_terms( $post->ID , 'manualdocumentationcategory' ); 
							if( !empty($terms) ) echo $terms[0]->name;
						}
					} else if ( class_exists('bbPress') && is_bbPress() ) { // bbpress
						echo $theme_options['manual-forum-title'];
					} else if( $current_post_type == 'page' || (is_home() && $current_post_type == 'post') || ($current_post_type == 'manual_portfolio') 
								||  function_exists("is_woocommerce") && function_exists("is_product") && is_product() && is_single() /*woo handle - 1*/ 
								||  function_exists("is_woocommerce") && (is_shop())  /*woo handle - 2*/  ) { 
						if( $replace_title_act == true ) echo (($replace_title != '')?$replace_title:get_the_title());
					} else if( function_exists("is_product_category") && is_product_category() /*woo handle - 3*/ ) {
						echo single_term_title();
					} else if( (is_single()) ) {
						$postID = get_option('page_for_posts');
						$pagetitle = get_post_meta( $post->ID, '_manual_page_tagline', true );
						echo ($theme_options['blog_single_title_on_header'] ==  false? (($pagetitle != '')?$pagetitle:'') :get_the_title());
					} else {
						 if ( is_category() ) {
							 echo single_cat_title( '', false ); 
						 } else if ( is_tag() ) {
							echo single_cat_title( '', false ); 
						 } else if( is_archive() ) {
							if ( is_day() ) :
									printf( esc_html__( 'daily archives: %s', 'manual' ), get_the_date() );
								elseif ( is_month() ) :
									printf( esc_html__( 'monthly archives: %s', 'manual' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'manual' ) ) );
								elseif ( is_year() ) :
									printf( esc_html__( 'yearly archives: %s', 'manual' ), get_the_date( _x( 'Y', 'yearly archives date format', 'manual' ) ) );
								else :
									esc_html_e( 'archives', 'manual' );
							endif;
						 } else if( is_404() ) {
							 echo $theme_options['home-404-main-title'];
						 } else if( is_search() ) {
						 	echo $replace_title;
						 } else {
							echo get_the_title();
						 }
					}
					echo '</h1>';
					// subtitle
					if( (is_home() && $current_post_type == 'post') || $current_post_type == 'page' && $subtitle_act == true 
								|| function_exists("is_woocommerce") && (is_shop()) || ($current_post_type == 'manual_portfolio') ) { 
						echo '<p class="inner-header-color">'.$subtitle.'</p>';
					} else if ( class_exists('bbPress') && is_bbPress() ) {
						if( !empty($theme_options['manual-forum-subtitle']) && bbp_is_forum_archive() ) { 
							echo '<p class="inner-header-color">'.$theme_options['manual-forum-subtitle'].'</p>';
						}
					} else if( is_search() ) {
						if( $subtitle == '' ) $space_quot = '';
						else $space_quot = '&quot;';
						 echo '<p class="inner-header-color">'.$subtitle.'&nbsp;<b>'.$space_quot.''.get_search_query().''.$space_quot.'</b></p>';
					 } 
					// breadcurmb
					if(function_exists("is_product") && is_product() || function_exists("is_product_category") && is_product_category() ){ // woo
						$woo_args = array(
								'delimiter' => '<span class="sep">/</span>',
								'wrap_before' => '<div id="breadcrumbs">',
								'wrap_after' => '</div>',
						);
						echo '<div class="inner-header-color">'.woocommerce_breadcrumb($woo_args) .'</div>'; 
					} else if ( class_exists('bbPress') && is_bbPress() ) { //bbpress
						if( $theme_options['bbpress_breadcrumb_status'] == true ) {
							if( bbp_is_single_user() != true && !bbp_is_forum_archive() ) {
								$manual_breadcrumbs_args = array(
									'before'          => '',
									'after'           => '',
									'sep'             => esc_html__( '/', 'manual' ),
									'home_text'       => esc_html__( 'Home', 'manual' ),
									(($theme_options['bbpress_breadcrumb'] ==  true )?'':'include_root') => ( ($theme_options['bbpress_breadcrumb'] ==  true )? '' : false ),
								);
								echo '<div id="breadcrumbs" class="forum-breadcrumbs">'.bbp_get_breadcrumb($manual_breadcrumbs_args).'</div>'; 
							}
						}
					} else {
						if( is_single() && $theme_options['blog_single_breadcrumb_on_header'] == false ) $breadcrumb = false;
						if( !is_404() && $breadcrumb == true ) { 
							echo '<div class="inner-header-color">'.manual_breadcrumb().'</div>'; 
						}
					}
					
					// search
					if ( class_exists('bbPress') && is_bbPress() ) {
						if( $theme_options['bbpress_search_status'] == true ) {
							echo '<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1 search-margin-top"><div class="global-search">';
							get_template_part( 'search', 'home' );
							echo '</div></div>';
						}
					} else if($searchbox == true) { 
						echo '<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1 search-margin-top"><div class="global-search">';
						get_template_part( 'search', 'home' );
						echo '</div></div>';
					} 
                    
			  echo '</div>
			</div>
		  </div>
		  </div>
		</div>';
		
	}
}

/********************************************
*** LOGIN FORM **
*********************************************/
if (!function_exists('manual_get_login_form_control')) {
	function manual_get_login_form_control($custom_login_message) {
		$return = '<div class="manual_login_page"> <div class="custom_login_form">';
		if( $custom_login_message != '' ) { $return .= '<h6>'.stripslashes($custom_login_message).'</h6>';  }
		$return .= '<ul class="custom_register">';
		$return .= '<li><a href="'.wp_login_url().'" title="Login">'.esc_html__( 'Login', 'manual' ).'</a></li></ul>';
		$return .= '</div></div>';
		return $return;
	}
}


/*-----------------------------------------------------------------------------------*/
/*	FOOTER 1
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_footer_controls_lower')) {
	function manual_footer_controls_lower() {
		global $theme_options, $post;
		$current_post_type = get_post_type();
		$hide_widget_area = ''; 
		
		if( (is_page() || $current_post_type == 'manual_portfolio') && (get_post_meta( $post->ID, '_manual_footer_force_hide_widget_area', true ) == true) ) { 
			$hide_widget_area = 1;
		} else {  
			if( $theme_options['footer-widget-status'] == true )  $hide_widget_area = 1;
			else $hide_widget_area = 2;
		}

        echo '<footer><div class="footer-bg custom-footer';
			if( $theme_options['theme-footer-type'] == 1 ) { echo 'footer-type-one'; } 
		echo '"';
			if( $hide_widget_area != 1 || $theme_options['theme-footer-type'] != 2 ) {  echo 'style="padding: 65px 0px 0px;"'; }
        echo '>';
    
	    if( $theme_options['theme-footer-type'] == 1 ) { 
			echo '<div class="container"> <div class="row"> <div class="col-md-12 col-sm-12 footer-btm-box-one" style="text-align:center;">
					<ul class="social-foot" style="margin-bottom: 35px;">';
						manual_footer_social_share();
					echo '</ul>
					<p style=" font-size: 12px;">';
					if( isset($theme_options['footer-copyright']) && $theme_options['footer-copyright'] != '' ) { echo $theme_options['footer-copyright']; }
					echo '</p>
				</div> </div> </div>';
		} else { 
		
			// widget section
			if( $hide_widget_area != 1 ) { 
			if( isset( $theme_options['theme_footer_noof_section_widget_area'] ) && $theme_options['theme_footer_noof_section_widget_area'] != '' ) {
				if( $theme_options['theme_footer_noof_section_widget_area'] == 4 ) {
					$col_md = 3;
					$col_sm = 6;
					$hide_widget = 4;
				} else if( $theme_options['theme_footer_noof_section_widget_area'] == 3 ) {
					$col_md = 4;
					$col_sm = 6;
					$hide_widget = 3;
				} else if( $theme_options['theme_footer_noof_section_widget_area'] == 2 ) {
					$col_md = 6;
					$col_sm = 6;
					$hide_widget = 2;
				} else if( $theme_options['theme_footer_noof_section_widget_area'] == 1 ) {
					$col_md = 12;
					$col_sm = 12;
					$hide_widget = 1;
				}
			} else {
				$col_md = 3;
				$col_sm = 6;
				$hide_widget = 4;
			}
			echo '<div class="container"> <div class="row">
				  
					<div class="col-md-'.$col_md.' col-sm-'.$col_sm.'">';
					if ( is_active_sidebar( 'footer-box-1' ) ) : 
						dynamic_sidebar( 'footer-box-1' ); 
					endif; 
					echo '</div>';
					
					if( $hide_widget == 4 || $hide_widget == 3 || $hide_widget == 2  ) {
						echo '<div class="col-md-'.$col_md.' col-sm-'.$col_sm.'">';
								if ( is_active_sidebar( 'footer-box-2' ) ) : 
									dynamic_sidebar( 'footer-box-2' ); 
								endif; 
						echo '</div>';
					}
				
					if( $hide_widget == 4 || $hide_widget == 3 && $hide_widget != 2  ) {
						echo '<div class="col-md-'.$col_md.' col-sm-'. $col_sm.'">';
								if ( is_active_sidebar( 'footer-box-3' ) ) : 
									dynamic_sidebar( 'footer-box-3' ); 
								endif; 
						echo '</div>';
					} 
				
					if( $hide_widget == 4 && $hide_widget != 3 && $hide_widget != 2  ) {
						echo '<div class="col-md-'.$col_md.' col-sm-'.$col_sm.'">';
								if ( is_active_sidebar( 'footer-box-4' ) ) : 
									dynamic_sidebar( 'footer-box-4' ); 
								endif; 
						echo '</div>';
					} 

					if( $hide_widget == 4 && $hide_widget != 3 && $hide_widget != 2  ) {
						echo '<div class="col-md-'.$col_md.' col-sm-'.$col_sm.'">';
								if ( is_active_sidebar( 'footer-box-5' ) ) : 
									dynamic_sidebar( 'footer-box-5' ); 
								endif; 
						echo '</div>';
					} 
                
			  echo '</div> </div>';
		     } 
			 
			 // copyright section
			 if( $theme_options['footer-social-copyright-status'] == false ) {
			 echo '<div class="footer_social_copyright custom_copyright"> <div class="container"> <div class="row">';
			 echo '<div class="col-md-12 col-sm-12 footer-btm-box" style="'; 
			 	if( $theme_options['footer-disable-social-icons'] == true ) { echo 'padding-top: 19px;';} 
             echo '">';
					
				  if( $theme_options['footer-disable-social-icons'] == false ) { 
					echo '<ul class="social-foot" style="padding-left:0px;border-bottom:1px solid rgba(249, 249, 249, 0.05);">';
					  manual_footer_social_share();
					echo '</ul>';
				  } 
				
				  $display_copyright_style = 'float:none;';	
				  if ( has_nav_menu( 'footer' ) ) { 
					 wp_nav_menu( array( 'theme_location' => 'footer', 'menu_class' => 'footer-secondary', 'container' => 'ul', 'container_class' => '', 'link_before' => '', 'link_after' => '', 'fallback_cb' => false, 'menu_id' => 'footer_menu', 'depth' => 1 ));
					 $display_copyright_style = ''; 	
				  }
				  
				 if( $theme_options['footer-disable-copyright-section'] == false ) {
					echo '<ul class="footer-tertiary copyright-section secondaryLink" style="'.$display_copyright_style.'"> <li> <p>';
						  if( isset($theme_options['footer-copyright']) && $theme_options['footer-copyright'] != '' ) { echo $theme_options['footer-copyright']; } 
					echo '</p> </li> </ul>';
				  }

				  echo "<div class='footer-logo'>";
				  		$white_url = get_stylesheet_directory_uri().'/img/logo-footer.jpg';
				  		echo '<img src="'.esc_url( $white_url ).'" class="custom-footer-logo" alt="">'; 
				  echo "</div>";
				  
				  echo '</div> </div> </div> </div>';
			 } // end of copyright section

			} // end of main else 
	  echo '</div></footer>'; // main div close 
	} // function close
}


/*-----------------------------------------------------------------------------------*/
/*	MANUAL :: SEARCH ACCESS CONTROL
/*-----------------------------------------------------------------------------------*/
if (!function_exists('manual_search_content_access_control')) {
	function manual_search_content_access_control($postID,$texonomycat,$cat_login,$cat_role,$article_login,$article_role) {
		//access control check for category
		$terms = get_the_terms( $postID , $texonomycat ); 
		$check_if_login_call = get_option( $cat_login.$terms[0]->term_id );
		$check_user_role = get_option( $cat_role.$terms[0]->term_id );
		if( $check_if_login_call == 1 && !is_user_logged_in() ) {
			$post_access_display = 'none';
		} else {
			$access_status = manual_doc_access($check_user_role);
			if( $check_if_login_call == 1 && is_user_logged_in() && $access_status == false ) { 
				$post_access_display = 'none';
			} else {
				// check for single page
				$access_meta = get_post_meta( $postID, $article_login, true );
				$check_post_user_level_access = get_post_meta( $postID, $article_role, true );
				if( isset($access_meta['login']) && $access_meta['login'] == 1 && !is_user_logged_in() ) {
					$post_access_display = 'none';
				} else {
					if( !empty($check_post_user_level_access) )  $access_status = manual_doc_access(serialize($check_post_user_level_access));
					else  $access_status = true;
					
					if(  isset($access_meta['login']) && $access_meta['login'] == 1 && is_user_logged_in() && $access_status == false ) {
						$post_access_display = 'none';
					} else {
						$post_access_display = '';
					}
				}
				// eof single page check 
			}
		} 
		//eof access control check for category
		return $post_access_display;
	} // function close
}
?>