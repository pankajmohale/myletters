<?php

/*************************************
***      
One Click Import 
https://github.com/proteusthemes/one-click-demo-import     
***
**************************************/

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active( 'one-click-demo-import/one-click-demo-import.php' ) ) {


	// START IMPORT
	function ocdi_import_files() {
	  return array(
	  
		array(
            'import_file_name'             => 'Manual Original',
            'categories'                   => array( 'Manual Original'),
            'local_import_file'            => trailingslashit( get_template_directory() ) . 'framework/import/demo-content.xml',
            'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'framework/import/widgets.wie',
            'local_import_redux'           => array(
                array(
                    'file_path'   => trailingslashit( get_template_directory() ) . 'framework/import/redux.json',
                    'option_name' => 'redux_demo',
                ),
            ),
            'import_preview_image_url' =>  trailingslashit( get_template_directory() ) . 'img/preview_import_1.jpg',
            'import_notice'            => '<strong style="color: #dc2f2f;">Possible issue: Can\'t import Demo content</strong><br>Sometimes your server has \'low upload size and minumim execution time\' creating an issue. Please follow below link URL that shows how to increase execution and upload size limit. <a href="http://www.wpbeginner.com/wp-tutorials/how-to-increase-the-maximum-file-upload-size-in-wordpress/" target="_blank">Tutorial</a>
			
			<br><br><strong style="color: #17d639;">Important</strong>: <br>After you import this demo, you will have to setup the slider separately from; "Slider Revolution -> Import Slider".<br> Please download slider using link: <a href="http://demo.wpsmartapps.com/themes/manual/MANUAL-ReV-SlideR/" target="_blank">Download Sliders</a>',
        ),
		
	  );
	}
	add_filter( 'pt-ocdi/import_files', 'ocdi_import_files' );
	
	
	// AUTOMATICALLY ASSIGN 'FRONT PAGE' & MENU LOCATION AFTER THE IMPORT IS DONE.
	function ocdi_after_import_setup() {
		// Assign menus to their locations.
		$main_menu = get_term_by( 'name', 'Primary Menu', 'nav_menu' );
	
		set_theme_mod( 'nav_menu_locations', array(
				'primary' => $main_menu->term_id,
			)
		);
	
		// Assign front page and posts page (blog page).
		$front_page_id = get_page_by_title( 'Home Landing' );
		$blog_page_id  = get_page_by_title( 'News' );
	
		update_option( 'show_on_front', 'page' );
		update_option( 'page_on_front', $front_page_id->ID );
		update_option( 'page_for_posts', $blog_page_id->ID );
	
	}
	add_action( 'pt-ocdi/after_import', 'ocdi_after_import_setup' );
	
	
	// CHANGE THE LOCATION
	function ocdi_plugin_page_setup( $default_settings ) {
		$default_settings['parent_slug'] = 'themes.php';
		$default_settings['page_title']  = esc_html__( 'Manual Demo Import' , 'manual' );
		$default_settings['menu_title']  = esc_html__( 'Manual Demo Import' , 'manual' );
		$default_settings['capability']  = 'import';
		$default_settings['menu_slug']   = 'manual-demo-import';
		return $default_settings;
	}
	add_filter( 'pt-ocdi/plugin_page_setup', 'ocdi_plugin_page_setup' );

}

?>