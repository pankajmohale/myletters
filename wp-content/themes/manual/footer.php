<?php global $theme_options;?>
</div>
</div>

<!-- SECTION FOOTER TOP-->
<?php 
manual_footer_controls(); 
//SECTION FOOTER -->
manual_footer_controls_lower();
// HOOK -->
wp_footer(); 
if( !empty( $theme_options['manual-editor-js'] ) ) {
	echo "<script type='text/javascript'>"; 
	echo $theme_options['manual-editor-js'];
	echo "</script>";
}
manual_google_analytics_code("body");
?>
</body></html>