<?php
/**
* Set the content width based on the theme's design and stylesheet.
*/
if ( ! isset( $content_width ) ) $content_width = 700;

/*-----------------------------------------------------------------------------------*/
/*	Sets up theme defaults and registers support for various WordPress features.
/*-----------------------------------------------------------------------------------*/
if ( ! function_exists( 'manual_theme_manual_setup' ) ) {
	function manual_theme_manual_setup() {
		
        /*	Load Text Domain */
		load_theme_textdomain( 'manual', trailingslashit( get_template_directory() ) . 'languages' );
		
        /*	Add Automatic Feed Links Support */
        add_theme_support( 'automatic-feed-links' );
		
        /* Add Post Formats Support */
        add_theme_support('post-formats', array('gallery', 'link', 'image', 'quote', 'video', 'audio'));
		
		/*** If BBPress is active, add theme support */
		if ( class_exists( 'bbPress' ) ) { add_theme_support( 'bbpress' ); }

		/* Let WordPress manage the document title. */
		add_theme_support( 'title-tag' );
		
		/* Add Post Thumbnails Support and Related Image Sizes */
		add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 825, 510, true );
		
		/** This theme uses wp_nav_menu() in one location. */
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu',  'manual' ),
			'footer'  => esc_html__( 'Footer Menu',  'manual' ),
		) );
		
		/** Custom image sizes */
		add_image_size( 'portfolio-FitRows', 700, 525, true ); 
		
	}
}
add_action( 'after_setup_theme', 'manual_theme_manual_setup' );

/*-----------------------------------------------------------------------------------*/
/*	Framework :: REDUXCORE (Manual Theme Options)
/*-----------------------------------------------------------------------------------*/
require_once( trailingslashit( get_template_directory() ) . 'framework/ReduxCore/manual/manual.php' );

/*-----------------------------------------------------------------------------------*/
/*	Framework :: CMB2 (META TYPE)
/*-----------------------------------------------------------------------------------*/ 
require trailingslashit( get_template_directory() ) . 'framework/post-meta/kb.php';
require trailingslashit( get_template_directory() ) . 'framework/post-meta/portfolio.php';
require trailingslashit( get_template_directory() ) . 'framework/post-meta/page.php';
require trailingslashit( get_template_directory() ) . 'framework/post-meta/custom-meta.php';
require trailingslashit( get_template_directory() ) . 'framework/post-meta/doc.php';
require trailingslashit( get_template_directory() ) . 'framework/post-meta/faq.php';
require trailingslashit( get_template_directory() ) . 'framework/post-meta/home-pg-block.php';

/*-----------------------------------------------------------------------------------*/
/*	Customizer
/*-----------------------------------------------------------------------------------*/ 
require trailingslashit( get_template_directory() ) . 'framework/customizer.php';

/*-----------------------------------------------------------------------------------*/
/*	Custom template tags for this theme
/*-----------------------------------------------------------------------------------*/ 
require trailingslashit( get_template_directory() ) . 'framework/template-tags.php';

/*-----------------------------------------------------------------------------------*/
/*	Widget Register & Placement
/*-----------------------------------------------------------------------------------*/ 
require trailingslashit( get_template_directory() ) . 'framework/widget/widget-type.php';
require trailingslashit( get_template_directory() ) . 'framework/widget/widget.php';

/*-----------------------------------------------------------------------------------*/
/*	Breadcrumb
/*-----------------------------------------------------------------------------------*/ 
require trailingslashit( get_template_directory() ) . 'framework/breadcrumb.php';

/*-----------------------------------------------------------------------------------*/
/*	DOC=AJAX
/*-----------------------------------------------------------------------------------*/ 
require trailingslashit( get_template_directory() ) . 'framework/doc-ajax.php';

/*-----------------------------------------------------------------------------------*/
/*	HOOK
/*-----------------------------------------------------------------------------------*/
require trailingslashit( get_template_directory() ) . 'framework/includes/hook.php';

/*-----------------------------------------------------------------------------------*/
/*	SUPPORT FUNCTION
/*-----------------------------------------------------------------------------------*/ 
require trailingslashit( get_template_directory() ) . 'framework/includes/functions.php';

/*-----------------------------------------------------------------------------------*/
/*	Custom Menu
/*-----------------------------------------------------------------------------------*/  
require trailingslashit( get_template_directory() ) . 'framework/includes/menu.php';

/*-----------------------------------------------------------------------------------*/
/*	WOOCOMMERCE
/*-----------------------------------------------------------------------------------*/
require trailingslashit( get_template_directory() ) . 'woocommerce/woocommerce_configuration.php';

/*-----------------------------------------------------------------------------------*/
/*	IMPORT
/*-----------------------------------------------------------------------------------*/ 
require trailingslashit( get_template_directory() ) . 'framework/import/one-click-import.php';

/*-----------------------------------------------------------------------------------*/
/*	Manual Framework Plugin Update Checker
/*-----------------------------------------------------------------------------------*/ 
$manual_theme_framework_version_check = "1.5"; 
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if ( is_plugin_active('manual-framework/manual-framework.php') ) {
	$manual_theme_framework_version = get_option( 'manual_theme_framework_version' );
	if( $manual_theme_framework_version != $manual_theme_framework_version_check || $manual_theme_framework_version == '' ) {  
		add_action('admin_notices', 'manual_plugin_notify');
	}
}

function manual_plugin_notify() {
$allowed_html_array = array('a' => array('href' => array(),'title' => array()),'br' => array(),'div' => array('class' => array('manual_adminmsg'),),'strong' => array(),'span' => array(),);
echo wp_kses( '<div class="manual_adminmsg"><span>PLEASE UPGRADE "Manual Framework (Post Type)" to new version 1.5</span> <br><br> 1. Go to: Plugins -> Installed Plugins. <br>2. <strong>DELETE plugin</strong> "Manual Framework (Post Type)" for the system. <strong><i>(you must DEACTIVATE plugin first and DELETE it)</i></strong> <br> 3. <strong>Click on "Begin installing plugin"</strong>, to install new version. </span> <br><br><span>IMPORTANT: Team section (left sidebar menu) & and its related component will be removed from the version 1.5, Please use VC shortcode "Team" to replace this feature</span>.<br><i>Note: No data will be loss in this upgrade process.</i> </div>', $allowed_html_array);
}

/*-----------------------------------------------------------------------------------*/
/*	ACTIVATE VC
/*-----------------------------------------------------------------------------------*/
if ( is_plugin_active('js_composer/js_composer.php') ) {
	
	// check the latest vc version
	if( version_compare(WPB_VC_VERSION, '5.4.5', '<' ) ) {
		add_action('admin_notices', 'manual_plugin_vc_update_notify');
	}
	//if(function_exists('vc_set_as_theme')) vc_set_as_theme(true);   // Activate on last
	require trailingslashit( get_template_directory() ) . 'framework/vc.php';
	require trailingslashit( get_template_directory() ) . 'framework/shortcodes.php';
	
	function manual_vc_remove_frontend_links() {
		vc_disable_frontend(); // this will disable frontend editor
	}
	add_action( 'vc_after_init', 'manual_vc_remove_frontend_links' );

}

function manual_plugin_vc_update_notify() {
$allowed_html_array = array('a' => array('href' => array(),'title' => array()),'br' => array(),'div' => array('class' => array('manual_adminmsg'),),'strong' => array(),'span' => array(),);
echo wp_kses( '<div class="manual_adminmsg"><div class="message" style="padding: 10px; font-size: 14px; color: #FCFCFC; color: #000000; background: white; box-shadow: 1px 1px 10px #828181;"><span style="color: #C31111; font-weight:bold;">PLEASE UPGRADE "WPBakery Visual Composer" to the new version 5.4.5</span> <br><br> 1. Go to: Plugins -> Installed Plugins. <br>2. <strong>DELETE plugin</strong> "WPBakery Visual Composer" for the system. <strong><i>(you must DEACTIVATE plugin first and DELETE it)</i></strong> <br> 3. <strong>Click on "Begin installing plugin"</strong>, to install new version.</span> <br><br><i>Note: No data will be loss in this upgrade process.</i> </div></div>', $allowed_html_array);
}


/*-----------------------------------------------------------------------------------*/
/*	CHECKER SR PLUGIN
/*-----------------------------------------------------------------------------------*/ 
if ( is_plugin_active('revslider/revslider.php') ) {
	global $revSliderVersion;
	if( version_compare( $revSliderVersion, '5.4.6.4', '<' ) ) {
		add_action('admin_notices', 'manual_plugin_slider_revolution_update_notify');
	}
}
function manual_plugin_slider_revolution_update_notify() {
$allowed_html_array = array('a' => array('href' => array(),'title' => array()),'br' => array(),'div' => array('class' => array('manual_adminmsg'),),'strong' => array(),'span' => array(),);
echo wp_kses( '<div class="manual_adminmsg"><span>PLEASE UPGRADE "Slider Revolution" to new version 5.4.6.4</span> <br><br> 1. Go to: Plugins -> Installed Plugins. <br>2. <strong>DELETE plugin</strong> "Slider Revolution" for the system. <strong><i>(you must DEACTIVATE plugin first and DELETE it)</i></strong> <br> 3. <strong>Click on "Begin installing plugin"</strong>, to install new version.</span> <br><br><i>Note: No data will be loss in this upgrade process.</i> </div>', $allowed_html_array);
}
?>