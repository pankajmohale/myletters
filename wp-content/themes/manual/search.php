<?php
/**
 * The template for displaying search results pages.
 */
if(!empty($_GET['ajax']) ? $_GET['ajax'] : null) { ?>
    <?php if (have_posts()) : ?>
        <ul class="manual-searchresults">
            <?php 
			while (have_posts()) : the_post(); 
			$post_type = get_post_type( get_the_ID() );
			$post_access_display = '';
			if( $post_type == "manual_documentation" ) {
				$new_class = 'live_search_doc_icon';
				$post_access_display = manual_search_content_access_control(get_the_ID(), 'manualdocumentationcategory','doc_cat_check_login_', 'doc_cat_user_role_','doc_single_article_access','doc_single_article_user_access');
			} else if ( $post_type == "manual_faq" ) {
				$new_class = 'live_search_faq_icon';
				$post_access_display = manual_search_content_access_control(get_the_ID(), 'manualfaqcategory','doc_cat_check_login_', 'doc_cat_user_role_','doc_single_article_access','doc_single_article_user_access');
			} else if ( $post_type == "manual_kb" ) {
				$new_class = 'live_search_kb_icon';
				$post_access_display = manual_search_content_access_control(get_the_ID(), 'manualknowledgebasecat','kb_cat_check_login_', 'kb_cat_user_role_','doc_single_article_access','doc_single_article_user_access');
			} else if ( $post_type == "forum" ) {
				$new_class = 'live_search_forum_icon';
			} else if ( $post_type == "manual_portfolio" ) {
				$new_class = 'live_search_portfolio_icon';
			} else if ( $post_type == "attachment" ) {
				$new_class = 'live_search_attachment_icon';
			} else if ( $post_type == "post" ) {
				$new_class = 'live_search_post_icon';
			} else {
				$new_class = '';
			}
			?>
            <li class="<?php echo $new_class; ?>" style="display:<?php echo $post_access_display; ?>;">
                <a href="<?php the_permalink() ?>">
                    <?php 
					the_title();
                    manual_live_search_navigation(get_the_ID(), $post_type);
                    ?>
                </a>
            </li>
            <?php endwhile ?>
            <li class="manual-searchresults-showall">
            <?php 
			// translation search url fix
			$ajax_live_search_url = home_url('/'); 
			$ajax_live_search_url_query_str = parse_url($ajax_live_search_url, PHP_URL_QUERY);
			$only_site_url = preg_replace('/\\?.*/', '', $ajax_live_search_url);
			if($ajax_live_search_url_query_str != '' ) {
				$live_search_final_url = $only_site_url.'?'.$ajax_live_search_url_query_str.'&s=';
			} else {
				$live_search_final_url = $only_site_url.'?s=';
			}
			// eof translation search url fix
			?>
                <a href="<?php echo $live_search_final_url.get_search_query(); ?>&post_type=<?php echo $_GET['post_type']; ?>">
					<?php echo $theme_options['global-showall-search-text-paceholder']; ?>
                </a> 
            </li>
        </ul>
    <?php else : ?>
        <ul class="manual-searchresults">
        <li class="manual-searchresults-noresults"><?php echo $theme_options['global-noresult-search-text-paceholder']; ?></li>
        </ul>
    <?php endif ?>
<!-- Normal search results code -->
<?php 
} else {
get_header();
?>
<!-- /start container -->
<div class="container content-wrapper body-content">
<div class="row margin-top-btm-50">
<div class="col-md-8 col-sm-8">
  <?php 
		if ( have_posts() ) : 
		// Start the loop.
		while ( have_posts() ) : the_post();
			$post_type = get_post_type( get_the_ID() );
			$post_access_display = '';
			if( $post_type == "manual_documentation" ) {
				//access control check for category
				$terms = get_the_terms( get_the_ID() , 'manualdocumentationcategory' ); 
				$check_if_login_call = get_option( 'doc_cat_check_login_'.$terms[0]->term_id );
				$check_user_role = get_option( 'doc_cat_user_role_'.$terms[0]->term_id );
				if( $check_if_login_call == 1 && !is_user_logged_in() ) {
					$post_access_display = 'none';
				} else {
					$access_status = manual_doc_access($check_user_role);
					if( $check_if_login_call == 1 && is_user_logged_in() && $access_status == false ) { 
						$post_access_display = 'none';
					} else {
						// check for single page
						$access_meta = get_post_meta( get_the_ID(), 'doc_single_article_access', true );
						$check_post_user_level_access = get_post_meta( get_the_ID(), 'doc_single_article_user_access', true );
						if( isset($access_meta['login']) && $access_meta['login'] == 1 && !is_user_logged_in() ) {
							$post_access_display = 'none';
						} else {
							if( !empty($check_post_user_level_access) )  $access_status = manual_doc_access(serialize($check_post_user_level_access));
							else  $access_status = true;
							
							if(  isset($access_meta['login']) && $access_meta['login'] == 1 && is_user_logged_in() && $access_status == false ) {
								$post_access_display = 'none';
							} else {
								$post_access_display = '';
							}
						}
						// eof single page check 
					}
				} 
				//eof access control check for category 
			}  else if ( $post_type == "manual_kb" ) {
				//access control check for category
				$terms = get_the_terms( get_the_ID() , 'manualknowledgebasecat' ); 
				$check_if_login_call = get_option( 'kb_cat_check_login_'.$terms[0]->term_id );
				$check_user_role = get_option( 'kb_cat_user_role_'.$terms[0]->term_id );
				if( $check_if_login_call == 1 && !is_user_logged_in() ) {
					$post_access_display = 'none';
				} else {
					$access_status = manual_doc_access($check_user_role);
					if( $check_if_login_call == 1 && is_user_logged_in() && $access_status == false ) { 
						$post_access_display = 'none';
					} else {
						// check for single page
						$access_meta = get_post_meta( get_the_ID(), 'doc_single_article_access', true );
						$check_post_user_level_access = get_post_meta( get_the_ID(), 'doc_single_article_user_access', true );
						if( isset($access_meta['login']) && $access_meta['login'] == 1 && !is_user_logged_in() ) {
							$post_access_display = 'none';
						} else {
							if( !empty($check_post_user_level_access) )  $access_status = manual_doc_access(serialize($check_post_user_level_access));
							else  $access_status = true;
							
							if(  isset($access_meta['login']) && $access_meta['login'] == 1 && is_user_logged_in() && $access_status == false ) {
								$post_access_display = 'none';
							} else {
								$post_access_display = '';
							}
						}
						// eof single page check 
					}
				} 
				//eof access control check for category 
			}  else if ( $post_type == "manual_faq" ) {
				//access control check for category
				$terms = get_the_terms( get_the_ID() , 'manualfaqcategory' ); 
				$check_if_login_call = get_option( 'doc_cat_check_login_'.$terms[0]->term_id );
				$check_user_role = get_option( 'doc_cat_user_role_'.$terms[0]->term_id );
				if( $check_if_login_call == 1 && !is_user_logged_in() ) {
					$post_access_display = 'none';
				} else {
					$access_status = manual_doc_access($check_user_role);
					if( $check_if_login_call == 1 && is_user_logged_in() && $access_status == false ) { 
						$post_access_display = 'none';
					} else {
						// check for single page
						$post_access_display = '';
						// eof single page check 
					}
				} 
				//eof access control check for category 
			}  
			if($post_access_display == '') get_template_part( 'content', 'search' );
		endwhile;
		// Previous/next page navigation.
		the_posts_pagination( array(
			'prev_text'          => esc_html__( '&lt;', 'manual' ),
			'next_text'          => esc_html__( '&gt;', 'manual' ),
		) );
	// If no content, include the "No posts found" template.
	else :
		esc_html_e( 'Sorry!! nothing found related to your search topic, please try search again.', 'manual' );
	endif;
	?>
  <div class="clearfix"></div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
<?php } ?>