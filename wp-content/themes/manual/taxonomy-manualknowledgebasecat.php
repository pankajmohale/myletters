<?php 
get_header(); 
global $theme_options;
if( isset($theme_options['kb-cat-display-order']) && $theme_options['kb-cat-display-order'] != ''  ) {
	if( $theme_options['kb-cat-display-order'] == 1 ) {
		$cat_display_order = 'ASC';
	} else {
		$cat_display_order = 'DESC';
	}
}
// pages
if( isset($theme_options['kb-cat-page-display-order']) && $theme_options['kb-cat-page-display-order'] != ''  ) {
	if( $theme_options['kb-cat-page-display-order'] == 1 ) {
		$page_display_order = 'ASC';
	} else {
		$page_display_order = 'DESC';
	}
}
if( isset( $theme_options['kb-cat-page-display-order-by'] ) && $theme_options['kb-cat-page-display-order-by'] != '' ) {
	$display_page_order_by = $theme_options['kb-cat-page-display-order-by'];	
} else {
	$display_page_order_by = 'date';	
}

if( $theme_options['kb-cat-sidebar-status'] == true ) {
	$col_content = 12;
} else {
	$col_content = 8;
}

// eof page order
// Get our extra meta
$st_term_data =	$wp_query->queried_object;
$term_slug = get_query_var( 'term' );
$current_term = get_term_by( 'slug', $term_slug, 'manualknowledgebasecat' );
$check_if_login_call = get_option( 'kb_cat_check_login_'.$current_term->term_id );
$check_user_role = get_option( 'kb_cat_user_role_'.$current_term->term_id );
$custom_login_message = get_option( 'kb_cat_login_message_'.$current_term->term_id );

$term_id = $current_term->term_id;
?>

<!-- /start container -->
<div class="container content-wrapper body-content">
<div class="row margin-top-btm-50">
<?php if( $check_if_login_call == 1 && !is_user_logged_in() ) { ?>
<div class="col-md-<?php echo $col_content; ?> col-sm-<?php echo $col_content; ?>">
  <div class="manual_login_page">
    <div class="custom_login_form">
      <?php if( $custom_login_message != '' ) { ?>
      <h3><?php echo stripslashes($custom_login_message); ?></h3>
      <?php } ?>
      <?php wp_login_form(); ?>
      <ul class="custom_register">
        <?php if ( get_option( 'users_can_register' ) ) { wp_register(); } ?>
        <li><a href="<?php echo wp_lostpassword_url(); ?>" title="Lost Password">
          <?php esc_html_e( 'Lost Password', 'manual' ); ?>
          </a></li>
      </ul>
    </div>
  </div>
</div>
<?php } else { 
	$access_status = manual_doc_access($check_user_role);
	if( $check_if_login_call == 1 && is_user_logged_in() && $access_status == false ) { 
		echo '<div class="col-md-'.$col_content.' col-sm-'.$col_content.'"><div class="doc_access_control"><p>';
		echo $theme_options['kb-cat-page-access-control-message'];
		echo '</p></div></div>';
	} else {

?>
<div class="col-md-<?php echo $col_content; ?> col-sm-<?php echo $col_content; ?>"> <!--control-->
  <?php 

if( $theme_options['all-child-cat-post-in-root-category'] == false ) {
echo '<div class="masonry-grid-inner margin-btm-25" style="clear:both;">';	
	if( !is_paged() ) { 
	// CHILD CAT !! CHEK IF THERE IS ANY
	$st_subcat_args = array(
	  'orderby' => 'name',
	  'order'   => $cat_display_order,
	  'child_of' => $term_id,
	  'parent' => $term_id
	);
	$st_sub_categories = get_terms('manualknowledgebasecat', $st_subcat_args);
		if ($st_sub_categories) {
			 // If the category has sub categories 
			$st_subcat_args = array(
			  'orderby' => 'name',
			  'order'   => $cat_display_order,
			  'child_of' => $term_id,
			  'parent' => $term_id
			);
			$st_sub_categories = get_terms('manualknowledgebasecat', $st_subcat_args); 
			$st_sub_categories_count = count($st_sub_categories);
			if( $st_sub_categories_count == 1 ) { 
				$col_md_sm = 12;
				$col_sm = 12;
			} else { 
				$col_md_sm = 6; 
				$col_sm = 12;
			}
			foreach($st_sub_categories as $st_sub_category) {
	?>
  <div class="col-md-<?php echo $col_md_sm; ?> col-sm-<?php echo $col_sm; ?> masonry-item kb-subcat-cssfix"> 
    <!--Start-->
    <div class="knowledgebase-body">
      <h5><a href="<?php echo get_term_link($st_sub_category->slug, 'manualknowledgebasecat'); ?>">
        <?php   
					 $cat_title = $st_sub_category->name; 
					 echo $cat_title = html_entity_decode($cat_title, ENT_QUOTES, "UTF-8");
					?>
        </a> </h5>
      <span class="separator small"></span>
      <ul class="kbse">
        <?php 
			
						if( isset( $theme_options['kb-no-of-records-per-cat'] ) && $theme_options['kb-no-of-records-per-cat'] != '' ) {
							$display_on_of_records_under_cat = $theme_options['kb-no-of-records-per-cat'];	
						} else {
							$display_on_of_records_under_cat = '5';	
						}
				  
						// Get posts per category
						$args = array( 
							'numberposts' => $display_on_of_records_under_cat, 
							'post_type'  => 'manual_kb',
							'orderby' => $display_page_order_by,
							'order'  => $page_display_order,
							'tax_query' => array(
								array(
									'taxonomy' => 'manualknowledgebasecat',
									'field' => 'id',
									'include_children' => false,
									'terms' => $st_sub_category->term_id
								)
							)
						);
						$st_cat_posts = get_posts( $args );
						foreach( $st_cat_posts as $post ) : 
						?>
        <li class="cat inner"> <a href="<?php the_permalink(); ?>">
          <?php 
						 $org_title = get_the_title(); 
						 echo $title = html_entity_decode($org_title, ENT_QUOTES, "UTF-8");
						 ?>
          </a> </li>
        <?php 
						endforeach; 
						?>
      </ul>
      <div style="padding:10px 0px;"> <a href="<?php echo get_term_link($st_sub_category->slug, 'manualknowledgebasecat'); ?>"  class="custom-link hvr-icon-wobble-horizontal kblnk"> <?php echo $theme_options['kb-cat-view-all']; ?> <?php echo $st_sub_category->count; ?> </a> </div>
    </div>
    <!--Eof Start--> 
  </div>
  <?php 
			}   
		} 
	}
echo '</div>';	
}


// PARENT CAT
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array(
				'post_type' => 'manual_kb',
				'paged' => $paged,
				'posts_per_page' => '-1',
				'order'  => $page_display_order,
				'orderby' => $display_page_order_by,
				'tax_query' => array(
						array(
							'taxonomy' => 'manualknowledgebasecat',
							'field' => 'slug',
							'include_children' => (($theme_options['all-child-cat-post-in-root-category'] == false)? false : true ), //false,
							'terms' => $term_slug
							)
						),
				
);
$wp_query = new WP_Query($args);

if($wp_query->have_posts()) :  
?>
  <div class="col-md-12 col-md-12 clearfix margin-btm-25" style="padding-left:1px; clear:both;"> 
    <!--Start-->
    <div class="knowledgebase-cat-body">
      <?php
		if ( have_posts() ) {
			  
			while($wp_query->have_posts()) : $wp_query->the_post(); 
        ?>
      <div class="kb-box-single">
        <h4 style="padding-top:4px; margin-bottom:5px;"><a href="<?php the_permalink(); ?>">
          <?php 
					 $org_title = get_the_title(); 
					 echo $title = html_entity_decode($org_title, ENT_QUOTES, "UTF-8");
					 ?>
          </a> </h4>
        <p>
          <?php if( $theme_options['knowledgebase-cat-quick-stats-under-title'] == false ) { ?>
          <i class="fa fa-eye"></i> <span>
          <?php 
			if( get_post_meta( $post->ID, 'manual_post_visitors', true ) != '' ) { 
				echo get_post_meta( $post->ID, 'manual_post_visitors', true );
				echo ' views';
			} else { echo '0 views'; } ?>
          </span> <i class="fa fa-calendar"></i> <span>
          <?php the_time( get_option('date_format') ); ?>
          <?php if($theme_options['knowledgebase-cat-disable-doc-author-name'] == true) { ?>
          </span> <i class="fa fa-user"></i> <span>
          <?php the_author(); } ?>
          </span> <i class="fa fa-thumbs-o-up"></i> <span>
          <?php if( get_post_meta( $post->ID, 'votes_count_doc_manual', true ) == '' ) { echo 0; } else { echo get_post_meta( $post->ID, 'votes_count_doc_manual', true ); } ?>
          </span>
          <?php } ?>
        </p>
      </div>
      <?php  endwhile; 
	  
			
		// Previous/next page navigation.
		the_posts_pagination( array(
			'prev_text'          => esc_html__( '&lt;', 'manual' ),
			'next_text'          => esc_html__( '&gt;', 'manual' ),
		) );  

		} else {
			 esc_html_e( 'Sorry, no posts were found', 'manual' );
		}			
				  ?>
    </div>
    <!--Eof Start--> 
  </div>
  <?php endif; 
  wp_reset_postdata();
  ?>
</div>
<?php }} ?>
<?php if( $theme_options['kb-cat-sidebar-status'] != true ) { ?>
<aside class="col-md-4 col-sm-4" id="sidebar-box">
  <div class="custom-well sidebar-nav">
    <?php 
                if ( is_active_sidebar( 'kb-sidebar-1' ) ) : 
                    dynamic_sidebar( 'kb-sidebar-1' ); 
                endif; 
            ?>
  </div>
</aside>
<?php } ?>
<?php get_footer(); ?>
