<?php
/**
* The template for displaying Search Results pages.
*
*/

get_header(); 
$f_sidebar = $theme_options['manual-forum-yes-no-sidebar'];
if( $f_sidebar == 1 ) { $col_md = 12;
} else { $col_md = 8; } 
?>

<!-- /start container -->
<div class="container content-wrapper body-content">
<div class="row margin-50">
<?php if( $f_sidebar == 'left' ) { get_sidebar('bbpress'); } ?>
<div class="col-md-<?php echo $col_md; ?> col-sm-<?php echo $col_md; ?>"> 
  <!-- #primary -->
  <?php do_action( 'bbp_template_before_search' ); ?>
  <?php if ( bbp_has_search_results() ) : ?>
  <?php bbp_get_template_part( 'loop',       'search' ); ?>
  <?php bbp_get_template_part( 'pagination', 'search' ); ?>
  <?php elseif ( bbp_get_search_terms() ) : ?>
  <?php bbp_get_template_part( 'feedback',   'no-search' ); ?>
  <?php else : ?>
  <?php bbp_get_template_part( 'form', 'search' ); ?>
  <?php endif; ?>
  <?php do_action( 'bbp_template_after_search_results' ); ?>
</div>
<!-- /#content -->

<?php 
if( $f_sidebar == 1 ) { 
} else { get_sidebar('bbpress'); } 
?>
<?php get_footer(); ?>