<?php 
$terms = get_the_terms( $post->ID , 'manualknowledgebasecat' ); 
if( !empty($terms) ) { 
$current_term = $terms[0];
$term_id = $current_term->term_id; // cat id

get_header();
global $theme_options;
$col_type = '';
if( $theme_options['kb-cat-sidebar-singlepg-status'] == true ) {
	$col_type = 12;	
} else {
	$col_type = 8;	
}
?>

<!-- /start container -->
<div class="container content-wrapper body-content">
<div class="row margin-top-btm-50">

<?php 
$check_if_login_call = get_option( 'kb_cat_check_login_'.$terms[0]->term_id );
$check_user_role = get_option( 'kb_cat_user_role_'.$terms[0]->term_id );
$custom_login_message = get_option( 'kb_cat_login_message_'.$terms[0]->term_id );
if( $check_if_login_call == 1 && !is_user_logged_in() ) {
?>
<div class="col-md-<?php echo $col_type; ?> col-sm-<?php echo $col_type; ?>">
    <div class="manual_login_page">
      <div class="custom_login_form">
        <?php if( $custom_login_message != '' ) { ?>
        	<h3><?php echo stripslashes($custom_login_message); ?></h3> 
		<?php } ?>
        <?php wp_login_form(); ?>
        <ul class="custom_register">
        <?php if ( get_option( 'users_can_register' ) ) { wp_register(); } ?>
        <li><a href="<?php echo wp_lostpassword_url(); ?>" title="Lost Password"><?php esc_html_e( 'Lost Password', 'manual' ); ?></a></li>
        </ul>
      </div>
    </div>
</div>

<?php 
} else {
	$access_status = manual_doc_access($check_user_role);
	if( $check_if_login_call == 1 && is_user_logged_in() && $access_status == false ) { 
		echo '<div class="col-md-'.$col_type.' col-sm-'.$col_type.'"><div class="doc_access_control"><p>';
		echo $theme_options['kb-single-page-access-control-message'];
		echo '</p></div></div>';
	} else {
?>


<?php if( $theme_options['kb-cat-sidebar-singlepg-status'] != true && $theme_options['kb-single-page-sidebar-position'] == 'left' ) { ?>
<aside class="col-md-4 col-sm-4" id="sidebar-box">
  <div class="custom-well sidebar-nav">
    <?php 
                if ( is_active_sidebar( 'kb-sidebar-1' ) ) : 
                    dynamic_sidebar( 'kb-sidebar-1' ); 
                endif; 
            ?>
  </div>
</aside>
<?php } ?>

<div class="col-md-<?php echo $col_type; ?> col-sm-<?php echo $col_type; ?>">


	<?php 
	// post access control
	$access_meta = get_post_meta( $post->ID, 'doc_single_article_access', true );
	$check_post_user_level_access = get_post_meta( $post->ID, 'doc_single_article_user_access', true );
	if( isset($access_meta['login']) && $access_meta['login'] == 1 && !is_user_logged_in() ) {
	?>	
	<div class="col-md-12 col-sm-12">
		<div class="manual_login_page">
		  <div class="custom_login_form">
			<?php if( isset($access_meta['message']) && $access_meta['message'] != '' ) { ?>
				<h3><?php echo stripslashes($access_meta['message']); ?></h3> 
			<?php } ?>
			<?php wp_login_form(); ?>
			<ul class="custom_register">
			<?php if ( get_option( 'users_can_register' ) ) { wp_register(); } ?>
			<li><a href="<?php echo wp_lostpassword_url(); ?>" title="Lost Password"><?php esc_html_e( 'Lost Password', 'manual' ); ?></a></li>
			</ul>
		  </div>
		</div>
	</div>
	<?php	
	} else { 
	
		if( !empty($check_post_user_level_access) )  $access_status = manual_doc_access(serialize($check_post_user_level_access));
		else  $access_status = true;

		if( isset($access_meta['login']) && $access_meta['login'] == 1 && is_user_logged_in() && $access_status == false ) {
				echo '<div class="doc_access_control"><p>';
				echo $theme_options['kb-single-page-access-control-message'];
				echo '</p></div>';
		} else {
	// eof post access control
		?>

  <?php while ( have_posts() ) : the_post(); ?>
  <div class="kb-single" <?php if( $theme_options['knowledgebase-quick-stats-under-title'] == true ) { ?> <?php } ?>>
  <h2 class="singlepg-font">
    <?php the_title(); ?>
  </h2>
  
    <p class="kb-box-single-page" > 
    <?php if( $theme_options['knowledgebase-quick-stats-under-title'] != true ) { ?>  
    <i class="fa fa-eye"></i>
     <span> <?php 
			if( get_post_meta( $post->ID, 'manual_post_visitors', true ) != '' ) { 
				echo get_post_meta( $post->ID, 'manual_post_visitors', true );
				echo esc_html_e( '&nbsp;views ', 'manual' );
			} else { echo '0 views'; } ?></span>
            
      <?php if( $theme_options['kb-singlepg-publish-date-status'] == true ) { ?>      
      <i class="fa fa-calendar"></i> <span><?php the_time( get_option('date_format') ); ?></span>
      <?php } ?>
      
      <?php 
	  if( $theme_options['kb-singlepg-modified-date-status'] == true ) {
	  if (get_the_modified_time() != get_the_time()) { ?>
      <i class="fa fa-calendar-plus-o"></i> <span><?php the_modified_time( get_option('date_format') ); ?></span>
      <?php } } ?>
      
      <?php if( $theme_options['kb-disable-doc-author-name'] == true ) { ?>
      <i class="fa fa-user"></i> <span><?php $author_id = $post->post_author; echo the_author_meta( $theme_options['kb-single-post-user-name'] , $author_id ); ?></span>
      <?php } ?>
      
      <i class="fa fa-thumbs-o-up"></i> <span><?php if( get_post_meta( $post->ID, 'votes_count_doc_manual', true ) == '' ) { echo 0; } else { echo get_post_meta( $post->ID, 'votes_count_doc_manual', true ); } ?></span>
      <?php } ?> 
      <?php 
	  if( $theme_options['kb-single-pg-print-status'] == true ) {
		  if (class_exists('WP_Print_O_Matic')) { echo do_shortcode('[print-me printstyle="pom-small-grey" tag="span" target=".entry-content"]'); echo '<span></span>'; } 
	  }
		 ?> 
      <?php edit_post_link( esc_html__( 'Edit', 'manual' ), '<i class="fa fa-edit"></i> <span class="edit-link">', '</span>' ); ?>
    </p>
    
  </div>
  <div class="margin-15 entry-content clearfix">
    <?php the_content(); ?>
  </div>
  
  <?php if (is_single() && has_term( '', 'manual_kb_tag' )) { ?>
      <div class="tagcloud singlepgtag kbtag clearfix margin-btm-20 singlepg"><span><i class="fa fa-tags"></i> <?php echo esc_html__( 'Tags:', 'manual' ); ?></span><?php the_terms( get_the_ID(), 'manual_kb_tag', '' , ''); ?>
      </div>
 <?php } ?>
  
  <?php 
	if( get_post_meta( $post->ID, '_manual_attachement_access_status', true ) == true && !is_user_logged_in() ) { 
		$message = get_post_meta( $post->ID, '_manual_attachement_access_login_msg', true ); 
		manual_access_attachment($message);
	} else { 
		manual_kb_attachment_files(); 
	} 
  ?>
  <?php endwhile; // end of the loop. ?>
  <?php if( $theme_options['knowledgebase-social-share-status'] != true ) { manual_social_share(get_permalink()); } ?>
  <?php if( ($theme_options['knowledgebase-voting-buttons-status'] != true && $theme_options['knowledgebase-voting-login-users'] != true) || 
            ($theme_options['knowledgebase-voting-buttons-status'] == false && $theme_options['knowledgebase-voting-login-users'] == true && is_user_logged_in())
		   ) { ?>
  <div id="rate-topic-content" class="row-fluid margin-15">
    <div class="rate-buttons"> <?php if(isset($theme_options['yes-no-above-message'])) { ?><p class="helpfulmsg"><?php echo $theme_options['yes-no-above-message']; ?></p> <?php } ?><span class="post-like"><a data-post_id="<?php echo $post->ID; ?>" href="#"><span class="btn btn-success rate custom-like-dislike-btm" data-rating="1"><i class="glyphicon glyphicon-thumbs-up"></i> <span class="manual_doc_count"><?php echo $meta_values = get_post_meta( $post->ID, 'votes_count_doc_manual', true ); ?> <?php echo $theme_options['yes-user-input-text']; ?></span></span></a></span> <span class="post-unlike"><a data-post_id="<?php echo $post->ID; ?>" href="#"><span class="btn btn-danger rate custom-like-dislike-btm" data-rating="0"> <i class="glyphicon glyphicon-thumbs-down"></i> <span class="manual_doc_unlike_count"><?php echo $meta_values = get_post_meta( $post->ID, 'votes_unlike_doc_manual', true ); ?> <?php echo $theme_options['no-user-input-text']; ?> </span></span></a></span> </div>
    <?php 
	if( is_super_admin() && is_user_logged_in() ) {
		echo '<span class="post-reset"><a data-post_id="'.$post->ID.'" href="#"><span class="btn btn-link" data-rating="0"> <i class="fa fa-refresh"></i> <span class="rating_reset_display"> Reset </span></span></a></span>';
	}
	?>
  </div>
  <?php } ?>
    <?php
	if( $theme_options['kb-related-post-status'] == true ) { manual_kb_related_post(); }
	if( $theme_options['kb-comment-status'] == true ) {
		if ( comments_open() || get_comments_number() ) {
			if( $theme_options['kb-comment-box-on-thumbsdown'] == true ){ echo '<div class="kb-respond-no-message"><div class="kb-feedback-showhide" style="display:none;">';}
			comments_template( '', true ); 
			if( $theme_options['kb-comment-box-on-thumbsdown'] == true ){ echo '</div></div>'; }
		}
	}
	?>
  
  <div style="clear:both"></div>
  <span class="manual-views" id="manual-views-<?php echo $post->ID; ?>"></span> <br>
  
  
  
  <?php
	// post access control
			}
		}
	// eof post access control
  ?>
  
  
  
</div>

<?php }} ?>

<?php if( $theme_options['kb-cat-sidebar-singlepg-status'] != true && $theme_options['kb-single-page-sidebar-position'] == 'right' ) { ?>
<aside class="col-md-4 col-sm-4" id="sidebar-box">
  <div class="custom-well sidebar-nav">
    <?php 
                if ( is_active_sidebar( 'kb-sidebar-1' ) ) : 
                    dynamic_sidebar( 'kb-sidebar-1' ); 
                endif; 
            ?>
  </div>
</aside>
<?php } ?>
<?php 
get_footer(); 
} else {
 esc_html_e( 'Please assign category for your Knowledge Base RECORD', 'manual' );	
} 
?>